require(
	 ['jquery', 'jquery/ui', 'bootstrap', 'owl-carousel'], 
    function($) {
    	$('#showmore_text').hide();
		$('#showmore_button').click(function (){
			$('#showmore_text').slideToggle();
		});
        
//        $(".topresources").owlCarousel({
//        autoPlay: 3000,
//        navigation : true,
//        items : 2,
//        itemsDesktop : [1199,2],
//        itemsDesktopSmall : [979,2]
//
//        });  
        
       $(document).ready(function(){ 
            $('.nav.nav-tabs li a').click(function(){  
              $(".tab-content").removeClass('tab-active');
              $(".tab-content[data-id='"+$(this).attr('data-id')+"']").addClass("tab-active");
              $("nav.nav-tabs li a").removeClass('active');
              $(this).parent().find("nav.nav-tabs li a").addClass('active');
             });
        });
        
        $(".innerpagerecentartical").owlCarousel({
            autoPlay: 3000,
            navigation : true,
            items : 3,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [979,2]

            });
        
         $(".homevideotesti").owlCarousel({
            autoPlay: false,
            navigation : true,
            items : 1,
            itemsDesktop : [1199,1],
            itemsDesktopSmall : [979,1]

            });  
        
        
        
        
        
        $(".drop-down .selected a").click(function() {
            $(".drop-down .options ul").toggle();
            $(".virtualcategory").hide();
        });

        //SELECT OPTIONS AND HIDE OPTION AFTER SELECTION
        $(".drop-down .options ul li a").click(function() {
            var text = $(this).html();
            $(".drop-down .selected a span").html(text);
            $(".drop-down .options ul").hide();
        }); 
        
        $(".clickoption").click(function() {
            var text = $(this).html();
            $(".virtualcategory").toggle();
        }); 


        //HIDE OPTIONS IF CLICKED ANYWHERE ELSE ON PAGE
        $(document).bind('click', function(e) {
            var $clicked = $(e.target);
            if (! $clicked.parents().hasClass("drop-down"))
                $(".drop-down .options ul").hide();
        });
        
        $(".gotitbutton").click(function() {
            $(".bss-cookie-noticeS").hide();
        });
        

        
        $(document).ready(function(){
            var showText=false;
            $(".toggle-button").click(function(){
                if (!showText){
                    jQuery(".toggle-button").text("Show Less");
                    showText=true;
                } else { 
                    jQuery(".toggle-button").text("Show More");
                    showText=false;
                };
                $(".hidden").slideToggle("slow");
            });
        });
        
        $(document).ready(function(){
            var showText=false;
            $(".toggle-buttoncat").click(function(){
                if (!showText){
                    jQuery(".toggle-buttoncat").text("Show Less");
                    showText=true;
                } else { 
                    jQuery(".toggle-buttoncat").text("Show More");
                    showText=false;
                };
                $(".hiddencat").slideToggle("slow");
            });
        });
        
        
        $(document).ready(function(){
            var showText=false;
            $("#flip").click(function(){
                if (!showText){
                    jQuery("#flip").text("Show Less");
                    showText=true;
                } else { 
                    jQuery("#flip").text("Show More");
                    showText=false;
                };
                $("#panel").slideToggle("slow");
            });
        });
       
        
//        var str = $('.blog_post_feed .detail p').html();
//        str.replace('&nbsp;', '');
//        $('.blog_post_feed .detail p').html(str);
     
    });

//Product Video Popup
require(['jquery', 'jquery/ui'], function($){
    $(document).ready( function() {
        $('#watch_video').click(function(){
            $('.pro_video_container').show();
        });

        $('.close_video').click(function(){
            window.location.reload();
            $('.pro_video_container').hide();
            
            //$('.pro_video_iframe').stopVideo();

            var video = $(".pro_video_iframe").attr("src");
            $(".pro_video_iframe").attr("src","");
            $(".pro_video_iframe").attr("src",video);
        });
         
        //Custom Category Content
        $('.custom_category_content').css({'height':'auto', 'overflow':'hidden'});
        $('.more_link').click(function(){
            $('.custom_category_content').css({'height':'auto', 'overflow':'auto'});
            //$('.custom_category_content').css({'height':'auto', 'overflow':'auto'}, 'slow');
            $(this).hide();
            $('.less_link').show();
        });
        $('.less_link').click(function(){
            $('.custom_category_content').css({'height':'150px', 'overflow':'hidden'});
            $(this).hide();
            $('.more_link').show();
        });
    });
});
