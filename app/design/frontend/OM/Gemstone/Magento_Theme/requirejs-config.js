var config = {
    paths: {  
    		'owl-carousel': "Magento_Theme/owl-carousel/js/owl.carousel",         
            'bootstrap': "Magento_Theme/bootstrap/js/bootstrap.bundle",
            'aos': "Magento_Theme/bootstrap/js/aos",
        },   
    shim: {
    	 'owl-carousel': {
            deps: ['jquery']
        },
        'bootstrap': {
            deps: ['jquery']
        },
         'aos': {
            deps: ['jquery']
        },
         
    }
};
