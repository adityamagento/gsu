require(
	 ['jquery', 'jquery/ui', 'owl-carousel', 'bootstrap'], 
    function($) {
    	$('#showmore_text').hide();
		$('#showmore_button').click(function (){
			$('#showmore_text').slideToggle();
		});
        
//        $(".topresources").owlCarousel({
//        autoPlay: 3000,
//        navigation : false,
//        dots: true,
//        items : 1
//
//        });  
        
//        $(".recentarticalsection").owlCarousel({
//        autoPlay: 3000,
//            navigation : false,
//            dots: true,
//        items : 1
//
//        }); 
        
        $(".innerpagerecentartical").owlCarousel({
            autoPlay: 3000,
            navigation : true,
            items : 1

            });
        
         $(".homevideotesti").owlCarousel({
            autoPlay: false,
            navigation : true,
            items : 1,
            itemsDesktop : [1199,1],
            itemsDesktopSmall : [979,1]

            });  
        
        
         $(".mobilebanner").owlCarousel({
            autoPlay: 3000,
            navigation : false,
            dots: true,
            items : 1

            }); 
        $(".certifiedMobile").owlCarousel({
            autoPlay: true,
            navigation : true,
            items : 4,
            itemsDesktopSmall : [979,4]

            });
        
        $(".gotitbutton").click(function() {
            $(".bss-cookie-noticeS").hide();
        });
        
        var accordionItem = document.getElementsByClassName("accordion-item");
        var accordionHeading = document.getElementsByClassName("accordion-item-heading");

        // Create event listeners for each accordion heading
        for (i = 0; i < accordionHeading.length; i++) {
          accordionHeading[i].addEventListener("click", openItem);
        }

        function openItem() {
          var itemClassName = this.parentNode.className;

          // If we want to close previous open item when opening another
          for (i = 0; i < accordionItem.length; i++) {
            accordionItem[i].className = 'accordion-item closed';
          }

          if (itemClassName == 'accordion-item closed') {
            this.parentNode.className = 'accordion-item open';
          }
        }

        window.onunload = function() {
          for (i = 0; i < accordionHeading.length; i++) {
            accordionHeading[i].removeEventListener("click", openItem);
          }        
          return;
        }
        
        
        
        $('.searchsectionmain').hide();
        $('.mobilesearchicon').click(function(e){ 
            e.stopPropagation();
            $('.searchsectionmain').show('slide', {direction: 'right'}, 500);
        });
        $('.searchsectionmain').click(function(e){
            e.stopPropagation();
        });

        $(document).click(function(){
             $('.searchsectionmain').hide('slide', {direction: 'right'}, 500);
        });
        
        
        $('.searchsectionmain').hide();
        $('.filterclick').click(function(e){ 
            e.stopPropagation();
            $('.filtershow').slideToggle();
        });
        $('.defaultshorting').click(function(e){
             $('.filtershow').slideUp();
        });
        
        $('.mobileshorting').hide();
        $('.defaultshorting').click(function(e){ 
            e.stopPropagation();
            $('.mobileshorting').slideToggle(100);
        });
        $('.filterclick').click(function(e){
             $('.mobileshorting').slideUp();
        });
        
        $(".mobilefilter li").click(function() {
            $(this).addClass('selected').siblings().removeClass('selected');

            });
        
    // Header Navigation section    
             $('.openNav').click(function(e){ 
              document.getElementById("mySidenav").style.width = "340px";
              document.getElementById("maincontent").style.marginLeft = "250px";
            });

            $('.closeNav').click(function(e){ 
              document.getElementById("mySidenav").style.width = "0";
              document.getElementById("maincontent").style.marginLeft= "0";
            });
   
        
        $('.ddnav').hide();
        $('.menu-arrow').click(function(e){ 
            e.stopPropagation();
            $(this).find('.ddnav').slideToggle();
            $(this).siblings('li').find('.ddnav').slideUp();
            
            $(this).addClass('selected').siblings().removeClass('selected');

        });
           
        
        $(document).ready(function(){
            var showText=false;
            $(".toggle-button").click(function(){
                if (!showText){
                    jQuery(".toggle-button").text("Show Less");
                    showText=true;
                } else { 
                    jQuery(".toggle-button").text("Show More");
                    showText=false;
                };
                $(".hidden").slideToggle("slow");
            });
        });
        
        $(document).ready(function(){
            var showText=false;
            $(".toggle-buttoncat").click(function(){
                if (!showText){
                    jQuery(".toggle-buttoncat").text("Show Less");
                    showText=true;
                } else { 
                    jQuery(".toggle-buttoncat").text("Show More");
                    showText=false;
                };
                $(".itemcontent").slideToggle("slow");
            });
        });
        
        
        $(document).ready(function(){
            var showText=false;
            $("#flip").click(function(){
                if (!showText){
                    jQuery("#flip").text("Show Less");
                    showText=true;
                } else { 
                    jQuery("#flip").text("Show More");
                    showText=false;
                };
                $("#panel").slideToggle("slow");
            });
        });
        
        $('.categories').hide();
        $('.categorytitle').click(function(e){ 
            e.stopPropagation();
            $('.categories').slideToggle();
//            $(this).siblings('li').find('.sidebar_wrapper .categories').slideUp();
            
            $(this).addClass('selected').siblings().removeClass('selected');

        });
        // Header Navigation section 
    });


//Product Video Popup
require(['jquery', 'jquery/ui'], function($){
    $(document).ready( function() {
        $('#watch_video').click(function(){
            $('.pro_video_container').show();
        });

        $('.close_video').click(function(){
            window.location.reload();
            $('.pro_video_container').hide();
            
            //$('.pro_video_iframe').stopVideo();

            var video = $(".pro_video_iframe").attr("src");
            $(".pro_video_iframe").attr("src","");
            $(".pro_video_iframe").attr("src",video);
        });     
    });
});