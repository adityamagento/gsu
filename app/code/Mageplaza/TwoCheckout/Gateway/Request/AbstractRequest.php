<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Gateway\Request;

use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\Item;
use Mageplaza\TwoCheckout\Gateway\Config\Cards;
use Mageplaza\TwoCheckout\Helper\Request;

/**
 * Class AbstractRequest
 * @package Mageplaza\TwoCheckout\Gateway\Request
 */
abstract class AbstractRequest
{
    /**
     * @var Request
     */
    protected $helper;

    /**
     * @var Cards
     */
    protected $config;

    /**
     * AbstractRequest constructor.
     *
     * @param Request $helper
     * @param Cards $config
     */
    public function __construct(
        Request $helper,
        Cards $config
    ) {
        $this->helper = $helper;
        $this->config = $config;
    }

    /**
     * @param array $buildSubject
     *
     * @return InfoInterface
     */
    protected function getValidPaymentInstance(array $buildSubject)
    {
        $paymentDataObject = SubjectReader::readPayment($buildSubject);

        $payment = $paymentDataObject->getPayment();

        ContextHelper::assertOrderPayment($payment);

        return $payment;
    }

    /**
     * @param array $txnArray
     * @param Order $order
     */
    protected function appendLineItems(&$txnArray, $order)
    {
        $lineItems = [];

        /** @var Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            if (!$item->getQtyOrdered() || !$item->getPrice() || $item->getParentItem()) {
                continue;
            }

            $lineItems[] = [
                'type'      => 'product',
                'name'      => $item->getName(),
                'quantity'  => $item->getQtyOrdered(),
                'price'     => $item->getPrice(),
                'tangible'  => $item->getIsVirtual() ? 'N' : 'Y',
                'productId' => $item->getProductId(),
            ];
        }

        if (!$order->getIsVirtual()) {
            $lineItems[] = [
                'type'      => 'shipping',
                'name'      => $order->getShippingMethod(),
                'price'     => $order->getShippingAmount(),
                'productId' => 'shipping',
            ];
        }

        $lineItems[] = [
            'type'      => 'tax',
            'name'      => 'Tax',
            'price'     => $order->getTaxAmount(),
            'productId' => 'tax',
        ];

        $lineItems[] = [
            'type'      => 'coupon',
            'name'      => 'Coupon',
            'price'     => abs($order->getDiscountAmount()),
            'productId' => 'coupon',
        ];

        $txnArray['lineItems'] = $lineItems;
    }

    /**
     * @param array $txnArray
     * @param OrderAddressInterface|Address $address
     * @param string $key
     */
    protected function appendAddress(&$txnArray, $address, $key)
    {
        if (!$address) {
            return;
        }

        $street = $address->getStreet();

        $txnArray[$key] = [
            'name'        => $address->getName(),
            'addrLine1'   => isset($street[0]) ? $street[0] : '',
            'addrLine2'   => isset($street[1]) ? $street[1] : '',
            'city'        => $address->getCity(),
            'state'       => $address->getRegion(),
            'zipCode'     => $address->getPostcode(),
            'country'     => $address->getCountryId(),
            'email'       => $address->getEmail(),
            'phoneNumber' => $address->getTelephone(),
        ];
    }
}
