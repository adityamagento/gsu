<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\TwoCheckout\Helper\Request;
use Mageplaza\TwoCheckout\Model\Source\PaymentInfo as Info;

/**
 * Class RefundRequest
 * @package Mageplaza\TwoCheckout\Gateway\Request
 */
class RefundRequest extends AbstractRequest implements BuilderInterface
{
    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     *
     * @return array
     */
    public function build(array $buildSubject)
    {
        /** @var Payment $payment */
        $payment = $this->getValidPaymentInstance($buildSubject);
        $order   = $payment->getOrder();
        $info    = $payment->getAdditionalInformation();

        return [
            'command'    => 'refund_invoice',
            'sale_id'    => $this->helper->getInfo($info, Info::ORDER_ID),
            'invoice_id' => $this->helper->getInfo($info, Info::TXN_ID),
            'amount'     => $this->helper->convertAmount($buildSubject['amount'], $order, 'refund'),
            'currency'   => strtolower($order->getOrderCurrencyCode()),
            'comment'    => (string) __('Refund request'),
            'category'   => Request::REFUND_CATEGORY_OTHER,
        ];
    }
}
