<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Sales\Model\Order\Payment;

/**
 * Class AuthorizationRequest
 * @package Mageplaza\TwoCheckout\Gateway\Request
 */
class AuthorizationRequest extends AbstractRequest implements BuilderInterface
{
    /**
     * Builds request
     *
     * @param array $buildSubject
     *
     * @return array
     */
    public function build(array $buildSubject)
    {
        /** @var Payment $payment */
        $payment = $this->getValidPaymentInstance($buildSubject);
        $order   = $payment->getOrder();

        $txnArray = [
            'command'         => 'auth',
            'sellerId'        => $this->helper->getSellerId(),
            'privateKey'      => $this->helper->getPrivateKey(),
            'merchantOrderId' => $order->getQuoteId(),
            'token'           => $payment->getAdditionalInformation('token'),
            'currency'        => $order->getOrderCurrencyCode(),
        ];

        if ($this->config->hasLineItem()) {
            $this->appendLineItems($txnArray, $order);
        } else {
            $txnArray['total'] = $this->helper->convertAmount($buildSubject['amount'], $order);
        }

        $this->appendAddress($txnArray, $order->getBillingAddress(), 'billingAddr');
        $this->appendAddress($txnArray, $order->getShippingAddress(), 'shippingAddr');

        return $txnArray;
    }
}
