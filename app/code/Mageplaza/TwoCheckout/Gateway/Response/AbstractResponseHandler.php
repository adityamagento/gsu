<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Gateway\Response;

use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\TwoCheckout\Helper\Response;

/**
 * Class AbstractResponseHandler
 * @package Mageplaza\TwoCheckout\Gateway\Response
 */
abstract class AbstractResponseHandler
{
    /**
     * @var Response
     */
    protected $helper;

    /**
     * AbstractResponseHandler constructor.
     *
     * @param Response $helper
     */
    public function __construct(Response $helper)
    {
        $this->helper = $helper;
    }

    /**
     * @param array $buildSubject
     *
     * @return InfoInterface|Payment
     */
    protected function getValidPaymentInstance(array $buildSubject)
    {
        $paymentDataObject = SubjectReader::readPayment($buildSubject);

        $payment = $paymentDataObject->getPayment();

        ContextHelper::assertOrderPayment($payment);

        return $payment;
    }
}
