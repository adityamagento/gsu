/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
], function (Component, rendererList) {
    'use strict';

    var type = window.checkoutConfig.payment.mptwocheckout_cards.display === 'magento' ? 'cards' : 'hosted';

    rendererList.push(
        {
            type: 'mptwocheckout_cards',
            component: 'Mageplaza_TwoCheckout/js/view/payment/method-renderer/' + type
        }
    );

    return Component.extend({});
});
