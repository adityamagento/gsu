/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'ko',
    'jquery',
    'Magento_Payment/js/view/payment/cc-form',
    'Magento_Checkout/js/action/set-payment-information',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/model/full-screen-loader',
    'mage/translate',
    '2checkoutLib'
], function (
    ko,
    $,
    Component,
    setPaymentInformationAction,
    additionalValidators,
    fullScreenLoader,
    $t
) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Mageplaza_TwoCheckout/payment/cards',
            active: ko.observable(false),
            isValidated: ko.observable(false),
            cardHolderName: ko.observable(),
            cardData: null,
            token: null,
            count: 1
        },

        /**
         * @returns {exports.initialize}
         */
        initialize: function () {
            this._super();

            if (this.getConfig('isSandbox')) {
                TCO.loadPubKey('sandbox');
            }

            return this;
        },

        /**
         * @returns {String}
         */
        getCode: function () {
            return this.index;
        },

        getConfig: function (key) {
            if (window.checkoutConfig.payment[this.getCode()].hasOwnProperty(key)) {
                return window.checkoutConfig.payment[this.getCode()][key];
            }

            return null;
        },

        /**
         * @returns {Object}
         */
        getData: function () {
            var data = this._super();

            if (!data.hasOwnProperty('additional_data') || !data.additional_data) {
                data.additional_data = {};
            }

            data.additional_data.token                 = this.token;
            data.additional_data.mptwocheckout_card    = this.cardData;
            data.additional_data.mptwocheckout_sandbox = this.getConfig('isSandbox');

            return data;
        },

        isProvided: function () {
            if (this.hasVerification() && !this.creditCardVerificationNumber()) {
                return false;
            }

            return !!(this.cardHolderName() && this.creditCardNumber()
                && this.creditCardExpMonth() && this.creditCardExpYear());
        },

        isActive: function () {
            this.active(this.getCode() === this.isChecked());

            return this.active();
        },

        validate: function () {
            this.isValidated(true);

            return this._super() && this.isProvided();
        },

        checkout: function () {
            var args = {
                sellerId: this.getConfig('sellerId'),
                publishableKey: this.getConfig('pubKey'),
                ccNo: this.creditCardNumber(),
                cvv: this.creditCardVerificationNumber(),
                expMonth: this.creditCardExpMonth(),
                expYear: this.creditCardExpYear()
            };

            fullScreenLoader.startLoader();

            TCO.requestToken(this.successCallback.bind(this), this.errorCallback.bind(this), args);
        },

        placeOrder: function (data, event) {
            if (event) {
                event.preventDefault();
            }

            if (!this.validate() || !additionalValidators.validate()) {
                $('body, html').animate({scrollTop: $('#' + this.getCode()).offset().top}, 'slow');

                return false;
            }

            return this._super(data, event);
        },

        successCallback: function (response) {
            fullScreenLoader.stopLoader();

            if (!this.getConfig('ccTypes').hasOwnProperty(response.response.paymentMethod.cardType)) {
                this.messageContainer.addErrorMessage({
                    message: $t('Card type "' + response.response.paymentMethod.cardType + '" is not allowed')
                });

                return;
            }

            this.token = response.response.token.token;

            this.cardData = JSON.stringify(response.response.paymentMethod);

            this.placeOrder();
        },

        errorCallback: function (response) {
            fullScreenLoader.stopLoader();

            // Retry the token request if ajax call fails
            if (response.errorCode === 200 && this.count <= 2) {
                this.count++;
                this.checkout();
            } else {
                this.messageContainer.addErrorMessage({message: response.errorMsg});
            }
        }
    });
});
