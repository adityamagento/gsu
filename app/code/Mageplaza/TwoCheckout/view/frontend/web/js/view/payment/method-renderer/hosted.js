/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

define([
    'jquery',
    'underscore',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/set-payment-information',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/action/get-payment-information',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/view/form/element/email',
    'Magento_Checkout/js/model/quote',
    'mage/template',
    'mage/dataPost'
], function (
    $,
    _,
    Component,
    setPaymentInformationAction,
    additionalValidators,
    getPaymentInformationAction,
    fullScreenLoader,
    email,
    quote,
    mageTemplate
) {
    'use strict';

    var config = window.checkoutConfig;

    return Component.extend({
        defaults: {
            template: 'Mageplaza_TwoCheckout/payment/hosted',
            formTemplate: '<form action="<%- data.action %>" method="post">' +
                '<% _.each(data.data, function(value, index) { %>' +
                '<input name="<%- index %>" value="<%- value %>">' +
                '<% }) %></form>',
            formKeyInputSelector: 'input[name="form_key"]',
            hostedFields: null
        },

        /**
         * @returns {exports.initialize}
         */
        initialize: function () {
            var self = this, form;

            this._super();

            $('[action="' + this.getConfig('approvedUrl') + '"]').html(self.hostedFields);

            form = $(mageTemplate(this.formTemplate, {data: {'action': this.getConfig('approvedUrl'), 'data': {}}}));

            form.appendTo('body').hide();

            if (this.getConfig('display') === 'iframe') {
                require(['2checkoutDirectLib'], function () {
                    self.hostedFields = form.html();

                    inline_2Checkout.subscribe('checkout_closed', function () {
                        window.location.reload();
                    });
                });
            }

            return this;
        },

        /**
         * @returns {String}
         */
        getCode: function () {
            return this.index;
        },

        isActive: function () {
            return this.getCode() === this.isChecked();
        },

        getConfig: function (key) {
            if (window.checkoutConfig.payment[this.getCode()].hasOwnProperty(key)) {
                return window.checkoutConfig.payment[this.getCode()][key];
            }

            return null;
        },

        /**
         * @returns {Object}
         */
        getData: function () {
            var data = this._super();

            if (!data.hasOwnProperty('additional_data') || !data.additional_data) {
                data.additional_data = {};
            }

            data.additional_data.mptwocheckout_sandbox = this.getConfig('isSandbox');

            return data;
        },

        placeOrder: function (data, event) {
            var self = this;

            if (event) {
                event.preventDefault();
            }

            if (!this.validate() || !additionalValidators.validate()) {
                $('body, html').animate({scrollTop: $('#' + this.getCode()).offset().top}, 'slow');

                return false;
            }

            setPaymentInformationAction(this.messageContainer, this.getData()).done(function () {
                getPaymentInformationAction().done(function (response) {
                    self.submitForm(response);
                });
            });

            return true;
        },

        submitForm: function (response) {
            var params      = {},
                self        = this,
                form        = $('[action="' + this.getConfig('approvedUrl') + '"]'),
                billing     = quote.billingAddress(),
                shipping    = quote.shippingAddress(),
                bill_street = billing.street ? billing.street : [],
                ship_street = shipping.street ? shipping.street : [];

            fullScreenLoader.startLoader();

            // required params
            params.sid  = this.getConfig('sellerId');
            params.mode = '2CO';

            // additional params
            params.demo              = 'N';
            params.currency_code     = config.quoteData.quote_currency_code;
            params.merchant_order_id = window.checkoutConfig.quoteData.entity_id;
            params.purchase_step     = 'payment-method';

            // billing information
            params.card_holder_name = billing.firstname + ' ' + billing.lastname;
            params.street_address   = bill_street[0];
            if (bill_street[1]) {
                params.street_address2 = bill_street[1];
            }
            params.city    = billing.city;
            params.state   = billing.region;
            params.zip     = billing.postcode;
            params.country = billing.countryId;

            // shipping information
            params.ship_name           = shipping.firstname + ' ' + shipping.lastname;
            params.ship_street_address = ship_street[0];
            if (ship_street[1]) {
                params.ship_street_address2 = ship_street[1];
            }
            params.ship_city    = shipping.city;
            params.ship_state   = shipping.region;
            params.ship_zip     = shipping.postcode;
            params.ship_country = shipping.countryId;

            params.phone = billing.telephone;
            params.email = config.isCustomerLoggedIn ? config.customerData.email : email().email();

            this.appendLineItems(params, response);

            if (!this.getConfig('isSandbox')) {
                params.x_receipt_link_url = this.getConfig('receiptUrl');
            }

            if ($(this.formKeyInputSelector).val()) {
                params.form_key = $(this.formKeyInputSelector).val();
            }

            form.html(self.hostedFields);

            $.each(params, function (key, value) {
                self.appendForm(form, key, value);
            });

            form.submit();
        },

        appendLineItems: function (params, response) {
            var i        = 0,
                shipping = quote.shippingMethod(),
                total    = _.findWhere(response.totals.total_segments, {'code': 'grand_total'});

            if (!this.getConfig('hasLineItem')) {
                params.li_0_type  = 'product';
                params.li_0_name  = 'Amount';
                params.li_0_price = total ? total.value : 0;

                return;
            }

            $.each(config.quoteItemData, function (index, item) {
                params['li_' + i + '_type']       = 'product';
                params['li_' + i + '_name']       = item.name;
                params['li_' + i + '_quantity']   = item.qty;
                params['li_' + i + '_price']      = parseInt(item.price);
                params['li_' + i + '_tangible']   = item.is_virtual ? 'N' : 'Y';
                params['li_' + i + '_product_id'] = item.product_id;
                i++;
            });

            if (!config.quoteData.is_virtual) {
                params['li_' + i + '_type']       = 'shipping';
                params['li_' + i + '_name']       = shipping.carrier_title + ' - ' + shipping.method_title;
                params['li_' + i + '_price']      = response.totals.shipping_amount;
                params['li_' + i + '_product_id'] = 'shipping';
                i++;
            }

            params['li_' + i + '_type']       = 'tax';
            params['li_' + i + '_name']       = 'Tax';
            params['li_' + i + '_price']      = response.totals.tax_amount;
            params['li_' + i + '_product_id'] = 'tax';
            i++;

            params['li_' + i + '_type']       = 'coupon';
            params['li_' + i + '_name']       = 'Coupon';
            params['li_' + i + '_price']      = Math.abs(response.totals.discount_amount);
            params['li_' + i + '_product_id'] = 'coupon';
        },

        appendForm: function (form, key, value) {
            var i = document.createElement('input');

            i.setAttribute('type', 'hidden');
            i.setAttribute('name', key);
            i.setAttribute('value', value);

            form.append(i);
        }
    });
});
