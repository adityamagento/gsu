<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Mageplaza\Core\Helper\AbstractData;
use Mageplaza\TwoCheckout\Model\Source\Environment;

/**
 * Class Data
 * @package Mageplaza\TwoCheckout\Helper
 */
class Data extends AbstractData
{
    const CONFIG_MODULE_PATH = 'mptwocheckout';

    /**
     * @var string
     */
    protected $_serviceKey;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param EncryptorInterface $encryptor
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        EncryptorInterface $encryptor,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->encryptor     = $encryptor;
        $this->priceCurrency = $priceCurrency;

        parent::__construct($context, $objectManager, $storeManager);
    }

    /**
     * @param string $code
     * @param null $storeId
     *
     * @return mixed
     */
    public function getConfigGeneral($code = '', $storeId = null)
    {
        $value = parent::getConfigGeneral($code, $storeId);

        $obscureFields = ['private_key', 'api_password'];

        if (in_array($code, $obscureFields, true)) {
            return $this->encryptor->decrypt($value);
        }

        return $value;
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getEnvironment($store = null)
    {
        return $this->getConfigGeneral('environment', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getSellerId($store = null)
    {
        return $this->getConfigGeneral('seller_id', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getPublishableKey($store = null)
    {
        return $this->getConfigGeneral('publishable_key', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getPrivateKey($store = null)
    {
        return $this->getConfigGeneral('private_key', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getApiUsername($store = null)
    {
        return $this->getConfigGeneral('api_username', $store);
    }

    /**
     * @param null $store
     *
     * @return string
     */
    public function getApiPassword($store = null)
    {
        return $this->getConfigGeneral('api_password', $store);
    }

    /**
     * @param null $store
     *
     * @return bool
     */
    public function isSandbox($store = null)
    {
        return $this->getEnvironment($store) === Environment::SANDBOX;
    }

    /**
     * @param float $amount
     * @param Order $order
     * @param string $type
     *
     * @return int
     */
    public function convertAmount($amount, $order, $type = '')
    {
        $currency = $order->getOrderCurrencyCode();
        $scope    = $order->getStoreId();
        $max      = $order->getGrandTotal();

        if ($type === 'refund' && $creditMemos = $order->getCreditmemosCollection()) {
            foreach ($creditMemos->getItems() as $item) {
                $max -= $item->getGrandTotal();
            }
        }

        $amount = $this->priceCurrency->convert($amount, $scope, $currency);

        return min($amount, $max);
    }

    /**
     * @param array $response
     * @param array|string $keys
     *
     * @return mixed
     */
    public function getInfo($response, $keys)
    {
        if (is_string($keys)) {
            return isset($response[$keys]) ? $response[$keys] : null;
        }

        if (is_array($keys)) {
            foreach ($keys as $key) {
                if (isset($response[$key])) {
                    if ($key === array_values(array_slice($keys, -1))[0]) {
                        return $response[$key];
                    }

                    array_shift($keys);

                    return $this->getInfo($response[$key], $keys);
                }
            }
        }

        return null;
    }
}
