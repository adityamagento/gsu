<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Helper;

use Twocheckout;
use Twocheckout_Charge;
use Twocheckout_Sale;

/**
 * Class Request
 * @package Mageplaza\TwoCheckout\Helper
 */
class Request extends Data
{
    const REFUND_CATEGORY_OTHER = '5';

    /**
     * @param array $txnArray
     *
     * @return array
     */
    public function createTransaction(&$txnArray)
    {
        $command = $txnArray['command'];
        unset($txnArray['command']);
        switch ($command) {
            case 'auth':
                Twocheckout::privateKey($this->getPrivateKey());
                Twocheckout::sellerId($this->getSellerId());
                Twocheckout::sandbox($this->isSandbox());

                return Twocheckout_Charge::auth($txnArray);
            case 'refund_invoice':
                Twocheckout::username($this->getApiUsername());
                Twocheckout::password($this->getApiPassword());
                Twocheckout::sandbox($this->isSandbox());

                return Twocheckout_Sale::refund($txnArray);
            default:
                return [];
        }
    }
}
