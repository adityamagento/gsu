<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\TwoCheckout\Model\Source\PaymentInfo as Info;

/**
 * Class Response
 * @package Mageplaza\TwoCheckout\Helper
 */
class Response extends Data
{
    /**
     * @param Payment $payment
     * @param array $response
     *
     * @return InfoInterface|Payment
     * @throws LocalizedException
     */
    public function handleResponse($payment, $response)
    {
        $response = $this->getInfo($response, 'response');

        if (empty($response)) {
            return $payment;
        }

        if ($txnId = $this->getInfo($response, 'transactionId')) {
            $payment->setTransactionId($txnId);
        }

        $payment->setAdditionalInformation(Info::TXN_ID, $txnId);
        $payment->setAdditionalInformation(Info::ORDER_ID, $this->getInfo($response, 'orderNumber'));
        $payment->setAdditionalInformation(Info::RES_CODE, $this->getInfo($response, 'responseCode'));
        $payment->setAdditionalInformation(Info::RES_MSG, $this->getInfo($response, 'responseMsg'));
        $payment->setAdditionalInformation(Info::RES_TYPE, $this->getInfo($response, 'type'));

        return $payment;
    }

    /**
     * @param array $response
     *
     * @return string|bool
     */
    public function hasError($response)
    {
        if (empty($response)) {
            return (string) __('Response does not exist');
        }

        if (!$error = $this->getInfo($response, 'exception')) {
            return false;
        }

        $message = '';

        $this->appendMessage($message, $this->getInfo($error, 'errorCode'), 'Error ');
        $this->appendMessage($message, $this->getInfo($error, 'httpStatus'), 'Status ');
        $this->appendMessage($message, $this->getInfo($error, 'errorMsg'));

        return $message;
    }

    /**
     * @param string $message
     * @param string $string
     * @param string $prefix
     */
    protected function appendMessage(&$message, $string, $prefix = '')
    {
        if ($string) {
            if ($message) {
                $message .= ' - ';
            }

            $message .= $prefix . $string;
        }
    }
}
