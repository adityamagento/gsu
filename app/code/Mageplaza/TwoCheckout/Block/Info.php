<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Block;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Block\ConfigurableInfo;
use Magento\Payment\Gateway\ConfigInterface;
use Mageplaza\TwoCheckout\Helper\Data;
use Mageplaza\TwoCheckout\Model\Source\PaymentInfo;

/**
 * Class Info
 * @package Mageplaza\TwoCheckout\Block
 */
class Info extends ConfigurableInfo
{
    /**
     * @var string
     */
    private $_2checkoutUrl = '/sales/detail?sale_id=';

    /**
     * @var Data
     */
    private $helper;

    /**
     * Info constructor.
     *
     * @param Context $context
     * @param ConfigInterface $config
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ConfigInterface $config,
        Data $helper,
        array $data = []
    ) {
        $this->helper = $helper;

        parent::__construct($context, $config, $data);
    }

    /**
     * @param string $field
     *
     * @return mixed
     */
    protected function getLabel($field)
    {
        return PaymentInfo::getOptionArray()[$field];
    }

    /**
     * Sets data to transport
     *
     * @param DataObject $transport
     * @param string $field
     * @param string $value
     *
     * @return void
     * @throws LocalizedException
     */
    protected function setDataToTransfer(DataObject $transport, $field, $value)
    {
        if (in_array($field, [PaymentInfo::ORDER_ID, PaymentInfo::TXN_ID], true) && !$this->helper->isAdmin()) {
            return;
        }

        if ($field === PaymentInfo::ORDER_ID && $this->helper->isAdmin()) {
            $isSandbox = $this->getInfo()->getAdditionalInformation(PaymentInfo::SANDBOX);

            $url = 'https://';
            $url .= $isSandbox ? 'sandbox.2checkout.com/sandbox' : 'www.2checkout.com/va';
            $url .= $this->_2checkoutUrl . $value;

            $value = '<a href="' . $url . '" target="_blank">' . $value . '</a>';
        }

        parent::setDataToTransfer($transport, $field, $value);
    }

    /**
     * Escape HTML entities
     *
     * @param string|array $data
     * @param array|null $allowedTags
     *
     * @return string
     */
    public function escapeHtml($data, $allowedTags = null)
    {
        return strpos($data, $this->_2checkoutUrl) === false ? parent::escapeHtml($data, $allowedTags) : $data;
    }
}
