<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Block\Adminhtml\Backend;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Url
 * @package Mageplaza\TwoCheckout\Block\Adminhtml\Backend
 */
class Url extends Field
{
    /**
     * @var \Magento\Framework\Url
     */
    private $urlBuilder;

    /**
     * Url constructor.
     *
     * @param Context $context
     * @param \Magento\Framework\Url $urlBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Url $urlBuilder,
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;

        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $elementId = explode('_', $element->getHtmlId());
        $url       = $this->urlBuilder->getUrl('mptwocheckout/payment/' . $elementId[4], ['_nosid' => true]);
        $html      = '<input style="opacity:1;" readonly id="' . $element->getHtmlId()
            . '" class="input-text admin__control-text" value="' . $url . '" onclick="this.select()" type="text">';

        return $html;
    }
}
