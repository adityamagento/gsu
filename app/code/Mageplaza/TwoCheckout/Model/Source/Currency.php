<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Model\Source;

/**
 * Class Currency
 * @package Mageplaza\TwoCheckout\Model\Source
 */
class Currency extends \Magento\Config\Model\Config\Source\Locale\Currency
{
    const ALLOWED = 'ARS,AUD,BRL,GBP,BGN,CAD,CLP,DKK,EUR,HKD,INR,IDR,ILS,JPY,MYR,MXN,NZD,NOK,PHP,RON,RUB,SGD,ZAR,SEK,
    CHF,TRY,UAH,AED,USD';

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        return array_filter(parent::toOptionArray(), function ($option) {
            return in_array($option['value'], explode(',', self::ALLOWED), true);
        });
    }
}
