<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Model\Source;

/**
 * Class DisplayCheckout
 * @package Mageplaza\TwoCheckout\Model\Source
 */
class DisplayCheckout extends AbstractSource
{
    const MAGENTO  = 'magento';
    const REDIRECT = 'redirect';
    const IFRAME   = 'iframe';

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::MAGENTO  => __('Magento Default'),
            self::REDIRECT => __('Redirect Checkout'),
            self::IFRAME   => __('Iframe Checkout'),
        ];
    }
}
