<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Model\Source;

/**
 * Class CardTypeMapper
 * @package Mageplaza\TwoCheckout\Model\Source
 */
class CardTypeMapper extends AbstractSource
{
    const VISA             = 'VS';
    const MASTERCARD       = 'MC';
    const AMERICAN_EXPRESS = 'AX';
    const DISCOVER         = 'DS';
    const DINERS           = 'DN';
    const JCB              = 'JC';

    /**
     * Retrieve option array
     *
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::VISA             => CardType::VISA,
            self::MASTERCARD       => CardType::MASTERCARD,
            self::AMERICAN_EXPRESS => CardType::AMERICAN_EXPRESS,
            self::DISCOVER         => CardType::DISCOVER,
            self::DINERS           => CardType::DINERS,
            self::JCB              => CardType::JCB,
        ];
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public static function getCardType($value)
    {
        $options = self::getOptionArray();

        return isset($options[$value]) ? $options[$value] : $value;
    }

    /**
     * @param array $cctypes
     *
     * @return string
     */
    public static function getAllCardTypes($cctypes)
    {
        return array_filter(self::getOptionArray(), function ($option) use ($cctypes) {
            return in_array($option, $cctypes, true);
        });
    }
}
