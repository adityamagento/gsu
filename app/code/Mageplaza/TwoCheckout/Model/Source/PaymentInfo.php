<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Model\Source;

/**
 * Class PaymentInfo
 * @package Mageplaza\TwoCheckout\Model\Source
 */
class PaymentInfo extends AbstractSource
{
    const ORDER_ID = 'order_id';
    const TXN_ID   = 'txn_id';

    const CARD_TYPE    = 'mptwocheckout_card_type';
    const LAST_CARD_4  = 'mptwocheckout_card_last4';
    const EXPIRY_DATE  = 'mptwocheckout_expiry_date';
    const EXPIRY_MONTH = 'mptwocheckout_expiry_month';
    const EXPIRY_YEAR  = 'mptwocheckout_expiry_year';

    const RES_CODE = 'mptwocheckout_res_code';
    const RES_MSG  = 'mptwocheckout_res_msg';
    const RES_TYPE = 'mptwocheckout_res_type';

    const SANDBOX = 'mptwocheckout_sandbox';

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::CARD_TYPE   => __('Card Type'),
            self::LAST_CARD_4 => __('Last Card Number'),
            self::EXPIRY_DATE => __('Expiration Date'),
            self::TXN_ID      => __('2Checkout Invoice ID'),
            self::ORDER_ID    => __('2Checkout Order ID'),
            self::RES_CODE    => __('Response Code'),
            self::RES_MSG     => __('Response Message'),
            self::RES_TYPE    => __('Response Type'),
            self::SANDBOX     => __('Is Sandbox'),
        ];
    }
}
