<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Observer;

use InvalidArgumentException;
use Magento\Framework\Event\Observer;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Mageplaza\TwoCheckout\Gateway\Config\Cards;
use Mageplaza\TwoCheckout\Helper\Data;
use Mageplaza\TwoCheckout\Model\Source\CardType;
use Mageplaza\TwoCheckout\Model\Source\CardTypeMapper;
use Mageplaza\TwoCheckout\Model\Source\PaymentInfo as Info;

/**
 * Class DataAssignObserver
 * @package Mageplaza\TwoCheckout\Observer
 */
class DataAssignObserver extends AbstractDataAssignObserver
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Cards
     */
    private $config;

    /**
     * DataAssignObserver constructor.
     *
     * @param Data $helper
     * @param Cards $config
     */
    public function __construct(
        Data $helper,
        Cards $config
    ) {
        $this->helper = $helper;
        $this->config = $config;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $additionalData = $this->readDataArgument($observer)->getData(PaymentInterface::KEY_ADDITIONAL_DATA);

        if (!is_array($additionalData)) {
            return;
        }

        $payment = $this->readPaymentModelArgument($observer);

        foreach ($additionalData as $key => $datum) {
            if (is_object($datum)) {
                continue;
            }

            if ($key === 'mptwocheckout_card') {
                $card = Data::jsonDecode($datum);

                $cardType    = CardTypeMapper::getCardType($this->helper->getInfo($card, 'cardType'));
                $cardTitle   = CardType::getCardType($cardType);
                $cardNum     = $this->helper->getInfo($card, 'cardNum');
                $expiryMonth = $this->helper->getInfo($card, 'expMonth');
                $expiryYear  = $this->helper->getInfo($card, 'expYear');

                if ($cardType && !in_array($cardType, $this->config->getCcTypes(), true)) {
                    throw new InvalidArgumentException(__('Card type "%1" is not allowed', $cardTitle));
                }

                $payment->setAdditionalInformation(Info::CARD_TYPE, $cardTitle);
                $payment->setAdditionalInformation(Info::LAST_CARD_4, $cardNum);
                $payment->setAdditionalInformation(Info::EXPIRY_MONTH, $expiryMonth);
                $payment->setAdditionalInformation(Info::EXPIRY_YEAR, $expiryYear);

                if ($expiryMonth || $expiryYear) {
                    $payment->setAdditionalInformation(Info::EXPIRY_DATE, $expiryMonth . '/' . $expiryYear);
                }
            }

            $payment->setAdditionalInformation($key, $datum);
        }
    }
}
