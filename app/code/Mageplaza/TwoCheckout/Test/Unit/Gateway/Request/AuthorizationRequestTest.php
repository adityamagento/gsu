<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Test\Unit\Gateway\Request;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\Payment;
use Mageplaza\TwoCheckout\Gateway\Config\Cards;
use Mageplaza\TwoCheckout\Gateway\Request\AuthorizationRequest;
use Mageplaza\TwoCheckout\Helper\Request;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class AuthorizationRequestTest
 * @package Mageplaza\TwoCheckout\Test\Unit\Gateway\Request
 */
class AuthorizationRequestTest extends TestCase
{
    /**
     * @var Request|PHPUnit_Framework_MockObject_MockObject
     */
    private $helper;

    /**
     * @var Cards|PHPUnit_Framework_MockObject_MockObject
     */
    private $config;

    /**
     * @var AuthorizationRequest
     */
    private $object;

    protected function setUp()
    {
        $this->helper = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $this->config = $this->getMockBuilder(Cards::class)->disableOriginalConstructor()->getMock();

        $this->object = new AuthorizationRequest($this->helper, $this->config);
    }

    public function testBuild()
    {
        $paymentDataObject = $this->getMockBuilder(PaymentDataObjectInterface::class)->getMock();

        $payment = $this->getMockBuilder(Payment::class)->disableOriginalConstructor()->getMock();
        $paymentDataObject->method('getPayment')->willReturn($payment);

        /** @var Order|PHPUnit_Framework_MockObject_MockObject $order */
        $order = $this->getMockBuilder(Order::class)->disableOriginalConstructor()->getMock();
        $payment->method('getOrder')->willReturn($order);

        $token = 'token';
        $payment->expects($this->once())->method('getAdditionalInformation')->with('token')->willReturn($token);

        $quoteId      = 12;
        $currencyCode = 'GBP';
        /** @var Address|PHPUnit_Framework_MockObject_MockObject $billing */
        $billing = $this->getMockBuilder(Address::class)->disableOriginalConstructor()->getMock();
        /** @var Address|PHPUnit_Framework_MockObject_MockObject $shipping */
        $shipping = $this->getMockBuilder(Address::class)->disableOriginalConstructor()->getMock();
        $order->method('getQuoteId')->willReturn($quoteId);
        $order->method('getOrderCurrencyCode')->willReturn($currencyCode);
        $order->method('getBillingAddress')->willReturn($billing);
        $order->method('getShippingAddress')->willReturn($shipping);

        $buildSubject = [
            'payment' => $paymentDataObject,
            'amount'  => 100
        ];

        $this->helper->expects($this->once())->method('convertAmount')->willReturn($buildSubject['amount']);
        $sellerId = 'seller id';
        $this->helper->method('getSellerId')->willReturn($sellerId);
        $privateKey = 'private key';
        $this->helper->method('getPrivateKey')->willReturn($privateKey);

        $txnArray = [
            'command'         => 'auth',
            'sellerId'        => $sellerId,
            'privateKey'      => $privateKey,
            'merchantOrderId' => $quoteId,
            'token'           => $token,
            'currency'        => $currencyCode,
            'total'           => $buildSubject['amount']
        ];

        $this->appendAddress($txnArray, $billing, 'billingAddr');
        $this->appendAddress($txnArray, $shipping, 'shippingAddr');

        $this->assertEquals($txnArray, $this->object->build($buildSubject));
    }

    /**
     * @param array $txnArray
     * @param Address|PHPUnit_Framework_MockObject_MockObject $address
     * @param string $key
     */
    private function appendAddress(&$txnArray, $address, $key)
    {
        $name      = 'name';
        $lastname  = 'lastname';
        $street    = ['street1', 'street2'];
        $city      = 'city';
        $region    = 'region';
        $postcode  = 'postcode';
        $country   = 'country';
        $email     = 'email';
        $telephone = 'telephone';
        $address->method('getName')->willReturn($name);
        $address->method('getLastname')->willReturn($lastname);
        $address->method('getStreet')->willReturn($street);
        $address->method('getCity')->willReturn($city);
        $address->method('getRegion')->willReturn($region);
        $address->method('getPostcode')->willReturn($postcode);
        $address->method('getCountryId')->willReturn($country);
        $address->method('getEmail')->willReturn($email);
        $address->method('getTelephone')->willReturn($telephone);

        $txnArray[$key] = [
            'name'        => $name,
            'addrLine1'   => $street[0],
            'addrLine2'   => $street[1],
            'city'        => $city,
            'state'       => $region,
            'zipCode'     => $postcode,
            'country'     => $country,
            'email'       => $email,
            'phoneNumber' => $telephone,
        ];
    }
}
