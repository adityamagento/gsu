<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Controller\Payment;

use Exception;
use InvalidArgumentException;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use Mageplaza\TwoCheckout\Controller\PlaceOrder;

/**
 * Class Approved
 * @package Mageplaza\TwoCheckout\Controller\Payment
 */
class Approved extends PlaceOrder
{
    /**
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $quote = $this->checkoutSession->getQuote();
        $url   = $this->_url->getUrl('checkout/cart', ['_secure' => true]);

        try {
            if (!$quote || !$quote->getItemsCount()) {
                throw new InvalidArgumentException(__('We can\'t initialize checkout.'));
            }
            if ($this->getCheckoutMethod($quote) === Onepage::METHOD_GUEST) {
                $this->prepareGuestQuote($quote);
            }

            $this->disabledQuoteAddressValidation($quote);

            $quote->collectTotals();

            $this->paymentHandler($quote);

            $this->cartManagement->placeOrder($quote->getId());

            $url = $this->_url->getUrl('checkout/onepage/success', ['_secure' => true]);
        } catch (Exception $e) {
            $this->logger->critical($e);
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        }

        /** @var Http $response */
        $response = $this->getResponse();
        $response->setBody('<script>window.top.location.href = "' . $url . '";</script>');
    }

    /**
     * @param Quote $quote
     *
     * @throws LocalizedException
     */
    public function paymentHandler($quote)
    {
        $response = $this->getRequest()->getParams();

        $this->paymentLogger->debug(['hosted request' => [], 'hosted response' => $response]);

        if ($error = $this->helper->hasError($response)) {
            throw new InvalidArgumentException(__($error));
        }

        $hostedRes = [
            'response' => [
                'transactionId' => $this->helper->getInfo($response, 'invoice_id'),
                'orderNumber'   => $this->helper->getInfo($response, 'order_number'),
            ]
        ];

        $quote->getPayment()->setAdditionalInformation('hostedResponse', $hostedRes);
    }
}
