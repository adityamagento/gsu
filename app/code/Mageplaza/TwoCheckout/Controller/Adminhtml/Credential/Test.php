<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_TwoCheckout
 * @copyright   Copyright (c) Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\TwoCheckout\Controller\Adminhtml\Credential;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Mageplaza\TwoCheckout\Helper\Request;
use Mageplaza\TwoCheckout\Model\Source\Environment;
use Twocheckout;
use Twocheckout_Charge;
use Twocheckout_Error;

/**
 * Class Test
 * @package Mageplaza\TwoCheckout\Controller\Adminhtml\Credential
 */
class Test extends Action
{
    /**
     * @var Request
     */
    private $helper;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * Test constructor.
     *
     * @param Context $context
     * @param Request $helper
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        Request $helper,
        JsonFactory $resultJsonFactory
    ) {
        $this->helper            = $helper;
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $request = $this->getRequest();

        $txnArray = [
            'merchantOrderId' => 'Test',
            'token'           => $request->getParam('token'),
            'currency'        => 'USD',
            'total'           => '10.00',
            'billingAddr'     => [
                'name'        => 'Tester',
                'addrLine1'   => '123 Main Street',
                'city'        => 'Townsville',
                'state'       => 'Ohio',
                'zipCode'     => '43206',
                'country'     => 'USA',
                'email'       => 'example@2co.com',
                'phoneNumber' => '5555555555'
            ],
        ];

        /** @var Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        if (!class_exists('Twocheckout')) {
            $result = ['type' => 'error', 'message' => __('2Checkout library is not installed correctly')];

            return $resultJson->setData($result);
        }

        $privateKey = $request->getParam('private_key');

        Twocheckout::privateKey($privateKey === '******' ? $this->helper->getPrivateKey() : $privateKey);
        Twocheckout::sellerId($request->getParam('seller_id'));
        Twocheckout::sandbox($request->getParam('env') === Environment::SANDBOX);

        try {
            Twocheckout_Charge::auth($txnArray);

            return $resultJson->setData(['type' => 'success', 'message' => __('Credentials are valid')]);
        } catch (Twocheckout_Error $e) {
            return $resultJson->setData(['type' => 'error', 'message' => $e->getMessage()]);
        }
    }
}
