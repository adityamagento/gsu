<?php
namespace Magecomp\Ebs\Model;

class Payment extends \Magento\Payment\Model\Method\AbstractMethod
{
	protected $_code = 'ebs';
    protected $_formBlockType = 'Magecomp\Ebs\Block\Standard\Form';	
	const SECURE_EBS_URL = 'https://secure.ebs.in/pg/ma/sale/pay/';

    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;

    protected $_order = null;
	
	protected $urlBuilder;
	protected $_paymentData = null;
    protected $_moduleList;
    protected $checkoutSession;
    protected $_orderFactory;
	protected $_storeManager;
	protected $logger;
	protected $helper;
	protected $directory;
	
	public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Sales\Model\OrderFactory $orderFactory,
		\Magecomp\Ebs\Helper\Data $helper,
		\Magento\Directory\Model\Country $directory,
        \Magento\Framework\Url $urlBuilder,
        \Magento\Checkout\Model\Session $checkoutSession,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
		array $data = []
    ) {
		//$this->_paymentData = $paymentData;
		$this->urlBuilder = $urlBuilder;
		$this->_moduleList = $moduleList;
        $this->_scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
		$this->_storeManager = $storeManager;
		$this->logger = $logger;
		$this->helper = $helper;
		$this->directory =$directory;
        parent::__construct($context,
            $registry,
            $extensionFactory,			
            $customAttributeFactory,
			$paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    public function validate()
    {
        parent::validate();
        $paymentInfo = $this->getInfoInstance();		
	
        return $this;
    }

    public function capture(\Magento\Payment\Model\InfoInterface $payment,$amount){
        $payment->setStatus(self::STATUS_APPROVED)
            ->setLastTransId($this->getTransactionId());

        return $this;
    }

    public function getSecureebsUrl(){
        return self::SECURE_EBS_URL;
    }

    protected function getSuccessURL(){
		return $this->urlBuilder->getUrl('ebs/standard/success', ['_secure' => true]);
    }

    protected function getNotificationURL(){
		return $this->urlBuilder->getUrl('ebs/standard/notify', ['_secure' => true]);
    }

    protected function getFailureURL(){
		return $this->urlBuilder->getUrl('ebs/standard/failure', ['_secure' => true]);
    }

    public function getOrderPlaceRedirectUrl()
    {
		return $this->urlBuilder->getUrl('ebs/standard/redirect', ['_secure' => true]);
	}

	public function getSecureHasKey()
	{
		 $order = $this->getOrder();



        if (!($order instanceof \Magento\Sales\Model\Order)) {

            throw new \Magento\Framework\Exception\LocalizedException(__('Cannot retrieve order object'));

		}
		$transaction_key = $this->helper->getTransactionMode();
		if($transaction_key == 1) {
			$mode = "TEST";
		} else {
			$mode = "LIVE";
		}
		$orderAmount = number_format($order->getGrandTotal(), 2, '.', '');
		$hash = $this->helper->getAccountId()."|".$orderAmount."|".$order->getRealOrderId()."|".$this->urlBuilder->getUrl('ebs/standard/success',array('_secure' => true))."|".$mode;
		return $hash; 
	}

    public function getStandardCheckoutFormFields()
    {
        $order = $this->getOrder();

        if (!($order instanceof \Magento\Sales\Model\Order)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Cannot retrieve order object'));
		}

        $billingAddress = $order->getBillingAddress();
		$shippingAddress = $order->getShippingAddress();

        if ($order->getCustomerEmail()) {
            $email = $order->getCustomerEmail();
        } elseif ($billingAddress->getEmail()) {
            $email = $billingAddress->getEmail();
        } else {
            $email = '';
        }
		$billing_country = $this->directory->loadByCode($billingAddress->getCountryId(),'iso3_code');
		$shipping_country = $this->directory->loadByCode($shippingAddress->getCountryId(),'iso3_code');
		
		$fields = array(       	
        	'channel'		=> '10',
			'account_id'	=> $this->helper->getAccountId(), 
			'return_url'	=> $this->urlBuilder->getUrl('ebs/standard/success',array('_secure' => true)),
        	'reference_no'	=> $order->getRealOrderId(),
        	'amount' 		=> number_format($order->getGrandTotal(), 2, '.', ''),
        	'currency' 		=> $this->helper->getCurrency(),
			'description'	=> 'Order #'.$order->getRealOrderId(),
       		'name'			=> $billingAddress->getFirstname()." ".$billingAddress->getLastname(),
        	'address'		=> $billingAddress->getStreet()[0],
        	'city'			=> $billingAddress->getCity(),
        	'state'			=> $billingAddress->getRegion(),
        	'postal_code'	=> $billingAddress->getPostcode(),
        	'country'		=> $billing_country['iso3_code'],
        	'phone'			=> $billingAddress->getTelephone(),
        	'email'			=> $email,
			'ship_name'		=> $shippingAddress->getFirstname()." ".$shippingAddress->getLastname(),
			'ship_address'	=> $shippingAddress->getStreet()[0],
			'ship_city'		=>$shippingAddress->getCity(),
			'ship_state'	=>$shippingAddress->getRegion(),
			'ship_postal_code'	=> $shippingAddress->getPostcode(),
			'ship_country'	=> $shipping_country['iso3_code'],
			'ship_phone'	=> $shippingAddress->getTelephone()
        );
		
		return $fields;
    }
	
	public function order(\Magento\Payment\Model\InfoInterface $payment, $amount)
	{
        if (!$this->canOrder()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The order action is not available.'));
        }
        return $this;
    }
}
