<?php
namespace Magecomp\Ebs\Model\Config;

class Currency
{
    public function toOptionArray()
    {
        return [
            ['value' => 'INR', 'label' => __('Indian Rupee')],
            ['value' => 'GBP', 'label' => __('British Pound')],
            ['value' => 'AED', 'label' => __('Arab Emirates Dinar')],
            ['value' => 'OMR', 'label' => __('Omani Rial')],
			['value' => 'HKD', 'label' => __('Hong Kong Dollar')],
			['value' => 'AUD', 'label' => __('Australian Dollar')],
			['value' => 'USD', 'label' => __('American Dollar')],
			['value' => 'EUR', 'label' => __('Euro')],
			['value' => 'QAR', 'label' => __('Qatari Rial')],
			['value' => 'CAD', 'label' => __('Canadian Dollar')],
			['value' => 'SGD', 'label' => __('Singapore Dollar')],
        ];
    }
}