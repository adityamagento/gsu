<?php
namespace Magecomp\Ebs\Model\Config;

class Hashtype
{
    public function toOptionArray()
    {
        return [
            ['value'=>'SHA512', 'label'=> __('SHA512')],
            ['value'=>'SHA1', 'label'=> __('SHA1')],
            ['value'=>'MD5', 'label'=> __('MD5')]
        ];
    }
}