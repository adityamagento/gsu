<?php
namespace Magecomp\Ebs\Model\Config;

class Mode
{
    public function toOptionArray()
    {
        return [
            ['value'=>'1', 'label'=> __('TEST')],
            ['value'=>'0', 'label'=> __('LIVE')],
        ];
    }
}