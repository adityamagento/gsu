<?php
namespace Magecomp\Ebs\Model\Config;

class Language
{
    public function toOptionArray()
    {
        return [
            ['value' => 'EN', 'label' => __('English')],
            ['value' => 'RU', 'label' => __('Russian')],
            ['value' => 'NL', 'label' => __('Dutch')],
            ['value' => 'DE', 'label' => __('German')],
        ];
    }
}