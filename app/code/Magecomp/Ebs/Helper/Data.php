<?php
namespace Magecomp\Ebs\Helper;
 
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	public function __construct(\Magento\Framework\App\Helper\Context $context)
	{
		parent::__construct($context);
	}
	
	public function getTransactionMode()
	{
        return $this->scopeConfig->getValue('payment/ebs/mode',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
    
	public function getHashType()
	{
		return $this->scopeConfig->getValue('payment/ebs/hash_type',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
	public function getPageId()
	{
		return $this->scopeConfig->getValue('payment/ebs/page_id',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSecretKey()
    {
		return $this->scopeConfig->getValue('payment/ebs/secret_key',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

    public function getAccountId()
    {
        return $this->scopeConfig->getValue('payment/ebs/account_id',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

    public function getDescription()
    {
        return $this->scopeConfig->getValue('payment/ebs/description',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

    public function getNewOrderStatus()
    {
        return $this->scopeConfig->getValue('payment/ebs/order_status',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

	public function getDebug()
	{
        return $this->scopeConfig->getValue('payment/ebs/debug_flag',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCurrency()
    {
        return $this->scopeConfig->getValue('payment/ebs/currency',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getLanguage()
    {
        return $this->scopeConfig->getValue('payment/ebs/language',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
