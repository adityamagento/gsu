<?php
namespace Magecomp\Ebs\Controller\Standard;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magecomp\Ebs\Helper\Data;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
		
class Success extends \Magento\Framework\App\Action\Action
{
	protected $_order;
	protected $order;
	protected $helper;
	protected $pageFactory;
	protected $salesOrder;
	protected $orderSender;	
	protected $checkoutSession;
	protected $typeListInterface;
	protected $pool;
	
    public function __construct(Context $context, PageFactory $pageFactory, Data $helper, OrderSender $orderSender, Order $salesOrder, Session $checkoutSession,TypeListInterface $typeListInterface,Pool $pool)
    {
        $this->pageFactory = $pageFactory;
		$this->helper = $helper;
        $this->order = $salesOrder;
		$this->orderSender = $orderSender;
        $this->checkoutSession = $checkoutSession;
		$this->typeListInterface = $typeListInterface;
		$this->pool = $pool;
        return parent::__construct($context);
    }
	
	public function execute()
	{	
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$config = $this->helper;	
		$secret_key = $config->getSecretKey();
    	$hashType = $config->getHashType();		
		
    	$params = $secret_key;
    	//$response = $this->getRequest()->getPostValue();
    	$response = $this->getRequest()->getParams();
    	$secureHash = $response['SecureHash'];	
		
		$this->checkoutSession->setErrorCode($response['ResponseCode']);
        $this->checkoutSession->setErrorMessage($response['ResponseMessage']);
    	
		$_cacheTypeList = $this->typeListInterface;
       	$_cacheFrontendPool = $this->pool;
		$types = array('full_page','block_html');
       	foreach ($types as $type) {
			$_cacheTypeList->cleanType($type);
       	}
      	foreach ($_cacheFrontendPool as $cacheFrontend) {
        	$cacheFrontend->getBackend()->clean();
       	}
		
		unset($response['SecureHash']);
		ksort($response);
		foreach ($response as $key => $value){
			if (strlen($value) > 0) {
				$params .= '|'.$value;
			}
		}	
			
		if (strlen($params) > 0) {
			if($hashType == "SHA512")
				$hashValue = strtoupper(hash('SHA512',$params));	
			if($hashType == "SHA1")
				$hashValue = strtoupper(sha1($params));	
			if($hashType == "MD5")
				$hashValue = strtoupper(md5($params));	
			
		}

		$hashValid = ($hashValue == $secureHash) ? true : false;
		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
		
		$orderSender = $this->orderSender;
		$priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
		$formattedPrice = $priceHelper->currency($response['Amount'], true, false);
		if($response['ResponseCode'] == 0)
		//if($response['ResponseCode'] == 0 && $hashValid)
		{
			$this->checkoutSession->setQuoteId($this->checkoutSession->getSecureebsStandardQuoteId());
    		$this->checkoutSession->unsSecureebsStandardQuoteId();

    		$order = $this->getOrder();
			$order->setStatus($this->helper->getNewOrderStatus());

    		if (!$order->getId()) {
    			$this->norouteAction();
    			return;
    		}
			
			$msg = __('Captured amount of %1 online. Transaction ID: "%2".',$formattedPrice,$response['TransactionID']);
    		$order->addStatusToHistory($order->getStatus(),$msg);    		

    		$order->save();
			$orderSender->send($order);
			$resultRedirect->setPath('checkout/onepage/success');
    		return $resultRedirect;
		} else {
			$resultRedirect->setPath('ebs/standard/failure');
    		return $resultRedirect;
		}
	}    
		
	public function getOrder()
	{
	   $orderId = $this->checkoutSession->getLastOrderId();
	   return $this->order->load($orderId);
	}
}
