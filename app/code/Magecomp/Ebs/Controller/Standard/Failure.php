<?php
namespace Magecomp\Ebs\Controller\Standard;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use Magento\Framework\Controller\ResultFactory;
		
class Failure extends \Magento\Framework\App\Action\Action
{
	protected $_order;
	protected $order;
	protected $pageFactory;
	protected $salesOrder;
	protected $checkoutSession;
	
    public function __construct(Context $context, PageFactory $pageFactory, Order $salesOrder, Session $checkoutSession)
    {
        $this->pageFactory = $pageFactory;
        $this->order = $salesOrder;
        $this->checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }
	
	public function execute()
	{
        $errorMsg = __('Your Payment did not go through. Please try again with different payment method.');

        $order = $this->getOrder();
		
		if (!$order->getId()) {
            $this->norouteAction();
            return;
        }

        if ($order->getId()) {
            $order->addStatusToHistory($order->getStatus(), $errorMsg);
            $order->cancel();
            $order->save();
        }

		$resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
		$this->messageManager->addError($errorMsg);
        $resultRedirect->setPath('checkout/cart');
    	return $resultRedirect;
    }    
		
	public function getOrder()
	{
	   $orderId = $this->checkoutSession->getLastOrderId();
	   return $this->order->load($orderId);
	}
}
