<?php
namespace Magecomp\Ebs\Controller\Standard;

use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\App\Action\Context;
use \Magento\Sales\Model\Order;
use \Magento\Checkout\Model\Session;
use \Magecomp\Ebs\Helper\Data;
use \Magento\Framework\Controller\ResultFactory;


class Redirect extends \Magento\Framework\App\Action\Action
{
	protected $order;
	protected $pageFactory;
	protected $salesOrder;
	protected $checkoutSession;
	protected $helper;
	
    public function __construct(Context $context, PageFactory $pageFactory, Data $helper, Order $salesOrder, Session $checkoutSession)
    {
		$this->helper = $helper;
        $this->pageFactory = $pageFactory;
        $this->order = $salesOrder;
        $this->checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }
	
	public function execute()
	{
		return $this->redirect();
    }
	
	public function getOrder()
	{
	   $orderId = $this->checkoutSession->getLastOrderId();
	   return $this->order->load($orderId);
	}

    public function redirect()
    {
		$resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
		$this->checkoutSession->setSecureebsStandardQuoteId($this->checkoutSession->getQuoteId());
		
		$order = $this->getOrder();
		
		if (!$order->getId())
        {
            $this->norouteAction();
            return;
        }
		
		$order->addStatusToHistory($this->helper->getNewOrderStatus(),__('Customer was redirected to Secureebs'));
        $order->save();

        $this->getResponse()->setBody($resultPage->getLayout()->createBlock('\Magecomp\Ebs\Block\Standard\Redirect')->setOrder($order)->toHtml());
		
		$this->checkoutSession->unsQuoteId();
	}
}

