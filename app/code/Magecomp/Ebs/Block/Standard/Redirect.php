<?php
namespace Magecomp\Ebs\Block\Standard;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Customer\Model\Session;
use \Magento\Framework\ObjectManagerInterface;
use \Magecomp\Ebs\Model\Payment;
use \Magecomp\Ebs\Helper\Data;
use \Magento\Framework\Data\FormFactory;

class Redirect extends \Magento\Framework\View\Element\AbstractBlock
{
	protected $checkoutSession;
	protected $standardFactory;
	protected $_helper;
	
	protected $_methodCode = 'ebs';

	const GATEWAY_URL = "https://secure.ebs.in/pg/ma/payment/request/";
	
	public function __construct(Context $context, Payment $standardFactory, Data $helper, Session $checkoutSession, ObjectManagerInterface $objectManager,array $data = [])
	{
		$this->_helper = $helper;
		$this->standardFactory = $standardFactory;
		$this->checkoutSession = $checkoutSession;
		parent::__construct($context, $data);
	}
	
	public function getMethodCode()
	{
        return $this->_methodCode;
    }
  
	protected function _toHtml()
	{
		$secret_key = $this->_helper->getSecretKey();
		$transaction_key = $this->_helper->getTransactionMode();
		$hashType = $this->_helper->getHashType();
		$pageId = $this->_helper->getPageId();
		$standard = $this->standardFactory;
		
		if($transaction_key == 1) {
			$mode = "TEST";
			$actionUrl = self::GATEWAY_URL;
		} else {
			$mode = "LIVE";
			$actionUrl = self::GATEWAY_URL;
		}
        
		$params = $standard->setOrder($this->getOrder())->getStandardCheckoutFormFields();
		
		$params['mode'] =  $mode;
		$params['page_id'] =  $pageId;	
		ksort($params);
		$hashData = $secret_key;
		
		$secure_hash = $secret_key;
		$secure_hash .= '|'.$standard->setOrder($this->getOrder())->getSecureHasKey();
		
		foreach ($params as $key => $value){
			if (strlen($value) > 0) {
				$hashData .= '|'.$value;
			}
		}
		
		if (strlen($secure_hash) > 0) {
			if($hashType == "SHA512")
				$hashValue = hash('SHA512',$secure_hash);	
			if($hashType == "SHA1")
				$hashValue = sha1($secure_hash);	
			if($hashType == "MD5")
				$hashValue = md5($secure_hash);
		}      
       
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       	$_cacheTypeList = $objectManager->create('\Magento\Framework\App\Cache\TypeListInterface');
       	$_cacheFrontendPool = $objectManager->create('\Magento\Framework\App\Cache\Frontend\Pool');

       	$types = array('full_page');
       	foreach ($types as $type) {
			$_cacheTypeList->cleanType($type);
       	}

      	foreach ($_cacheFrontendPool as $cacheFrontend) {
        	$cacheFrontend->getBackend()->clean();
       	}
		$fields = "";
		
		foreach ($params as $key => $val)
		{	
			$fields .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />';       	
        }
        $fields .= '<input type="hidden" name="secure_hash" value="'.$hashValue.'" />';         
        $html = '<html><body><form name="ebsForm" id="ebsFormId" action="'.$actionUrl.'" method="POST">';
        $html .= 'You will be redirected to E-Billing Solutions in a few seconds.';
        $html .= $fields;
        $html .= '</form><script type="text/javascript">setTimeout(function(){document.getElementById("ebsFormId").submit()},2000)</script>';
        $html .= '</body></html>';
        
        return $html;
    }
}
