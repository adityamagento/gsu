<?php
namespace Magecomp\Ebs\Block\Standard;

use Magento\Framework\View\Element\Template;

class Failure extends Template
{
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) {
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }
    
    public function getRealOrderId()
    {
        return $this->checkoutSession->getLastRealOrderId();
    }

    public function getErrorMessage()
    {
        return $this->checkoutSession->getErrorMessage();
    }
}