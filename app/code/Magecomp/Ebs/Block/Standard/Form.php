<?php
namespace Magecomp\Ebs\Block;

class Form extends \Magento\Payment\Block\Form
{
	protected $_template = 'Magecomp_Ebs::form.phtml';
	
    protected $_methodCode = 'ebs';

	public function getMethodCode()
	{
        return $this->_methodCode;
    }
}
