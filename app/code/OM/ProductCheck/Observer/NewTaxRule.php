<?php

namespace OM\ProductCheck\Observer;

use Magento\Framework\Event\ObserverInterface;

class NewTaxRule implements ObserverInterface
{

    protected $_storeManager;

    protected $_productFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $_productFactory
    ) {
        $this->_storeManager = $storeManager;
        $this->_productFactory = $_productFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {       
            $quote = $observer->getQuote();
            $storeId = $this->_storeManager->getStore()->getId();
        
            $taxableCatsgem  = array(28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,65); 
            $taxableCatring  = array(46,47,11,9,7,12,10);  
            $taxableCoral = array(30,43);
            
            $gemArr = array(); 
            $rngArr = array();
            $coralArr = array();
            
            foreach ($quote->getAllVisibleItems() as $qItem) {
                $product = $qItem->getProduct();
                $_prod = $this->_productFactory->create()->load($product->getId());

                $categories = $_prod->getCategoryIds();
                $isGem  =  false;
                $isRing =  false;
                $isCoral = false;
                foreach($categories as $key => $category){  


                      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                      $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);                
                      $catId = $cat->getId(); 

                     
                      if( in_array($catId,$taxableCatsgem) ){
                          $isGem  =  true;
                      }
                      if( in_array($catId,$taxableCatring) ){
                          $isRing =  true;
                      }

                      if( in_array($catId,$taxableCoral) ){

                          $isCoral =  true;
                      }
                }
                
                if($isGem == true){
                    $gemArr[] = $qItem->getId();
                }
                 
                if($isRing == true){
                    $rngArr[] = $qItem->getId();
                }
                if($isCoral == true){
                    $coralArr[] = $qItem->getId();

                }

            }
           
            $finalItemArr = array();
            $finalCoralItemArr = array();
            if( (count($gemArr) > 0) && (count($rngArr) > 0) ){

                $gemNewArr    = array_slice($gemArr,0,count($rngArr));

                $finalItemArr = array_merge($gemNewArr,$rngArr);
               
            }


            if( (count($coralArr) > 0) && (count($rngArr) > 0) ){
               
               $coralNewArr         = array_slice($coralArr,0,count($rngArr));
               $finalCoralItemArr    = array_merge($coralNewArr, $rngArr); 

               /*var_dump($finalCoralItemArr); die();*/

            }
            
            foreach ($quote->getAllVisibleItems() as $qItem) {

                $totalPrice = $qItem->getPrice() * $qItem->getQty();
                
                if( in_array($qItem->getId(),$finalItemArr) ){                  
                    
                    $qItem->getProduct()->setTaxClassId(18);
                    /*$qItem->setTaxPercent(12);
                    $rowTotal = $qItem->getRowTotal();
                    $qItem->setTaxAmount($rowTotal*0.12);*/
                    $qItem->save();
                    /*$qItem->setPriceInclTax((float)$qItem->getPrice());
                    $qItem->setBasePriceInclTax((float)$qItem->getBasePrice());
                    $qItem->setBaseRowTotalInclTax((float)$qItem->getBaseRowTotal());*/
                    
                }

               if( in_array($qItem->getId(), $finalCoralItemArr) ){ 
                $qItem->getProduct()->setTaxClassId(39);
                $qItem->save();  

               } 
            }
            $quote->save();  
    }
}   
