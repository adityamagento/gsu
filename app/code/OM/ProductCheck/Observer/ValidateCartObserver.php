<?php
namespace OM\ProductCheck\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Cart as CustomerCart;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Event\Observer;
use Magento\Quote\Model\QuoteRepository;

class ValidateCartObserver implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var RedirectInterface
     */
    protected $redirect;

    /**
     * @var Cart
     */
    protected $cart;
    protected $quoteRepository;

    /**
     * @param ManagerInterface $messageManager
     * @param RedirectInterface $redirect
     * @param CustomerCart $cart
     */
    public function __construct(
        ManagerInterface $messageManager,
        RedirectInterface $redirect,
        CustomerCart $cart,
        QuoteRepository $quoteRepository

    ) {
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->cart = $cart;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Validate Cart Before going to checkout
     * - event: controller_action_predispatch_checkout_index_index
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $quote = $this->cart->getQuote();
        $controller = $observer->getControllerAction();
        $cartItemsQty = $quote->getItemsQty();
        $totalItems=count($quote->getAllItems());
         $quoteId = $quote->getId();
         $items = $quote->getAllVisibleItems();
         $valid=false;
        foreach ($items as $item) {
           
            $sku = $item->getsku(); 

            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productCollection = $objectManager->create('Magento\Catalog\Model\Product')->loadByAttribute('sku', $sku);
            $productPriceBySku = $productCollection->getPrice();
            $categories = $productCollection->getCategoryIds();
              $product_id = $item->getProductId();

            foreach($categories as $key => $category){
                
              if($key==0){ 
                  $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);
                  
                    $categoryId = $cat->getId();  
                } 
            }

             $product_id = $item->getProductId();  
             if($product_id =='13118' OR $product_id=='12854'){
               $valid=true;
             }

            if(in_array($categoryId,array(28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,4,65))){
                
                    $valid=true;
             } 
        }  
             if($valid===false && $totalItems > 0){
                $this->messageManager->addNoticeMessage(
                __('Can not Checkout only this item please add one item of gem!')
            );
            $this->redirect->redirect($controller->getResponse(), 'checkout/cart');
                       
            } 
    }
}

?>
        


    