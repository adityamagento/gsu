<?php

namespace OM\ProductCheck\Observer;

use Magento\Framework\Event\ObserverInterface;

class BeforePlaceOrderObserver implements ObserverInterface
{

    protected $_storeManager;

    protected $_productFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $_productFactory
    ) {
        $this->_storeManager = $storeManager;
        $this->_productFactory = $_productFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {       
			$quote = $observer->getOrder();
            $orderGrandTotal = $quote->getGrandTotal();
            //$quote->setGrandTotal($orderGrandTotal);
            //$quote->setGrandTotal($orderGrandTotal);
			$storeId = $this->_storeManager->getStore()->getId();
        
            $taxableCatsgem  = array(28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44); 
            $taxableCatring  = array(46,47,11,9,7,12,10);  
            
            $gemArr = array(); 
            $rngArr = array();
            
            foreach ($quote->getAllVisibleItems() as $qItem) {
				$product = $qItem->getProduct();
                $_prod = $this->_productFactory->create()->load($product->getId());

                $categories = $_prod->getCategoryIds();
                $isGem  =  false;
                $isRing =  false;
                foreach($categories as $key => $category){              
                      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                      $cat = $objectManager->create('Magento\Catalog\Model\Category')->load($category);                
                      $catId = $cat->getId(); 

                     
                      if( in_array($catId,$taxableCatsgem) ){
						  $isGem  =  true;
					  }
					  if( in_array($catId,$taxableCatring) ){
						  $isRing =  true;
					  }
                }
                
                if($isGem == true){
					$gemArr[] = $qItem->getId();
				}
				 
				if($isRing == true){
					$rngArr[] = $qItem->getId();
				}
                

			}
           
			$finalItemArr = array();
			if( (count($gemArr) > 0) && (count($rngArr) > 0) ){

				$gemNewArr    = array_slice($gemArr,0,count($rngArr));

				$finalItemArr = array_merge($gemNewArr,$rngArr);
                //$quote->setCustomerTaxClassId(18);

                $quote->save();
			}
			
              
    }
}   
