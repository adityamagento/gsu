<?php
namespace OM\ProductSorting\Plugin\Model;

use Magento\Store\Model\StoreManagerInterface;

class Config
{
       const BESTSELLERS_SORT_BY = 'bestsellers';


public function afterGetAttributeUsedForSortByArray($subject, $result)
    {
        $options = array();
           /* $options['position'] = __('Position');*/
            $options['newest'] = __('Default');
            
            
        foreach ($subject->getAttributesUsedForSortBy() as $attribute) {
            
            $options[$attribute['attribute_code']] = $attribute['frontend_label'];
        }
        unset($options['created_at']);
        return $options;
        
    }
}
