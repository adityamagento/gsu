<?php
namespace OM\ProductSorting\Plugin;
 
class Config
{
	public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
	{
    	$optionsnew = ['latest' => __('New Products')];
    	$options = array_merge($options, $optionsnew);
    	return $options;
	}
}