<?php
namespace OM\ProductSorting\Plugin\Product\ProductList;
class Toolbar
{
     const NEWEST_SORT_BY = 'newest';
  
    /**
     * Around Plugin
     *
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Data\Collection $collection
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        \Closure $proceed,
        $collection
    ) {
        $currentOrder = $subject->getCurrentOrder();
        $currentDirection = $subject->getCurrentDirection();
        $result = $proceed($collection);
  
        if ($currentOrder == self::NEWEST_SORT_BY) {
            if ($currentDirection == 'desc') {
                $subject->getCollection()->setOrder('created_at', 'asc');
            } 
            else {
                $subject->getCollection()->setOrder('created_at', 'desc');
            }
        }
  
        return $result;
    }
}