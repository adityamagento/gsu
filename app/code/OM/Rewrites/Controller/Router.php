<?php
namespace OM\Rewrites\Controller;

use Magento\Framework\Module\Manager;

class Router implements \Magento\Framework\App\RouterInterface
{
    /** @var \Magento\Framework\App\ActionFactory */
    protected $_actionFactory;

    /** @var \Magento\Framework\App\ResponseInterface */
    protected $_response;

    /** @var  Manager */
    protected $_moduleManager;

    /** @var  Page */
    protected $_page;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory, 
        \Magento\Framework\App\ResponseInterface $response, 
        Manager $moduleManager,
        \Magento\Cms\Model\Page $page
    )
    {   //exit('php test');
        $this->_actionFactory = $actionFactory;
        $this->_response = $response;
        $this->_moduleManager = $moduleManager;
        $this->_page = $page;
    }

 echo "string"; exit();
    public function match(\Magento\Framework\App\RequestInterface $request)
    {

        echo $request->getPathInfo(); exit();
        $identifier = trim($request->getPathInfo() , '/');
        $identifier = current(explode("/", $identifier));
        
        $identifier = rtrim($identifier,'.php');

        $model = $this->_page->load($identifier, 'identifier');
        if ($pageId = $model->getPageId())
        {
            $request->setModuleName('cms')
                ->setControllerName('page')
                ->setActionName('view')
                ->setParam('page_id', $pageId);
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
            return $this
                ->_actionFactory
                ->create('Magento\Framework\App\Action\Forward');
        }
    }
}
