<?php
namespace OM\Homepage\Block;
use Magento\Framework\ObjectManagerInterface;
class Homepage extends \Magento\Framework\View\Element\Template
{    
    protected $_productCollectionFactory;
    protected $_imageBuilder;
    protected $_productImageHelper;
    protected $objectManager;
    protected $categoryFactory;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $_imageBuilder,
        \Magento\Catalog\Model\Product\OptionFactory $_optionFactory,
        
        \Magento\Catalog\Helper\Image $productImageHelper,
        ObjectManagerInterface $objectManager,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_imageBuilder = $_imageBuilder;
        $this->_productImageHelper = $productImageHelper;
        $this->objectManager = $objectManager;
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context, $data);
    }
    
     public function getMediaUrl(){

            $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            return $media_dir;
        }

   public function getProductImage(){
       $product_image = $this->objectManager->get('\Magento\Catalog\Helper\Image');
   }
    public function getCategoryProduct($categoryId)
    {
        $category = $this->categoryFactory->create()->load($categoryId)->getProductCollection()->addAttributeToSelect('*')
            ->setPageSize(5)
            ->setCurPage(1);
        return $category;
    }

   /* public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('is_homepage', 1);
        $collection->setOrder('product_position', 'ASC');
        /*echo $collection->getSelect();
        exit();*/
        /*$collection->setPageSize(20);
        return $collection;
    }*/

    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->_imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }

    public function getAddToCartUrl($product)
    {
        $params = [
            'product' => $product->getId(),
            '_secure' => $this->getRequest()->isSecure()
        ];

        return $this->getUrl('checkout/cart/add', $params);
    }

    public function getOptions($product)
    {
        return $this->_optionFactory->create()->getProductOptionCollection($product);
    }
}
?>