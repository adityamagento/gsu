<?php

namespace OM\ProductData\Block\Product;

class Review extends \Magento\Review\Block\Product\Review
{
    /**
     * Set tab title
     *
     * @return void
     */
    public function setTabTitle()
    {
        $title = $this->getCollectionSize()
            ? __('Customers Testimonials %1', '<span class="counter">' . $this->getCollectionSize() . '</span>')
            : __('Customers Testimonials');
        $this->setTitle($title);
    }
}