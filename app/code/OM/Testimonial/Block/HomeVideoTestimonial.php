<?php
/**
 * @category   OM
 * @package    OM_Testimonial
 * @author     kumar.dhananjay@orangemantra.in
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace OM\Testimonial\Block;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\Template\Context;
use OM\Testimonial\Model\TestimonialFactory;
/**
 * Testimonial  block
 */
class HomeVideoTestimonial extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Testimonial
     */
    protected $objectManager;
    protected $_testimonial;
    public function __construct(Context $context, ObjectManagerInterface $objectManager, TestimonialFactory $testimonial)
    {
        $this->objectManager = $objectManager;
        $this->_testimonial  = $testimonial;
        
        parent::__construct($context);
    }
    
    public function getMediaUrl()
    {
        
        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        
        return $media_dir;
    }
    
   public function getHomeTestimonialCollectionVideo()
    {
        
        $testimonial = $this->_testimonial->create();
        $collection  = $testimonial->getCollection();
        $collection->addFieldToFilter('status', '1');
        $collection->addFieldToFilter('home_page', '1'); 
        $collection->addFieldToFilter('category', 'video');
        $collection->setOrder('position','ASC');
        $collection->setPageSize(5);
        return $collection;
    }
    
    
} 