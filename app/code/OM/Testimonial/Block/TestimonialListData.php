<?php
/**
 * @category   OM
 * @package    OM_Testimonial
 * @author     kumar.dhananjay@orangemantra.in
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace OM\Testimonial\Block;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\Element\Template\Context;
use OM\Testimonial\Model\TestimonialFactory;
/**
 * Testimonial List block
 */
class TestimonialListData extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Testimonial
     */
    protected $objectManager;
    protected $_testimonial;
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        TestimonialFactory $testimonial
    ) {
         $this->objectManager = $objectManager;
        $this->_testimonial = $testimonial;

        parent::__construct($context);
    }

    public function getMediaUrl(){

            $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            return $media_dir;
        }

    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('OM Testimonial Module List Page'));
        
        if ($this->getTestimonialCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'om.testimonial.pager'
            )->setAvailableLimit(array(5=>5,10=>10,15=>15))->setShowPerPage(true)->setCollection(
                $this->getTestimonialCollection()
            );
            $this->setChild('pager', $pager);
            $this->getTestimonialCollection()->load();
        }
        return parent::_prepareLayout();
    }

    public function getTestimonialVideoCollection()
    {
        $page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 5;

        $testimonial = $this->_testimonial->create();
        $collection = $testimonial->getCollection();
        $collection->addFieldToFilter('status','1');
         $collection->addFieldToFilter('category','0');
        //$testimonial->setOrder('testimonial_id','ASC');
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

  public function getTestimonialTextCollection(){
    $testimonial =$this->_testimonial->create();
    $collection = $testimonial->getCollection();
    $collection->addFieldToFilter('status','1');
    $collection->addFieldToFilter('category','1');
  }


    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}