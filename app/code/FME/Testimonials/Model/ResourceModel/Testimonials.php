<?php

namespace FME\Testimonials\Model\ResourceModel;

class Testimonials extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
        
    protected $_storeManager;
        
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_storeManager = $storeManager;
    }
           
    protected function _construct()
    {
        $this->_init('fme_testimonials', 'testimonial_id');
    }
    protected function _beforeDelete(\Magento\Framework\Model\AbstractModel $object)
    {
        $condition = ['testimonial_id = ?' => (int)$object->getId()];

        $this->getConnection()->delete($this->getTable('fme_testimonials_store'), $condition);

        return parent::_beforeDelete($object);
    }
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());

            $object->setData('store_id', $stores);
        }
            
        return parent::_afterLoad($object);
    }
    protected function _afterSave(
        \Magento\Framework\Model\AbstractModel $object
    ) {
            
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();
        if (empty($newStores)) {
            $newStores = (array)$object->getStoreId();
        }
        $table = $this->getTable('fme_testimonials_store');
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);

        if ($delete) {
            $where = ['testimonial_id = ?' => (int)$object->getId(), 'store_id IN (?)' => $delete];

            $this->getConnection()->delete($table, $where);
        }

        if ($insert) {
            $data = [];

            foreach ($insert as $storeId) {
                $data[] = ['testimonial_id' => (int)$object->getId(), 'store_id' => (int)$storeId];
            }

            $this->getConnection()->insertMultiple($table, $data);
        }
            
        return parent::_afterSave($object);
    }
    protected function isNumericIdentifier(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[0-9]+$/', $object->getData('identifier'));
    }
    protected function isValidIdentifier(\Magento\Framework\Model\AbstractModel $object)
    {
        return preg_match('/^[a-z0-9][a-z0-9_\/-]+(\.[a-z0-9_-]+)?$/', $object->getData('identifier'));
    }
    public function lookupStoreIds($testimonialid)
    {
        $connection = $this->getConnection();

        $select = $connection->select()->from(
            $this->getTable('fme_testimonials_store'),
            'store_id'
        )->where(
            'testimonial_id = ?',
            (int)$testimonialid
        );

        return $connection->fetchCol($select);
    }
    public function getIsUniqueTestimonialToStores(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($this->_storeManager->hasSingleStore()) {
            $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID];
        } else {
            $stores = (array)$object->getData('stores');
        }

        $select = $this->getConnection()->select()->from(
            ['ft' => $this->getMainTable()]
        )->join(
            ['fts' => $this->getTable('fme_testimonials_store')],
            'ft.testimonial_id = fts.testimonial_id',
            []
        )->where(
            'ft.identifier = ?',
            $object->getData('identifier')
        )->where(
            'fts.store_id IN (?)',
            $stores
        );

        if ($object->getId()) { //in edit mode, compare other then current
            $select->where('ft.testimonial_id <> ?', $object->getId());
        }

        if ($this->getConnection()->fetchRow($select)) {
            return false;
        }

        return true;
    }
    public function checkIdentifier($identifier, $storeId)
    {
        $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID, $storeId];
        $select = $this->_getLoadByIdentifierSelect($identifier, $stores, 1);
        $select->reset(
            \Magento\Framework\DB\Select::COLUMNS
        )->columns(
            'ft.testimonial_id'
        )->order(
            'fts.store_id DESC'
        )->limit(1);

        return $this->getConnection()->fetchOne($select);
    }
    protected function _getLoadByIdentifierSelect($identifier, $store, $isActive = null)
    {
        $select = $this->getConnection()->select()->from(
            ['ft' => $this->getMainTable()]
        )->join(
            ['fts' => $this->getTable('fme_testimonials_store')],
            'ft.testimonial_id = fts.testimonial_id',
            []
        )->where(
            'ft.identifier = ?',
            $identifier
        )->where(
            'fts.store_id IN (?)',
            $store
        );

        if (!is_null($isActive)) {
            $select->where('ft.status = ?', $isActive);
        }

        return $select;
    }
}
