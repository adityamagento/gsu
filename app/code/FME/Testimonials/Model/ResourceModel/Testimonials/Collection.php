<?php

namespace FME\Testimonials\Model\ResourceModel\Testimonials;

use \FME\Testimonials\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'testimonial_id';

    protected $_previewFlag;

    protected function _construct()
    {
        $this->_init('FME\Testimonials\Model\Testimonials', 'FME\Testimonials\Model\ResourceModel\Testimonials');
        $this->_map['fields']['testimonial_id'] = 'main_table.testimonial_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }

    protected function _afterLoad()
    {
        $this->performAfterLoad('fme_testimonials_store', 'testimonial_id');
        $this->_previewFlag = false;

        return parent::_afterLoad();
    }

    protected function _renderFiltersBefore()
    {
        $this->joinStoreRelationTable('fme_testimonials_store', 'testimonial_id');
    }
}
