<?php

namespace FME\Testimonials\Model\Config\Source;

class Slidereffect implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'auto', 'label' => __('Auto Slide')],
                ['value' => 'manual', 'label' => __('Manual Slide')]];
    }

    public function toArray()
    {
        return ['auto' => __('Auto Slide'),
        'manual' => __('Manual Slide')];
    }
}
