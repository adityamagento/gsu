<?php

namespace FME\Testimonials\Model\Config\Source;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'Enable', 'label' => __('Enable')],
                ['value' => 'Disable', 'label' => __('Disable')]];
    }
}
