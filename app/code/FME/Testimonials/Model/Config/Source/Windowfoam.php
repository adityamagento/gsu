<?php

namespace FME\Testimonials\Model\Config\Source;

class Windowfoam implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'slide', 'label' => __('Slide')],
                ['value' => 'newpage', 'label' => __('New Page')],
                ['value' => 'popup', 'label' => __('Popup')]
                ];
    }

    public function toArray()
    {
        return ['slide' => __('Slide'),'newpage' => __('New Page'),'popup' => __('Popup') ];
    }
}
