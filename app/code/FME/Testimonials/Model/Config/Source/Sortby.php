<?php

namespace FME\Testimonials\Model\Config\Source;

class Sortby implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'latest', 'label' => __('Latest')],
                ['value' => 'giverorder', 'label' => __('Given Order')]];
    }

    public function toArray()
    {
        return ['latest' => __('Latest'), 'giverorder' => __('Given Order')];
    }
}
