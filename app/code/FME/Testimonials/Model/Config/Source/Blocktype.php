<?php

namespace FME\Testimonials\Model\Config\Source;

class Blocktype implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'random', 'label' => __('Random Testimonials')],
                ['value' => 'featured', 'label' => __('Featured Testimonials')]
                ];
    }
}
