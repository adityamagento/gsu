<?php

namespace FME\Testimonials\Model\Config\Source;

class Emailsender implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'generalcontact', 'label' => __('General Contact')],
                ['value' => 'sale', 'label' => __('Sales Representative')],
                ['value' => 'customersupport', 'label' => __('Customer Support')],
                ['value' => 'email1', 'label' => __('Customer Email 1')],
                 ['value' => 'email2', 'label' => __('Customer Email 2')]];
    }

    public function toArray()
    {
        return ['generalcontact' => __('General Contact'),
        'sale' => __('Sales Representative'),
        'customersupport' => __('Customer Support'),
        'email1' => __('Customer Email 1'),
        'email2' => __('Customer Email 2')];
    }
}
