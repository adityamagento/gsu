<?php

namespace FME\Testimonials\Model\Config\Source;

class Moderatortemplate implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'moderatortemplate',
        'label' => __('Testimonials Moderator Notification(Deafult from Locale)')]];
    }
    public function toArray()
    {
        return ['moderatortemplate' => __('Testimonials Moderator Notification(Deafult from Locale)')];
    }
}
