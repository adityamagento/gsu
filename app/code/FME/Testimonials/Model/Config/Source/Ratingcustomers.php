<?php

namespace FME\Testimonials\Model\Config\Source;

class Ratingcustomers implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'all', 'label' => __('All')],
                ['value' => 'guests', 'label' => __('Only Guests')],
                ['value' => 'registered', 'label' => __('Only Registered')],
                ['value' => 'none', 'label' => __('None')]];
    }
}
