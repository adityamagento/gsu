<?php

namespace FME\Testimonials\Model\Config\Source;

class Testimonialsallow implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'guest', 'label' => __('Guests')],
                ['value' => 'register', 'label' => __('Registered')],
                ['value' => 'no', 'label' => __('No')]];
    }

    public function toArray()
    {
        return ['guest' => __('Guests'),'register' => __('Registered'), 'no' => __('No')];
    }
}
