<?php

namespace FME\Testimonials\Model\Config\Source;

class Clienttemplate implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [['value' => 'clienttemplate', 'label' => __('Testimonials Client Notification(Deafult from Locale)')]];
    }

    public function toArray()
    {
        return ['clienttemplate' => __('Testimonials Client Notification(Deafult from Locale)')];
    }
}
