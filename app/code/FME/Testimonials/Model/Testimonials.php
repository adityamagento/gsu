<?php

namespace FME\Testimonials\Model;

class Testimonials extends \Magento\Framework\Model\AbstractModel
{
    protected $storeManager;
    protected $_objectManager;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
            
        $this->storeManager = $storeManager;
        $this->_objectManager = $objectManager;
            
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
        
    protected function _construct()
    {
        $this->_init('FME\Testimonials\Model\ResourceModel\Testimonials');
    }
        
    public function getAvailableStatuses()
    {
        $availableOptions = ['1' => 'Enable',
                          '0' => 'Disable'];
        return $availableOptions;
    }
        
    public function getTestimonialsList()
    {
            
        $collection = $this->getCollection()->addFieldToFilter('status', 1);
        $testimonialList = [];
            
        foreach ($collection as $data) {
            $testimonialList[$data->getId()] = $data->getTitle();
        }
            
        return $testimonialList;
    }

    public function getTestimonials()
    {
        $testimonial = [];
        $collection = $this->getCollection()->addFieldToFilter('status', 1);
        $testimonialList = [];
        $i=0;
        foreach ($collection as $data) {
            $testimonial[$i] =['value' => $data->gettestimonial_id(), 'label' => __($data->getTitle())];
            $i++;
        }
        return $testimonial;
    }
}
