<?php

namespace FME\Testimonials\Model\Testimonials\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Yesno implements OptionSourceInterface
{
    public function toOptionArray()
    {
        
        $availableOptions = ['1' => 'Yes', '0' => 'No'];
        
        $options = [];
        foreach ($availableOptions as $key => $label) {
            $options[] = [
                'label' => $label,
                'value' => $key,
            ];
        }
        return $options;
    }
}
