<?php

namespace FME\Testimonials\Helper;

use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_TESTIMONIALS_ENABLE           ='Testimonials/general/testimonials_mod_enable';
    const XML_TESTIMONIALS_HEADER           ='Testimonials/general/testimonial_header';
    const XML_TESTIMONIALS_HEADER_DESC      ='Testimonials/general/testimonial_header_desc';
    const XML_TESTIMONIALS_PAGE_TITLE       ='Testimonials/testimonials_list/page_title';
    const XML_TESTIMONIALS_IDENTIFIER       ='Testimonials/testimonials_list/url_identifier';
    const XML_TESTIMONIALS_PER_PAGE         ='Testimonials/testimonials_list/testimonials_per_page';
    const XML_TESTIMONIALS_KEYWORDS         ='Testimonials/testimonials_list/testimonials_meta_keywords';
    const XML_TESTIMONIALS_DESCRIPTION      ='Testimonials/testimonials_list/testimonials_meta_description';
    const XML_TESTIMONIALS_READ_MORE_LINK   ='Testimonials/testimonials_list/is_testimonial_link';
    const XML_TESTIMONIALS_IS_SHOW_IMAGE    ='Testimonials/testimonials_list/show_image';
    const XML_TESTIMONIALS_SORTBY           ='Testimonials/testimonials_list/testimonial_sotyby';
    const XML_CHARACTER_PER_TESTIMONIAL     ='Testimonials/testimonials_list/character_per_testimonials';
    const XML_TEST_LIST_HEADING             ='Testimonials/testimonials_list/list_title';
   
    
    const XML_T_SETTING_CUSTOMER_TYPE       ='Testimonials/add_testimonials_setting/customer_type';
    const XML_T_SETTING_ADMIN_APPROVAL      ='Testimonials/add_testimonials_setting/is_admin_approval';
    const XML_T_SETTING_WIN_FORM            ='Testimonials/add_testimonials_setting/window_form';
    const XML_T_DEFAULT_IMAGE               ='Testimonials/add_testimonials_setting/upload_image_id';
    const XML_T_DEFAULT_IMAGE_BASE          ='Testimonials/add_testimonials_setting/upload_image_id/media';
    const XML_T_BUTTON_NAME                 ='Testimonials/add_testimonials_setting/testimonial_button_text';
    const XML_T_FORM_HEADING                 ='Testimonials/add_testimonials_setting/testimonial_form_heading';




    const CONFIG_CAPTCHA_ENABLE = 'Testimonials/google_options/captchastatus';
    const CONFIG_CAPTCHA_PRIVATE_KEY = 'Testimonials/google_options/googleprivatekey';
    const CONFIG_CAPTCHA_PUBLIC_KEY = 'Testimonials/google_options/googlepublickey';
    const CONFIG_CAPTCHA_THEME = 'Testimonials/google_options/theme';
    const XML_SHOW_COMPANY_NAME             ='Testimonials/testimonials_form_fields/is_show_companyname';
    const XML_SHOW_CONTACT_NAME             ='Testimonials/testimonials_form_fields/is_show_contactname';
    const XML_SHOW_EMAIL                    ='Testimonials/testimonials_form_fields/is_show_email';
    const XML_SHOW_WEBSITE                  ='Testimonials/testimonials_form_fields/is_show_website';
    const XML_SHOW_PHOTO                    ='Testimonials/testimonials_form_fields/is_show_photo';
    const XML_SHOW_DESCRIPTION              ='Testimonials/testimonials_form_fields/is_show_desc';
    const XML_BLOCK_ENABLE                  ='Testimonials/testimonials_block_setting/enable_block';
    const XML_BLOCK_TYPE                    ='Testimonials/testimonials_block_setting/block_type';
    const XML_BLOCK_TITLE                   ='Testimonials/testimonials_block_setting/block_title';
    const XML_BLOCK_NO_OF_TESTIMONIALS      ='Testimonials/testimonials_block_setting/numbe_of_testimonials';
    const XML_BLOCK_ALLOW_LINK              ='Testimonials/testimonials_block_setting/allow_link';
    const XML_BLOCK_SLIDER_EFFECT           ='Testimonials/testimonials_block_setting/slider_effect';
    const XML_BLOCK_SLIDER_DURATION         ='Testimonials/testimonials_block_setting/slider_duration';
    const XML_BLOCK_PAGINATION              ='Testimonials/testimonials_block_setting/allow_pagination';
    const XML_CHARACTER_PER_BLOCK           ='Testimonials/testimonials_block_setting/character_per_testimonialblock';
    const XML_EMAIL_SENDER                  ='Testimonials/email_setiing/email_sender';
    const XML_EMAIL_ENABLE_MODERATOR        ='Testimonials/email_setiing/enable_notification_moderator';
    const XML_EMAIL_MODERATOR               ='Testimonials/email_setiing/moderator_email';
    const XML_EMAIL_MODERATOR_SUBJECT       ='Testimonials/email_setiing/moderator_subject';
    const XML_EMAIL_MODERATOR_TEMPLATE      ='Testimonials/email_setiing/moderator_template';
    const XML_EMAIL_ENABLE_CLIENT           ='Testimonials/email_setiing/enable_notification_clint';
    const XML_EMAIL_CLIENT_SUBJECT          ='Testimonials/email_setiing/client_subject';
    const XML_EMAIL_CLIENT_TEMPLATE         ='Testimonials/email_setiing/client_template';
    const XML_CUSTOM_FIELD_LABEL_1          ='Testimonials/testimonials_custom_form_fields/custom_field_1_label';
    const XML_ENABLE_CUSTOM_FIELD_1         ='Testimonials/testimonials_custom_form_fields/enable_custom_field_1';
    const XML_CUSTOM_FIELD_LABEL_2          ='Testimonials/testimonials_custom_form_fields/custom_field_2_label';
    const XML_ENABLE_CUSTOM_FIELD_2         ='Testimonials/testimonials_custom_form_fields/enable_custom_field_2';
    const XML_CUSTOM_FIELD_LABEL_3          ='Testimonials/testimonials_custom_form_fields/custom_field_3_label';
    const XML_ENABLE_CUSTOM_FIELD_3         ='Testimonials/testimonials_custom_form_fields/enable_custom_field_3';
    const XML_CUSTOM_FIELD_LABEL_4          ='Testimonials/testimonials_custom_form_fields/custom_field_4_label';
    const XML_ENABLE_CUSTOM_FIELD_4         ='Testimonials/testimonials_custom_form_fields/enable_custom_field_4';
    const XML_TESTIMONIALS_URL_SUFFIX       ='Testimonials/testimonials_seo/url_suffix';
    
    const XML_TESTIMONIALS_FOOTER_ENABLE    ='Testimonials/footer_links/footer_link_enable';
    const XML_TESTIMONIALS_FOOTER_NAME      ='Testimonials/footer_links/footer_title';
    
    
    
    
    protected $_storeManager;
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        
        $this->_storeManager = $storeManager;
        
        parent::__construct($context);
    }
    public function isEnabledInFrontend()
    {
        $isEnabled = true;
        $enabled = $this->scopeConfig->getValue(self::XML_TESTIMONIALS_ENABLE, ScopeInterface::SCOPE_STORE);
        if ($enabled == null || $enabled == '0') {
            $isEnabled = false;
        }
        return $isEnabled;
    }
    
   public function getFormHeading()
    {
        
        return $this->scopeConfig->getValue(self::XML_T_FORM_HEADING);
    }
   public function getFooterLinkName()
    {
        
        return $this->scopeConfig->getValue(self::XML_TESTIMONIALS_FOOTER_NAME);
    }
    public function isFooterLinkEnable()
    {
        
        return $this->scopeConfig->getValue(self::XML_TESTIMONIALS_FOOTER_ENABLE);
    }
    public function getTestimonialsListHeading()
    {
        
        return $this->scopeConfig->getValue(self::XML_TEST_LIST_HEADING);
    }
    public function getTestimonialsHeaderContent()
    {
        
        return $this->scopeConfig->getValue(self::XML_TESTIMONIALS_HEADER);
    }
    public function getTestimonialsHeaderDescContent()
    {
        
        return $this->scopeConfig->getValue(self::XML_TESTIMONIALS_HEADER_DESC);
    }

    public function getCharacterPerTestimonials()
    {
        
        return $this->scopeConfig->getValue(self::XML_CHARACTER_PER_TESTIMONIAL);
    }
    public function getTestimonialButtonName()
    {
        
        return $this->scopeConfig->getValue(self::XML_T_BUTTON_NAME);
    }

    public function getCharacterPerBlock()
    {
        
        return $this->scopeConfig->getValue(self::XML_CHARACTER_PER_BLOCK);
    }
    public function isCaptchaEnable()
    {
        
        return $this->scopeConfig->getValue(self::CONFIG_CAPTCHA_ENABLE);
    }
    
    public function getPrivateKey()
    {
        
        return $this->scopeConfig->getValue(self::CONFIG_CAPTCHA_PRIVATE_KEY);
    }
    
    public function getPublicKey()
    {
        
        return $this->scopeConfig->getValue(self::CONFIG_CAPTCHA_PUBLIC_KEY);
    }
    
    public function getCaptchaTheme()
    {
        
        return $this->scopeConfig->getValue(self::CONFIG_CAPTCHA_THEME);
    }
    
    public function getCustomFieldLabel1()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_CUSTOM_FIELD_LABEL_1,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function enableCustomField1()
    {
         return $this->scopeConfig->getValue(
             self::XML_ENABLE_CUSTOM_FIELD_1,
             \Magento\Store\Model\ScopeInterface::SCOPE_STORE
         );
    }
    public function getDefaultImageBaseUrl()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_T_DEFAULT_IMAGE_BASE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getDefaultImageUrl()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_T_DEFAULT_IMAGE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getCustomFieldLabel2()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_CUSTOM_FIELD_LABEL_2,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function enableCustomField2()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_ENABLE_CUSTOM_FIELD_2,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getCustomFieldLabel3()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_CUSTOM_FIELD_LABEL_3,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function enableCustomField3()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_ENABLE_CUSTOM_FIELD_3,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getCustomFieldLabel4()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_CUSTOM_FIELD_LABEL_4,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function enableCustomField4()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_ENABLE_CUSTOM_FIELD_4,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getSEOUrl()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_URL_SUFFIX,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getTestimonialSortOder()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_SORTBY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getTestimonialPageIdentifier()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_IDENTIFIER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getTestimonialPageMetaKeywords()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_KEYWORDS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
    public function getTestimonialPageMetaDesc()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_DESCRIPTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
    public function allowReadMoreLink()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_READ_MORE_LINK,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
    public function getNumOfTestimonialsForTestimonialPage()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_PER_PAGE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getEmailClientTemplate()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_CLIENT_TEMPLATE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getEmailClientSubject()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_CLIENT_SUBJECT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getEmailModeratorTemplate()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_MODERATOR_TEMPLATE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getEmailModeratorSubject()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_MODERATOR_SUBJECT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getEmailModerator()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_MODERATOR,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function enableClientNotification()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_ENABLE_CLIENT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function enableModeratorNotification()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_ENABLE_MODERATOR,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getTestimonialsLink()
    {
        $identifier = $this->getTestimonialPageIdentifier();
        $seo_suffix = $this->getSEOUrl();
        if (isset($identifier) && isset($seo_suffix)) {
            return $identifier.$seo_suffix;
        }else if(isset($identifier))
        {
              return $identifier;
        }
        else {
            return 'testimonials';
        }
    }
    public function getEmailSender()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_EMAIL_SENDER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isAllowPaginationInBlock()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_PAGINATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getBlockSliderDuration()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_SLIDER_DURATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getBlockSliderEffect()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_SLIDER_EFFECT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isAllowLinkInBlock()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_ALLOW_LINK,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getNumberofTestimonialOnBlock()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_NO_OF_TESTIMONIALS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getBlockTitle()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getBlockType()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_TYPE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableBlock()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_BLOCK_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableContactDesc()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_SHOW_DESCRIPTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableContactPhoto()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_SHOW_PHOTO,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableContactWebsite()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_SHOW_WEBSITE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableContactEmail()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_SHOW_EMAIL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableContactName()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_SHOW_CONTACT_NAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function isEnableCompanyName()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_SHOW_COMPANY_NAME,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function enableAdminPersmission()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_T_SETTING_ADMIN_APPROVAL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getWindowForm()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_T_SETTING_WIN_FORM,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getCustomerType()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_T_SETTING_CUSTOMER_TYPE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function isAllowImageWithTestimonials()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_IS_SHOW_IMAGE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function getTestimonialPageTitle()
    {
        
        return $this->scopeConfig->getValue(
            self::XML_TESTIMONIALS_PAGE_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function seoUrl($string)
    {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }
    public function getIdFromUrl($url)
    {
        $pos = strpos($url, "_");
        return substr($url, $pos+1, strlen($url));
    }
    public function getDetailUrl($companyName, $TestimonilaId)
    {
        return $this->seoUrl($companyName).'_'.$TestimonilaId;
    }
    public function getTestimonialsFinalDetailIdentifier($detailId)
    {
        if ($this->getTestimonialPageIdentifier()) {
            return $this->getTestimonialPageIdentifier().'/'.$detailId.$this->getSEOUrl();
        } else {
            return 'testimonials/'.$detailId.$this->getSEOUrl();
        }
    }
}
