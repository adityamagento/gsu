<?php

namespace FME\Testimonials\Controller\Adminhtml;

abstract class Testimonials extends \Magento\Backend\App\Action
{
    protected $_demoFactory;

    protected $_coreRegistry;

    public function __construct(
        \FME\Testimonials\Model\TestimonialsFactory $demoFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_demoFactory           = $demoFactory;
        $this->_coreRegistry          = $coreRegistry;
        parent::__construct($context);
    }

    protected function _initPost()
    {
        $postId  = (int) $this->getRequest()->getParam('testimonial_id');
        $post    = $this->_demoFactory->create();
        if ($postId) {
            $post->load($postId);
        }
        $this->_coreRegistry->register('fme_testimonials', $post);
        return $post;
    }
}
