<?php

namespace FME\Testimonials\Controller\Adminhtml\Testimonials;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use FME\Testimonials\Model\Testimonials as ModelTestimonials;

class InlineEdit extends \Magento\Backend\App\Action
{
    protected $dataProcessor;
    protected $TestimonialsModel;
    protected $jsonFactory;
    public function __construct(
        Context $context,
        PostDataProcessor $dataProcessor,
        ModelTestimonials $TestimonialsModel,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->dataProcessor = $dataProcessor;
        $this->TestimonialsModel = $TestimonialsModel;
        $this->jsonFactory = $jsonFactory;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('FME_Testimonials::testimonials');
    }
    
    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }

        foreach (array_keys($postItems) as $Id) {
            $Testimonials = $this->TestimonialsModel->load($Id);
            try {
                $Data = $this->filterPost($postItems[$Id]);
                $this->validatePost($Data, $Testimonials, $error, $messages);
                $extendedPageData = $Testimonials->getData();
                $this->setTestimonialsData($Testimonials, $extendedPageData, $Data);
                
                $this->TestimonialsModel->save($Testimonials);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithId($Testimonials, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithId($Testimonials, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithId(
                    $Testimonials,
                    __('Something went wrong while saving the item.')
                );
                $error = true;
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
    protected function filterPost($postData = [])
    {
        return $postData;
    }
    protected function validatePost(array $pageData, ModelTestimonials $page, &$error, array &$messages)
    {
        if (!($this->dataProcessor->validate($pageData) && $this->dataProcessor->validateRequireEntry($pageData))) {
            $error = true;
            foreach ($this->messageManager->getMessages(true)->getItems() as $error) {
                $messages[] = $this->getErrorWithId($page, $error->getText());
            }
        }
    }
    protected function getErrorWithId(ModelTestimonials $page, $errorText)
    {
        return '[Page ID: ' . $page->getId() . '] ' . $errorText;
    }
    public function setTestimonialsData(ModelTestimonials $page, array $extendedPageData, array $pageData)
    {
        $page->setData(array_merge($page->getData(), $extendedPageData, $pageData));
        return $this;
    }
}
