<?php

namespace FME\Testimonials\Controller\Adminhtml\Testimonials;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use FME\Testimonials\Model\Testimonials;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;
use FME\Testimonials\Model\UploaderPool;

class Save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;
    protected $scopeConfig;
    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    protected $uploaderPool;

    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory,
        UploaderPool $uploaderPool
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->scopeConfig = $scopeConfig;
        $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
        $this->inlineTranslation = $inlineTranslation;
        $this->uploaderPool = $uploaderPool;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('testimonial_id');
            if (isset($data['status']) && $data['status'] === '1') {
                $data['status'] = 'Enable';
            }
            if (empty($data['testimonial_id'])) {
                $data['testimonial_id'] = null;
            }
            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('FME\Testimonials\Model\Testimonials')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This Testimonials no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
                $image = $this->getUploader('image')->uploadFileAndGetName('image', $data);
                $data['image'] = $image;
            } else {
                  $data['image'] = null;
            }
            $model->setData($data);
            $this->inlineTranslation->suspend();
            try {
                $model->save();
                $this->messageManager->addSuccess(__('Testimonial Saved successfully'));
                $this->dataPersistor->clear('fme_testimonials');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['testimonial_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Testimonials.'));
            }

            $this->dataPersistor->set('fme_testimonials', $data);
            return $resultRedirect->setPath(
                '*/*/edit',
                ['testimonial_id' => $this->getRequest()->getParam('testimonial_id')]
            );
        }
        return $resultRedirect->setPath('*/*/');
    }

    protected function getUploader($type)
    {
        return $this->uploaderPool->getUploader($type);
    }
}
