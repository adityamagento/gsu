<?php

namespace FME\Testimonials\Controller\Adminhtml\Testimonials;

class Delete extends \Magento\Backend\App\Action
{
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('FME_Testimonials::testimonials');
    }

    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('testimonial_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                // init model and delete
                $model = $this->_objectManager->create('FME\Testimonials\Model\Testimonials');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The Testimonial has been deleted.'));
                // go to grid
                $this->_eventManager->dispatch(
                    'adminhtml_testimonialstestimonials_on_delete',
                    ['title' => $title, 'status' => 'success']
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_testimonialstestimonials_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['testimonial_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a testimonials to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
