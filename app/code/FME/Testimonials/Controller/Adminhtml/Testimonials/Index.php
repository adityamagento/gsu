<?php

namespace FME\Testimonials\Controller\Adminhtml\Testimonials;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
   
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('FME_Testimonials::testimonials');
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Testimonials::testimonials');
        $resultPage->addBreadcrumb(__('Testimonials'), __('Testimonials'));
        $resultPage->addBreadcrumb(__('Manage Testimonials'), __('Manage Testimonials'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Testimonials'));
 
        return $resultPage;
    }
}
