<?php

namespace FME\Testimonials\Controller\Adminhtml\Testimonials;

class PostDataProcessor
{
    protected $messageManager;

    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        
        $this->messageManager = $messageManager;
    }

    public function validate($data)
    {
        $errorNo = true;
        return $errorNo;
    }

    public function validateRequireEntry(array $data)
    {
        $requiredFields = [
            'identifier' => __('identifier')
        ];
        $errorNo = true;
        foreach ($data as $field => $value) {
            if (in_array($field, array_keys($requiredFields)) && $value == '') {
                $errorNo = false;
                $this->messageManager->addError(
                    __('To apply changes you should fill in hidden required "%1" field', $requiredFields[$field])
                );
            }
        }
        return $errorNo;
    }
}
