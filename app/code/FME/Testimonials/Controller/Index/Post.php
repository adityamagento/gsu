<?php

namespace FME\Testimonials\Controller\Index;

use FME\Testimonials\Model\Testimonials;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\PhpEnvironment\Request;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\JsonFactory;
class Post extends \Magento\Framework\App\Action\Action
{
    protected $jsonFactory;
    protected $scopeConfig;
    protected $_transportBuilder;
    protected $storeManager;
    private static $_siteVerifyUrl = "https://www.google.com/recaptcha/api/siteverify?";
    private static $_version = "php_1.0";
    const XML_PATH_EMAIL_RECIPIENT = 'Testimonials/email/recipient';
    const XML_PATH_EMAIL_SENDER = 'Testimonials/email/sender';
    const XML_PATH_EMAIL_TEMPLATE = 'Testimonials/email/template';
    const XML_PATH_EMAIL_SUBJECT = 'Testimonials/email/subject';
    const XML_RESPONSE_MSG = 'Testimonials/add_testimonials_setting/testimonial_response_msg';
    const XML_T_SETTING_ADMIN_APPROVAL      ='Testimonials/add_testimonials_setting/is_admin_approval';

    const CONFIG_CAPTCHA_ENABLE = 'Testimonials/google_options/captchastatus';
    const CONFIG_CAPTCHA_PRIVATE_KEY = 'Testimonials/google_options/googleprivatekey';
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool    
    ) {
        $this->_transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->jsonFactory = $jsonFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_frontCachePool = $cacheFrontendPool;
        
        parent::__construct($context);
    }
    public function execute()
    {
        
        $resultJson = $this->jsonFactory->create();
        $post = (array) $this->getRequest()->getPost();
        
        
        $remoteAddress = new \Magento\Framework\Http\PhpEnvironment\RemoteAddress($this->getRequest());
        $visitorIp = $remoteAddress->getRemoteAddress();
        $error = false;
        $message = '';
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            
        $captcha_enable = $this->scopeConfig->getValue(self::CONFIG_CAPTCHA_ENABLE);
            
        if ($captcha_enable) {
             $captcha =   $post["g-recaptcha-response"];
            $secret =  $this->scopeConfig->getValue(self::CONFIG_CAPTCHA_PRIVATE_KEY);
                
            $response = null;
            $path = self::$_siteVerifyUrl;
            $dataC =  [
            'secret' => $secret,
            'remoteip' => $visitorIp,
            'v' => self::$_version,
            'response' => $captcha
            ];
            $req = "";
            foreach ($dataC as $key => $value) {
                 $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
            }
        // Cut the last '&'
            $req = substr($req, 0, strlen($req)-1);
            $response = file_get_contents($path . $req);
            $answers = json_decode($response, true);
            if (trim($answers ['success']) == true) {
                $error = false;
            } else {
                $result = $this->jsonFactory->create();
               $result->setData(['output' => 'Incorrect Captcha Key','error' => 'true']);
               
                return $result;
                
                
            }
        }
        
        
        $isAdminAprroval = $this->scopeConfig->getValue(self::XML_T_SETTING_ADMIN_APPROVAL);
        if(!$isAdminAprroval):
             //flush page cache - so testimonial could appear on frontend rightaway
             $this->_cleanFullPageCache();             
        endif;
        
        
        try {
            if ((!empty($post)) && (! $error)) {
                $model = $this->_objectManager->create('FME\Testimonials\Model\Testimonials');
                $imageModel = $this->_objectManager->create('FME\Testimonials\Model\Testimonials\Image');
                $uploadimageModel = $this->_objectManager->create('FME\Testimonials\Model\Upload');
                $model->setData($post);
                
                    $featuredImage =  $uploadimageModel->uploadFileAndGetName(
                        'image',
                        $imageModel->getBaseDir(),
                        $post
                    );
                    $model->setImage($featuredImage);
                    
                $subject=$this->scopeConfig->getValue(self::XML_PATH_EMAIL_SUBJECT, $storeScope);
                 $newData = ['subject' => $subject,'company_name' => $post['company_name'], 'email' => $post['email'], 'testimonial_desc' => $post['testimonial_desc'], 'website_url'=> $post['website_url']];
                $model->save();
                $response_msg =  $this->scopeConfig->getValue(self::XML_RESPONSE_MSG);
                /*Email to Sending Start*/
                $postObject = new \Magento\Framework\DataObject();
                $postObject->setData($newData);
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                $transport = $this->_transportBuilder
                                ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE, $storeScope))
                                ->setTemplateOptions(
                                    [
                                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                        'store' => $this->storeManager->getStore()->getId(),
                                    ]
                                )
                                ->setTemplateVars(['data' => $postObject])
                                ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
                                ->addTo($this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope))
                                ->setReplyTo($post['email'])
                                ->getTransport();
                            
                $transport->sendMessage();
                
                
            $result = $this->jsonFactory->create();
            $result->setData(['output' => $response_msg,'error' => 'false']);
               
                return $result;
            }
        } catch (\Exception $ex) {
                $message = $ex->getMessage();
                $error = true;
                $resultJson->setData(['output' => $message,'error' => 'true']);
                return $resultJson;
        }
            $resultJson->setData(['output' => "Something Went Wrong", 'error' => 'true']);
                return $resultJson;
       
    }
    
    protected function _cleanFullPageCache() {
        
        $this->_cacheTypeList->cleanType('full_page');
        
        foreach ($this->_frontCachePool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
        
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }

    /**
     * @param array $post Post data from contact form
     * @return void
     */
    private function sendEmail($post)
    {
        $this->mail->send($post['email'], ['data' => new \Magento\Framework\DataObject($post)]);
    }

    /**
     * @return bool
     */
    private function isPostRequest()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        return !empty($request->getPostValue());
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function validatedParams()
    {
        $request = $this->getRequest();
        if (trim($request->getParam('name')) === '') {
            throw new LocalizedException(__('Name is missing'));
        }
        if (trim($request->getParam('comment')) === '') {
            throw new LocalizedException(__('Comment is missing'));
        }
        if (false === \strpos($request->getParam('email'), '@')) {
            throw new LocalizedException(__('Invalid email address'));
        }
        if (trim($request->getParam('hideit')) !== '') {
            throw new \Exception();
        }

        return $request->getParams();
    }
}
