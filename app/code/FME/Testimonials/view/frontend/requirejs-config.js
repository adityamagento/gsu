/**
 * Copyright © 2019 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            fmeCarousal:           'FME_Testimonials/js/owl.carousel'
        }
    }
};
