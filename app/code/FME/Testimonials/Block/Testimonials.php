<?php

namespace FME\Testimonials\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;
  
class Testimonials extends Template
{
    public $testimonialHelper;
    protected $scopeConfig;
    protected $collectionFactory;
    protected $objectManager;
    protected $_session;
   
    protected $templateProcessor;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FME\Testimonials\Model\ResourceModel\Testimonials\
        CollectionFactory $collectionFactory,
        \FME\Testimonials\Helper\Data $helper,
        ObjectManagerInterface $objectManager,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Zend_Filter_Interface $templateProcessor,
        $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->objectManager = $objectManager;
        $this->testimonialHelper = $helper;
        $this->_session = $authSession;
         $this->templateProcessor = $templateProcessor;
        parent::__construct($context, $data);
    }
        
    public function _prepareLayout()
    {
        if ($this->testimonialHelper->isEnabledInFrontend()) {
            $this->pageConfig->getTitle()->set($this->testimonialHelper->getTestimonialPageTitle());
            $this->pageConfig->setKeywords($this->testimonialHelper->getTestimonialPageMetaKeywords());
            $this->pageConfig->setDescription($this->testimonialHelper->getTestimonialPageMetaDesc());
               return parent::_prepareLayout();
        }
    }
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }
    
    /**
     * Get website identifier
     *
     * @return string|int|null
     */
    public function getWebsiteId()
    {
        return $this->_storeManager->getStore()->getWebsiteId();
    }
    
    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }
    
    /**
     * Get Store name
     *
     * @return string
     */
    public function getStoreName()
    {
        return $this->_storeManager->getStore()->getName();
    }
    
    /**
     * Get current url for store
     *
     * @param bool|string $fromStore Include/Exclude from_store parameter from URL
     * @return string     
     */
    public function getStoreUrl($fromStore = true)
    {
        return $this->_storeManager->getStore()->getCurrentUrl($fromStore);
    }
    
    /**
     * Check if store is active
     *
     * @return boolean
     */
    public function isStoreActive()
    {
        return $this->_storeManager->getStore()->isActive();
    }
    public function getCurrentStoreName()
    {
         return $this->_storeManager->getStore()->getId();
    }
    public function getFormAction()
    {
        return $this->getUrl('testimonials/index/post', ['_secure' => true]);
    }
    public function getMode()
    {
        return $this->getChildBlock('toolbar')->getCurrentMode();
    }
    public function filterOutputHtml($string)
    {
        return $this->templateProcessor->filter($string);
    }
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    public function getFeaturedTestimonialsDataForBlock()
    {
        $collection = $this->collectionFactory->create();
        $collection=$collection->addFieldToFilter('status', 'Enable');
        if ($this->testimonialHelper->getTestimonialSortOder()=="latest") {
            $collection->setOrder('testimonial_id', 'DESC');
        } elseif ($this->testimonialHelper->getTestimonialSortOder()=="giverorder") {
            $collection->setOrder('sort_order', 'ASC');
        }
        $collection=$collection->addFieldToFilter('featured', '1');
        $collection->addStoreFilter($this->_storeManager->getStore()); 
        $collection->setPageSize($this->testimonialHelper->getNumberofTestimonialOnBlock());
        $collection->setCurPage(1);
        return $collection;
    }
    public function getTestimonialsDataForBlock()
    {
        $collection = $this->collectionFactory->create();
        if ($this->testimonialHelper->getTestimonialSortOder()=="latest") {
            $collection->setOrder('testimonial_id', 'DESC');
        } elseif ($this->testimonialHelper->getTestimonialSortOder()=="giverorder") {
            $collection->setOrder('sort_order', 'ASC');
        }
        $collection->addStoreFilter($this->_storeManager->getStore()); 
        $collection=$collection->addFieldToFilter('status', 'Enable');

        if($this->testimonialHelper->getNumberofTestimonialOnBlock()!=null)
        {
        $collection->setPageSize($this->testimonialHelper->getNumberofTestimonialOnBlock());
         $collection->setCurPage(1);
        }
        return $collection;
    }
    public function getTestimonialsData()
    {
        $collection = $this->collectionFactory->create();
        $collection=$collection->addFieldToFilter('status', 'Enable');
       
        if ($this->testimonialHelper->getTestimonialSortOder()=="latest") {
            $collection->setOrder('testimonial_id', 'DESC');
        } elseif ($this->testimonialHelper->getTestimonialSortOder()=="giverorder") {
            $collection->setOrder('sort_order', 'ASC');
        }
        $collection->addStoreFilter($this->_storeManager->getStore()); 
        if($this->testimonialHelper->getNumOfTestimonialsForTestimonialPage()!=null)
        {
            $collection->setPageSize($this->testimonialHelper->getNumOfTestimonialsForTestimonialPage());
            $collection->setCurPage(1);
        }

        return $collection;
    }

     public function getTestimonialsHomeData()
    {
        $collection = $this->collectionFactory->create();
        $collection=$collection->addFieldToFilter('status', 'Enable');
       
        if ($this->testimonialHelper->getTestimonialSortOder()=="latest") {
            $collection->setOrder('testimonial_id', 'DESC');
        } 
        $collection->addStoreFilter($this->_storeManager->getStore()); 
        $collection->setPageSize(1);

        return $collection;
    }
   
    public function getMediaUrl()
    {
        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $media_dir;
    }
}
