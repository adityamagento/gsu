<?php 
namespace FME\Testimonials\Block;  

class Link extends \Magento\Framework\View\Element\Html\Link
{
    public function _toHtml() 
    {
        if (
            !$this->_scopeConfig->isSetFlag('Testimonials/general/testimonials_mod_enable') || 
            !$this->_scopeConfig->isSetFlag('Testimonials/footer_links/footer_link_enable')
        ) {
            return '';
        }
        return parent::_toHtml();
    }
}