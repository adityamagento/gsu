/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
var config = {
    map: {
        "*": {
            camera          : 'FME_Articles/js/camera',
            cameramin       : 'FME_Articles/js/camera-min',
            slidesjs        : 'FME_Articles/js/slides',
            owlcarousel     : 'FME_Articles/js/owlcarousel/owlcarousel',
            jwplayer        : 'FME_Articles/js/jwplayer',
            fitVids         : 'FME_Articles/js/fitVids',
            jsfunctions     : 'FME_Articles/js/jsfunctions'
            
        }
    },
        paths: {
            "camera"                : 'FME_Articles/js/camera',
            "cameramin"             : 'FME_Articles/js/camera-min',
            "slidesjs"              : 'FME_Articles/js/slides',
            "owlcarousel"           : 'js/owlcarousel/owlcarousel',
            "jwplayer"              : 'js/jwplayer',
            "fitVids"               : 'js/fitVids',
            "jsfunctions"           : 'js/jsfunctions'
        },
        shim: {
            "slidesjs": {
                deps: ["jquery"]
            },
             "jwplayer": {
            deps: ["jquery"]
        },
        "fitVids": {
            deps: ["jquery"]
        },
        "jsfunctions": {
            deps: ["jquery"]
        },
        "owlcarousel": {
            deps: ["jquery"]
        }

    },
    waitSeconds : 90
};

