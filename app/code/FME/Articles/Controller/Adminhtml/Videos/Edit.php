<?php

namespace FME\Articles\Controller\Adminhtml\Videos;

use Magento\Backend\App\Action;
 
class Edit extends \Magento\Backend\App\Action
{
    
    const ADMIN_RESOURCE = 'FME_Articles::manage_videos';

    
    protected $_coreRegistry;
    protected $resultPageFactory;
    protected $model;
    
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \FME\Articles\Model\Videos\Productvideos $model,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->model = $model;
        parent::__construct($context);
    }
    
    protected function _initAction()
    {
    
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Articles::articles_videos')
            ->addBreadcrumb(__('VIDEOS'), __('VIDEOS'))
            ->addBreadcrumb(__('Manage Comments'), __('Manage Videos'));
        return $resultPage;
    }
        
    public function execute()
    {

        $id = $this->getRequest()->getParam('video_id');
        if ($id) {
            $this->model->load($id);
            if (!$this->model->getId()) {
                $this->messageManager
                ->addError(__('This video no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('articles_videos', $this->model);

        $resultPage = $this->_initAction();

        $resultPage->addBreadcrumb(
            $id ? __('Edit Video') : __('New Video'),
            $id ? __('Edit Video') : __('New Video')
        );
        
        $resultPage->getConfig()->getTitle()->prepend(__('Comments'));
        $resultPage->getConfig()->getTitle()
            ->prepend($this->model->getId() ? $this->model->getTitle() : __('New Video'));

        return $resultPage;
    }
}
