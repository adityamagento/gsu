<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Controller\Adminhtml\Videos;

use Magento\Backend\App\Action;

class Delete extends \Magento\Backend\App\Action
{
    
    const ADMIN_RESOURCE = 'FME_Articles::videos_delete';
    
    protected $model;
    public function __construct(
        Action\Context $context,
        \FME\Articles\Model\Videos\Productvideos $model
    ) {
        $this->model = $model;
        parent::__construct($context);
    }
    public function execute()
    {
        
        $id = $this->getRequest()->getParam('video_id');
        
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                $this->model->load($id);
                $title = $this->model->getTitle();
                $this->model->delete();
                // display success message
                $this->messageManager->addSuccess(__('The video has been deleted.'));
                // go to grid
                $this->_eventManager->dispatch(
                    'adminhtml_videopage_on_delete',
                    ['title' => $title, 'status' => 'success']
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_videopage_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['video_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a video to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
