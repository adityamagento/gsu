<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Controller\Adminhtml\Videos\Productvideos\Image;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Upload
 */

class UploadAlt extends \Magento\Backend\App\Action
{

    /**
     * Image uploader
     *
     * @var \Magento\Catalog\Model\ImageUploader
     */
    protected $baseTmpPath;

    protected $imageUploaderAlt;

    /**
     * Upload constructor.
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Catalog\Model\ImageUploader $imageUploader
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \FME\Articles\Model\Videos\ImageUploader $imageUploaderAlt
    ) {
        parent::__construct($context);
        $this->imageUploaderAlt = $imageUploaderAlt;
    }//end __construct()


    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
    
        return true;
    }//end _isAllowed()


    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
       
        try {
            $result = $this->imageUploaderAlt->saveFileToTmpDir('video_thumb');

            $result['cookie'] = [
                                 'name'     => $this->_getSession()->getName(),
                                 'value'    => $this->_getSession()->getSessionId(),
                                 'lifetime' => $this->_getSession()->getCookieLifetime(),
                                 'path'     => $this->_getSession()->getCookiePath(),
                                 'domain'   => $this->_getSession()->getCookieDomain(),
                                ];
        } catch (\Exception $e) {
            $result = [
                       'error'     => $e->getMessage(),
                       'errorcode' => $e->getCode(),
                      ];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }//end execute()
}//end class
