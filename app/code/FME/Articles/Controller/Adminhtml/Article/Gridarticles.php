<?php

namespace FME\Articles\Controller\Adminhtml\Article;

class Gridarticles extends \Magento\Catalog\Controller\Adminhtml\Category
{

    protected $resultRawFactory;
    protected $layoutFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory = $layoutFactory;
    }

    public function execute()
    {
        $category = $this->_initCategory(true);
        if (!$category) {
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('articles/*/', ['_current' => true, 'id' => null]);
        }

        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents(
            $this->layoutFactory->create()->createBlock(
                'FME\Articles\Block\Adminhtml\RelatedArticles\Category\Tab\Product',
                'related.productarticles.grid'
            )->toHtml()
        );
    }
}
