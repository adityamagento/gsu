<?php

namespace FME\Articles\Controller\Adminhtml\Article;

use Magento\Backend\App\Action;
use FME\Articles\Model\Article;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    
    const ADMIN_RESOURCE = 'FME_Articles::manage_article';
    protected $dataProcessor;
    protected $dataPersistor;
    protected $model;

    public function __construct(
        Action\Context $context,
        PostDataProcessor $dataProcessor,
        Article $model,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        $this->model = $model;
        parent::__construct($context);
    }

    public function execute()
    {
        
        $data = $this->getRequest()->getPostValue();
             // echo '<pre>';
             // print_r($data);exit;
        if (isset($data['related_products'])) {
            $cat_array = json_decode($data['related_products'], true);
            $pro_array = array_values($cat_array);
                $c=0;
            foreach ($cat_array as $key => $value) {
                $pro_array[$c] = $key;
                $c++;
            }
              unset($data['related_products']);
              $data['imedia_id'] = $pro_array;
        }

        if (isset($data['assign_related_products'])) {
            $cat_array = json_decode($data['assign_related_products'], true);
            $pro_array = array_values($cat_array);
                $c=0;
            foreach ($cat_array as $key => $value) {
                $pro_array[$c] = $key;
                $c++;
            }
              unset($data['assign_related_products']);
              $data['entity_id'] = $pro_array;
        }

        if (isset($data['assign_related_articles'])) {
            $cat_array = json_decode($data['assign_related_articles'], true);
            $pro_array = array_values($cat_array);
                $c=0;
            foreach ($cat_array as $key => $value) {
                $pro_array[$c] = $key;
                $c++;
            }
              unset($data['assign_related_articles']);
              $data['related_articles_id'] = $pro_array;
        }

        if (($data['related_categories']) !== '') {
            $pro_array = $data['related_categories'];
                $c=0;
            foreach ($pro_array as $key => $value) {
                $pro_array[$c] = $value;
                $c++;
            }
              unset($data['related_categories']);
              $data['category_id'] = $pro_array;
        }
        if (isset($data['related_videos'])) {
            $cat_array = json_decode($data['related_videos'], true);
            $pro_array = array_values($cat_array);
                $c=0;
            foreach ($cat_array as $key => $value) {
                $pro_array[$c] = $key;
                $c++;
            }
              unset($data['related_videos']);
              $data['video_id'] = $pro_array;
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->dataProcessor->filter($data);
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Article::STATUS_ENABLED;
            }
            if (empty($data['articles_id'])) {
                $data['articles_id'] = null;
            }

            $id = $this->getRequest()->getParam('articles_id');
            if ($id) {
                $this->model->load($id);
            }
            if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
                $data['image'] ='/articles/article/'.$data['image'][0]['name'];
            } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
                $data['image'] =$data['image'][0]['name'];
            } else {
                $data['image'] = 'no_image.png';
            }
            //$data['image'] = $data['image'][0]['path'];
            //$data['path'] = $data['image'][0]['path'];
            
// echo '<pre>';
//          print_r($data);exit;
            $this->model->setData($data);

            $this->_eventManager->dispatch(
                'articles_article_prepare_save',
                ['article' => $this->model, 'request' => $this->getRequest()]
            );

            if (!$this->dataProcessor->validate($data)) {
                return $resultRedirect->setPath('*/*/edit', ['articles_id' => $this->model->getId(), '_current' => true]);
            }

            try {
                $this->model->save();
                $this->messageManager->addSuccess(__('You saved the article.'));
                $this->dataPersistor->clear('articles');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['articles_id' => $this->model->getId(),
                         '_current' => true]
                    );
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the article.'));
            }

            $this->dataPersistor->set('articles', $data);
            return $resultRedirect->setPath('*/*/edit', ['articles_id' => $this->getRequest()->getParam('articles_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
