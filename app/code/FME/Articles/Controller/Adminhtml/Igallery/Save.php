<?php

namespace FME\Articles\Controller\Adminhtml\Igallery;

use Magento\Backend\App\Action;
use FME\Articles\Model\Igallery;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'FME_Articles::manage_igallery';

    protected $dataProcessor;
    protected $dataPersistor;
    protected $model;

    public function __construct(
        Action\Context $context,
        PostDataProcessor $dataProcessor,
        Igallery $model,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataProcessor = $dataProcessor;
        $this->dataPersistor = $dataPersistor;
        $this->model = $model;
        parent::__construct($context);
    }
    
    public function execute()
    {
        
        $data = $this->getRequest()->getPostValue();
              
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->dataProcessor->filter($data);
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Igallery::STATUS_ENABLED;
            }
            if (empty($data['imedia_id'])) {
                $data['imedia_id'] = null;
            }

            $id = $this->getRequest()->getParam('imedia_id');
            if ($id) {
                $this->model->load($id);
            }
            $idata = $data['product']['gallery']['images'];

           
            foreach ($idata as $data) {
                        $this->model->setData($data);

                        $this->_eventManager->dispatch(
                            'articles_igallery_prepare_save',
                            ['Igallery' => $this->model, 'request' => $this->getRequest()]
                        );

                if (!$this->dataProcessor->validate($data)) {
                    return $resultRedirect->setPath('*/*/edit', ['imedia_id' => $this->model->getId(), '_current' => true]);
                }
                        $this->model->save();
            }
            try {
                $this->messageManager->addSuccess(__('You saved the media.'));
                $this->dataPersistor->clear('articles');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['imedia_id' => $this->model->getId(),
                         '_current' => true]
                    );
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the media.'));
            }

            $this->dataPersistor->set('articles', $data);
            return $resultRedirect->setPath('*/*/edit', ['imedia_id' => $this->getRequest()->getParam('imedia_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
