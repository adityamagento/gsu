<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Controller\Adminhtml\Igallery;

use Magento\Backend\App\Action;

class Delete extends \Magento\Backend\App\Action
{
    
    const ADMIN_RESOURCE = 'FME_Articles::igallery_delete';
    
    protected $model;
    public function __construct(
        Action\Context $context,
        \FME\Articles\Model\Igallery $model
    ) {
        $this->model = $model;
        parent::__construct($context);
    }
    public function execute()
    {
        
        $id = $this->getRequest()->getParam('imedia_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            $title = "";
            try {
                $this->model->load($id);
                $title = $this->model->getTitle();
                $this->model->delete();
                $this->messageManager->addSuccess(__('The image(s) has been deleted.'));
                $this->_eventManager->dispatch(
                    'adminhtml_igallerypage_on_delete',
                    ['title' => $title, 'status' => 'success']
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                    'adminhtml_igallerypage_on_delete',
                    ['title' => $title, 'status' => 'fail']
                );
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['imedia_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a image(s) to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
