<?php

namespace FME\Articles\Controller\Adminhtml\Igallery;

use Magento\Backend\App\Action;
 
class Edit extends \Magento\Backend\App\Action
{
    
    const ADMIN_RESOURCE = 'FME_Articles::manage_igallery';

    
    protected $_coreRegistry;
    protected $resultPageFactory;
    protected $model;
    protected $mediaFactory;
    
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \FME\Articles\Model\Igallery $model,
        \FME\Articles\Model\MediaFactory $mediaFactory,    
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->model = $model;
        $this->mediaFactory = $mediaFactory;
        
        parent::__construct($context);
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Articles::articles_igallery')
            ->addBreadcrumb(__('IMAGES'), __('IMAGES'))
            ->addBreadcrumb(__('Manage Images'), __('Manage Images'));
        return $resultPage;
    }
        
    public function execute()
    {

        $id = $this->getRequest()->getParam('imedia_id');
        $photogallery = $this->mediaFactory->create();
        $collection = $photogallery->getCollection()->addFieldToFilter('imedia_id', $id);
        

        $this->_objectManager->get('Magento\Framework\Registry')->register('photogallery_img', $collection);

        $this->_coreRegistry->register('articles_igallery', $this->model);

        $resultPage = $this->_initAction();

        $resultPage->addBreadcrumb(
            $id ? __('Edit Images') : __('New Images'),
            $id ? __('Edit Images') : __('New Images')
        );
        
        $resultPage->getConfig()->getTitle()->prepend(__('Images'));
        $resultPage->getConfig()->getTitle()
            ->prepend($this->model->getId() ? $this->model->getEventName() : __('New Images'));

        return $resultPage;
    }
}
