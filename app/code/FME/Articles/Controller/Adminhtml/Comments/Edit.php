<?php

namespace FME\Articles\Controller\Adminhtml\Comments;

use Magento\Backend\App\Action;
 
class Edit extends \Magento\Backend\App\Action
{
    
    const ADMIN_RESOURCE = 'FME_Articles::manage_comments';

    
    protected $_coreRegistry;
    protected $resultPageFactory;
    protected $model;
    
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \FME\Articles\Model\Article $model,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->model = $model;
        parent::__construct($context);
    }
    
    protected function _initAction()
    {
    
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Articles::articles_comments')
            ->addBreadcrumb(__('COMMENTS'), __('COMMENTS'))
            ->addBreadcrumb(__('Manage Comments'), __('Manage Comments'));
        return $resultPage;
    }
        
    public function execute()
    {

        $id = $this->getRequest()->getParam('comments_id');
        if ($id) {
            $this->model->load($id);
            if (!$this->model->getId()) {
                $this->messageManager
                ->addError(__('This comment no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('articles_comments', $this->model);

        $resultPage = $this->_initAction();

        $resultPage->addBreadcrumb(
            $id ? __('Edit Comment') : __('New Comment'),
            $id ? __('Edit Comment') : __('New Comment')
        );
        
        $resultPage->getConfig()->getTitle()->prepend(__('Comments'));
        $resultPage->getConfig()->getTitle()
            ->prepend($this->model->getId() ? $this->model->getTitle() : __('New Comment'));

        return $resultPage;
    }
}
