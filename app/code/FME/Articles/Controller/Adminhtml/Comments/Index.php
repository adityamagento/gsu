<?php

namespace FME\Articles\Controller\Adminhtml\Comments;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    
    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    protected function _isAllowed()
    {
       
        return $this->_authorization
                    ->isAllowed('FME_Articles::manage_comments');
    }
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('FME_Articles::comments');
        $resultPage->addBreadcrumb(__('Comments'), __('Comments'));
        $resultPage->addBreadcrumb(__('Manage Comments'), __('Manage Comments'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Comments'));
        return $resultPage;
    }
}
