<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Controller;

class Router implements \Magento\Framework\App\RouterInterface
{

    protected $actionFactory;
    protected $_response;
    protected $_request;
    protected $pageRepository;

    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\App\RequestInterface $request,
        \FME\Articles\Helper\Article $helper,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository,
        \Magento\Framework\App\ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->_request = $request;
        $this->pageRepository = $pageRepository;
        $this->_response = $response;
        $this->articlesHelper = $helper;
    }
    
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
            $route = $this->articlesHelper->getArticleSeoPrefix();
            
            
            $suffix = $this->articlesHelper->getArticleSeoSuffix();
            $identifier = trim($request->getPathInfo(), '/');
            
            if (strpos($identifier, $route) === false) {
				$identifier = $route.'/'.$identifier;
			}
			
            $parts = explode('/', $identifier);            
            
            $identifie = $route.$suffix;

            $identifieDetail = 'detail';

            
        if (strcmp($identifier, $identifie) == 0) {
            $request->setModuleName('article')->setControllerName('Index')->setActionName('Index');
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
        } elseif (isset($parts[0]) && ($parts[0] == $route) && isset($parts[1]) && $parts[1] == 'category') {
            $request->setModuleName('article')->setControllerName('Index')->setActionName('Index');
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
        } elseif (isset($parts[0]) && ($parts[0] == $route) && isset($parts[1]) && $parts[1] == 'cat') {
            $request->setModuleName('article')->setControllerName('Index')->setActionName('Category');
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
        } elseif (isset($parts[0]) && ($parts[0] == $route) && isset($parts[1]) && $parts[1] == 'search') {
            $request->setModuleName('article')->setControllerName('Index')->setActionName('Index');
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
            //comments controller
        } elseif (isset($parts[0]) && ($parts[0] == $route) && isset($parts[1]) && $parts[1] == 'index' && $parts[2] == 'comments') {
            $request->setModuleName('article')->setControllerName('Index')->setActionName('CommentsControl');
            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
        } elseif (isset($parts[0]) && ($parts[0] == $route) && isset($parts[1]) && !isset($parts[2])) {
              $detailIdentifier =  $parts[1];
            if (strpos($detailIdentifier, '.') !== false) {
                $detailIdentifier = explode('.', $detailIdentifier);
                $detailIdentifier = $detailIdentifier[0];
            }
             
              $request->setModuleName('article')->setControllerName('Index')->setActionName('Detail')->setParam('id', $detailIdentifier);

              $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
        } else {
              return null;
        }
                
            return $this->actionFactory->create(
                'Magento\Framework\App\Action\Forward',
                ['request' => $request]
            );
    }
}
