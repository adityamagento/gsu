<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Category extends \Magento\Framework\App\Action\Action
{

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->resultPageFactory->create(false, ['isIsolated' => true]);
        $youradminlayout = '1column';
         $page->getConfig()->setPageLayout($youradminlayout);
         return $page;
    }
}
