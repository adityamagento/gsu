<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Controller\Index;

use FME\Articles\Model\Comments;

class CommentsControl extends \Magento\Framework\App\Action\Action
{

    protected $dataProcessor;
    protected $dataPersistor;
    protected $model;
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Comments $model
    ) {
        $this->model = $model;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $this->model->setData($data);
        $this->model->save();
        $this->messageManager->addSuccess(__('Thanks for your comment.'));
        $this->_redirect('*/*/');
        return;
    }
}
