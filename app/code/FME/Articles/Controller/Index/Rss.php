<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Controller\Index;

use Magento\Framework\Controller\ResultFactory;


class Rss extends \Magento\Framework\App\Action\Action
{
    public $ssl = true;
    public $output = '';
    public $articlesHelper;
    public $sliderHelper;
    public $_storeManager;
    protected $collectionFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        ResultFactory $resultPageFactory,
        \FME\Articles\Helper\Article $helper,
        \FME\Articles\Helper\Slider $sliderHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \FME\Articles\Model\ResourceModel\Article\
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->articlesHelper = $helper;
        $this->sliderHelper = $sliderHelper;
        $this->_storeManager=$storeManager;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
            $shopUri = $this->_storeManager->getStore()->getBaseUrl();
            $posts = $this->collectionFactory->create()->addFieldToFilter('is_active', 1);
              
        $this->output .= '<?xml version="1.0" encoding="UTF-8"?>
        <rss version="2.0">
            <channel>
                <title><![CDATA['.$this->articlesHelper->getArticleFinalIdentifier().']]></title>
                <description>Recent Feeds</description>
                <link>'.$shopUri.'</link>
                <webMaster>'.$this->articlesHelper->getStoreEmail().'</webMaster>
                
                <image>
                    <title><![CDATA['.$this->articlesHelper->getStorename().']]></title>
                    
                    <link>'.$shopUri.'</link>
                </image>';
        if (isset($posts) && $posts) {
            foreach ($posts as $post) {
                $url = $post->getIdentifier();
                $blog_link =  $shopUri.$this->articlesHelper->getArticleFinalDetailIdentifier($url);
                
                $this->output .= '
                <item>
                    <title><![CDATA['.$post->getTitle().']]></title>
                    <pubDate>'.date('r', strtotime($post->getArticlePublishDate())).'</pubDate>
                    <description>';
                if ($post->getImage()) {
                    $this->output .= '<![CDATA[<img src="'.$this->sliderHelper->resizeImage($post->getImage(), 100, 75).'" title="'.$post->getTitle().'" alt="'.$post->getTitle().'" width="120"/>]]>';
                }
                $this->output .= '<![CDATA['.$post->getArtilceShortSummary().']]></description>';
                $this->output .= '
                    <link>'.str_replace('&amp;', '&', htmlspecialchars($blog_link)).'</link>
                    <guid isPermaLink="true">'.str_replace('&amp;', '&', htmlspecialchars($blog_link)).'</guid>
                </item>';
            }
        }
        $this->output .= '
            </channel>
        </rss>';        
        $resultJson = $this->resultPageFactory->create(ResultFactory::TYPE_RAW);
        $resultJson->setContents($this->output);
        return $resultJson;
        
    }
}
