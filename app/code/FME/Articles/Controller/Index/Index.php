<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    public $layoutConfig;
    public function __construct(
        \FME\Articles\Helper\ArticleSocial $layoutConfig,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->layoutConfig = $layoutConfig;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->resultPageFactory->create(false, ['isIsolated' => true]);
        if ($this->layoutConfig->getArticleLayoutNumColumns()) {
            $adminlayout = $this->layoutConfig->getArticleLayoutNumColumns();
        } else {
            $adminlayout = '1column';
        }
         $page->getConfig()->setPageLayout($adminlayout);
         return $page;
    }
}
