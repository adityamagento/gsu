<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use FME\Articles\Block\Adminhtml\Igallery\Grid\Renderer\Action\UrlBuilder;
use Magento\Framework\UrlInterface;

/**
 * Class PageActions
 */
class Igallery extends Column
{
    
    const IGALLERY_URL_PATH_EDIT = 'articles/igallery/edit';
    const IGALLERY_URL_PATH_DELETE = 'articles/igallery/delete';
    
    protected $actionUrlBuilder;
   
    protected $urlBuilder;
    
    private $editUrl;
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlBuilder $actionUrlBuilder,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::IGALLERY_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->actionUrlBuilder = $actionUrlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['imedia_id'])) {
                    // $item[$name]['edit'] = [
                    //     'href' => $this->urlBuilder->getUrl($this->editUrl, ['imedia_id' => $item['imedia_id']]),
                    //     'label' => __('Edit')
                    // ];
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(
                            self::IGALLERY_URL_PATH_DELETE,
                            ['imedia_id' => $item['imedia_id']]
                        ),
                       'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete \"${ $.$data.label }\"'),
                            'message' => __('Are you sure you wan\'t to delete a \"${ $.$data.file }\" record?')
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
