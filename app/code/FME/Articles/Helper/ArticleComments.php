<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class ArticleComments extends AbstractHelper
{
    
    // Comments Section Begins
    const ARTICLE_COMMENTS_APPROVE                  = 'articles/article_comments/article_comments_approve';
    const ARTICLE_REGISTER_CUSTOMER_COMMENTS        = 'articles/article_comments/article_registered_customers_comments';
    const ARTICLE_COMMENT_NAME                      = 'articles/article_comments/article_comment_name';
    const ARTICLE_COMMENTS_ANTISPAM                 = 'articles/article_comments/article_comments_antispam';
    const ARTICLE_COMMENTS_CAPTCHA                  = 'articles/article_comments/article_comments_captcha';
    const ARTICLE_GOOGLE_CAPTCHA                    = 'articles/article_comments/article_google_captcha';
    const ARTICLE_RECENT_COMMNT_ENABLE              = 'articles/article_recent_comments/article_recent_enable';
    const ARTICLE_RECENT_TITLE                      = 'articles/article_recent_comments/article_recent_title';
    const ARTICLE_RECENT_NUMBER                     = 'articles/article_recent_comments/article_recent_number';
    const ARTICLE_RECENT_LEFT                       = 'articles/article_recent_comments/article_recent_left';
    const ARTICLE_RECENT_RIGHT                     = 'articles/article_recent_comments/article_recent_right';
    const ARTICLE_FACEBOOK_COMMENTTENABLE          = 'articles/article_facebook_comments/article_facebook_enable';
    const ARTICLE_FACEBOOK_APPID                   = 'articles/article_facebook_comments/article_facebook_appid';
    const ARTICLE_FACEBOOK_WIDHT                   = 'articles/article_facebook_comments/article_facebook_width';
    const ARTICLE_FACEBOOK_NUMPOST                 = 'articles/article_facebook_comments/article_facebook_numposts';
    const ARTICLE_FACEBOOK_THEME                   = 'articles/article_facebook_comments/article_facebook_theme';
    const ARTICLE_FACEBOOK_SORT                    = 'articles/article_facebook_comments/article_facebook_sort';
    
    public function getArticleCommentsApprove()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_COMMENTS_APPROVE, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleRegisterCustomerComments()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_REGISTER_CUSTOMER_COMMENTS, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleCommentName()
    {

        return $this->scopeConfig->getValue(self::ARTICLE_COMMENT_NAME, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleCommentsAntispam()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_COMMENTS_ANTISPAM, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleCommentsCaptcha()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_COMMENTS_CAPTCHA, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleGoogleCaptcha()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_GOOGLE_CAPTCHA, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleRecentCommentEnable()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_RECENT_COMMNT_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleRecentTitle()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_RECENT_TITLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleRecentNumber()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_RECENT_NUMBER, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleFbCommentable()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_COMMENTTENABLE, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleFbAppId()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_APPID, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleFbWidth()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_WIDHT, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleFbNumPost()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_NUMPOST, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleFbTheme()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_THEME, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleFbSort()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_SORT, ScopeInterface::SCOPE_STORE);
    }
}
