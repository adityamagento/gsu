<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;

class Slider extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context              $context
     * @param \Magento\Framework\ObjectManagerInterface          $objectManager
     * @param \Magento\Framework\Registry                        $registry
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \FME\Categorybanner\Model\CategorybannerFactory    $categorybannerFactory
     * @param \FME\Categorybanner\Model\Categorybanner           $categorybanner
     * @param \Magento\Framework\Image\Factory                   $imageFactory
     * @param \Magento\Framework\App\ResourceConnection          $coreResource
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        // \FME\Categorybanner\Model\CategorybannerFactory $categorybannerFactory,
        // \FME\Categorybanner\Model\Categorybanner $categorybanner,
        \Magento\Framework\Image\Factory $imageFactory,
        \Magento\Framework\App\ResourceConnection $coreResource
    ) {

        // $this->_categorybannerFactory = $categorybannerFactory;
        // $this->_categorybanner = $categorybanner;
        $this->_objectManager = $objectManager;
        $this->_coreRegistry = $registry;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_eventManager = $context->getEventManager();
        $this->_imageFactory = $imageFactory;
        $this->_resource = $coreResource;

        parent::__construct($context);
    }

    // const XML_PATH_MODULE_ENABLE = 'categorybanner/general/enable_module';
    const XML_PATH_SLIDER_TYPE = 'articles/article_slider_settings/slider_type';
    const XML_PATH_BANNER_WIDTH = 'articles/article_slider_settings/banner_width';
    const XML_PATH_BANNER_HEIGHT = 'articles/article_slider_settings/banner_height';
    const XML_PATH_SHOW_BANNER_TITLE = 'articles/article_slider_settings/banner_title';
    const XML_PATH_SHOW_BANNER_CONTENT = 'articles/article_slider_settings/banner_content';

    // public function categoryBannerEnable()
    // {
    //     return $this->scopeConfig->getValue(
    //         self::XML_PATH_MODULE_ENABLE,
    //         \Magento\Store\Model\ScopeInterface::SCOPE_STORE
    //     );
    // }

    public function getSlidertype()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SLIDER_TYPE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getBannerWidth()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_BANNER_WIDTH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getBannerHeight()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_BANNER_HEIGHT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function showBannerTitle()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHOW_BANNER_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function showBannerContent()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SHOW_BANNER_CONTENT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /* Camera Slider Cinfiguration */

    public function getAnimation()
    {
        return $this->scopeConfig->getValue(
            'articles/cameraslider/animation',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function pauseOnHover()
    {
        $poh = $this->scopeConfig->getValue(
            'articles/cameraslider/pauseonhover',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($poh) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getLoader()
    {
        return $this->scopeConfig->getValue(
            'articles/cameraslider/loader',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function showNavBar()
    {
        $snb = $this->scopeConfig->getValue(
            'articles/cameraslider/shownavigation',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($snb) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function showPagination()
    {
        $spg = $this->scopeConfig->getValue(
            'articles/cameraslider/pagination',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($spg) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getSlideDuration()
    {
        return $this->scopeConfig->getValue(
            'articles/cameraslider/duration',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /* Simple Slider Configuration */

    public function autoPlayEnable()
    {
        $ape = $this->scopeConfig->getValue(
            'articles/simpleslider/autoplay',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($ape) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getInterval()
    {
        return $this->scopeConfig->getValue(
            'articles/simpleslider/interval',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function simplePauseOnHove()
    {
        $spoh = $this->scopeConfig->getValue(
            'articles/simpleslider/pauseonhover',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($spoh) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getSimpleAnimation()
    {
        return $this->scopeConfig->getValue(
            'articles/simpleslider/animation',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * getMediaUrl
     * @return string
     */
    public function getMediaUrl()
    {

        return $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
    }

    /**
     *
     * @param  $imgUrl
     * @param  $x
     * @param  $y
     * @param  $imagePath
     * @return string
     */
    public function resizeImage($imgUrl, $x = null, $y = null, $imagePath = null)
    {
        
        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                ->getDirectoryRead(DirectoryList::MEDIA);
        $baseScmsMediaURL = $mediaDirectory->getAbsolutePath() . 'categorybanners/images';
        
        if ($x == null && $y == null) {
            $x = $this->getBannerWidth();
            $y = $this->getBannerHeight();
            if ($x == null && $y == null) {
                $x = 200;
                $y = 200;
            }
        }

        $imgPath = $this->splitImageValue($imgUrl, "path");
        $imgName = $this->splitImageValue($imgUrl, "name");



        /**
         * Path with Directory Seperator
         */
        $imgPath = str_replace("/", '/', $imgPath);
        /**
         * Absolute full path of Image
         */
        $imgPathFull = $baseScmsMediaURL . $imgPath . '/' . $imgName;



        /**
         * If Y is not set set it to as X
         */
        $width = $x;
        $y ? $height = $y : $height = $x;

        /**
         * Resize folder is widthXheight
         */
        $resizeFolder = $width . "X" . $height;

        /**
         * Image resized path will then be
         */
        $imageResizedPath = $baseScmsMediaURL . $imgPath . '/' . $resizeFolder . '/' . $imgName;

        /**
         * First check in cache i.e image resized path
         * If not in cache then create image of the width=X and height = Y
         */
        $colorArray = [];
        $color = "255,255,255";
        $colorArray = explode(",", $color);

        //print_r($colorArray); exit();
        if (!file_exists($imageResizedPath) && file_exists($imgPathFull)) :
            $imageObj = $this->_imageFactory->create($imgPathFull);
            $imageObj->constrainOnly(true);
            $imageObj->keepAspectRatio(false);
            $imageObj->resize($width, $height);
            $imageObj->save($imageResizedPath);
        endif;

        /**
         * Else image is in cache replace the Image Path with / for http path.
         */
        $imgUrl = str_replace('/', "/", $imgPath);

        /**
         * Return full http path of the image
         */
        return $this->getMediaUrl() . $imgUrl . "/" . $imgName;
    }

    /**
     * splitImageValue
     * @param  $imageValue
     * @param  string $attr
     * @return string
     */
    public function splitImageValue($imageValue, $attr = "name")
    {
        $imArray = explode("/", $imageValue);

        $name = $imArray[count($imArray) - 1];
        $path = implode("/", array_diff($imArray, [$name]));
        if ($attr == "path") {
            return $path;
        } else {
            return $name;
        }
    }

    public function getFileThumbPaths($file)
    {
        $paths = [];
        $x1 = $this->getBannerWidth();
        $y1 = $this->getBannerHeight();
        $x2 = 100;
        $y2 = 75;

        $name = $this->splitImageValue($file, "name");

        $resized_path1 = 'categorybanners' . DIRECTORY_SEPARATOR . $x1 . 'X' . $y1 . DIRECTORY_SEPARATOR . $name;
        $resized_path2 = 'categorybanners' . DIRECTORY_SEPARATOR . $x2 . 'X' . $y2 . DIRECTORY_SEPARATOR . $name;

        $paths['path1'] = $resized_path1;
        $paths['path2'] = $resized_path2;
        return $paths;
    }
}
