<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class Article extends AbstractHelper
{
    protected $_timezoneInterface;
    const ARTICLE_MODULE_ENABLED               =   'articles/basic_configs/article_mod_enable';
    const ARTICLE_MODULE_MODE                  =   'articles/basic_configs/slider_type';
    const ARTICLE_HEADER_LINK_TITLE            =   'articles/basic_configs/article_header_link';
    const ARTICLE_HEADER_LINK_ENABLE           =   'articles/basic_configs/article_header_link_enable';
    const ARTICLE_BOTTOM_LINK_ENABLE           =   'articles/basic_configs/article_bottom_link_enable';
    const ARTICLE_BOTTOM_LINK_TITLE            =   'articles/basic_configs/article_bottom_link';
    const ARTICLE_POST_PERPAGE                 = 'articles/basic_config/article_post_perpage';
    const ARTICLE_READ_MORE                    = 'articles/basic_config/article_read_more';
    const ARTICLE_SHOW_DATE                    = 'articles/basic_config/article_show_date';
    const ARTICLE_COMMENT_COUNTER              = 'articles/basic_config/article_comments_counter';
    const ARTICLE_POST_ENABLE                  = 'articles/article_post_settings/article_post_enable';
    const ARTICLE_POST_TITLE                   = 'articles/article_post_settings/article_post_title';
    const ARTICLE_POST_NUMPOSTS                = 'articles/article_post_settings/article_post_numposts';
    const ARTICLE_POST_VIEW_ALL                = 'articles/article_post_settings/article_post_viewall';
    const ARTICLE_POST_BLOCK_LEFT              = 'articles/article_post_settings/article_post_blockleft';
    const ARTICLE_POST_BLOCKRIGHT              = 'articles/article_post_settings/article_post_blockright';
    const ARTICLE_POST_RSSBLOCKLEFT            = 'articles/article_post_settings/article_post_rssblockleft';
    const ARTICLE_POST_RSSBLOCKRIGHT           = 'articles/article_post_settings/article_post_rssblockright';
    const ARTICLE_HOME_POST_ENABLE             = 'articles/article_homepost_settings/article_post_enable';
    const ARTICLE_HOMEPOST_TITLE               = 'articles/article_homepost_settings/article_homepost_title';
    const ARTICLE_HOMEPOST_NUMPOSTS            = 'articles/article_homepost_settings/article_homepost_numposts';
    const ARTICLE_POSTGALLERY_ENABLE           = 'articles/article_post_gallery/article_post_galleryenable';
    const ARTICLE_POST_GALLERYTHEME            = 'articles/article_post_gallery/article_post_gallerytheme';
    const ARTICLE_PAGE_TITLE_SEO               =   'articles/article_seo_info/article_page_title';
    const ARTICLE_PAGE_METAKEYWORD_SEO         =   'articles/article_seo_info/article_meta_keywords';
    const ARTICLE_PAGE_METADESCRIPTION_SEO     =   'articles/article_seo_info/article_meta_description';
    const ARTICLE_URL_PREFIX_SEO               =   'articles/article_seo_info/article_url_prefix';
    const ARTICLE_URL_SUFFIX_SEO               =   'articles/article_seo_info/article_url_suffix';
    const ARTICLE_CATEGORY_URL_SEO             =   'articles/article_seo_info/article_category_url';
    const ARTICLE_SLIDER_ENABLE                =  'articles/article_slider_settings/article_slider_enable';
    const ARTICLE_SLIDER_THEME                 = 'articles/article_slider_settings/article_slider_theme';
    const ARTICLE_SLIDER_EFFECTS               = 'articles/article_slider_settings/article_slider_effects';
    const ARTICLE_SLIDER_PRENEXT               = 'articles/article_slider_settings/article_slider_prenext';
    const ARTICLE_SLIDER_PAGINATION            = 'articles/article_slider_settings/article_slider_pagination';
    const ARTICLE_ATTACHED_PRODUCTS            = 'articles/article_detail/article_attached_products';
    const ARTICLE_SHARING_OPTIONS              = 'articles/article_detail/article_sharing_options';
    const ARTICLE_RELATED_POSTS                = 'articles/article_detail/article_related_posts';
    const ARTICLE_ALLOW_COMMENTS               = 'articles/article_detail/article_allow_comments';
    const ARTICLE_SEARCH_ENABLE                = 'articles/article_search_block/article_search_enable';
    const ARTICLE_SEARCH_BUTTON                = 'articles/article_search_block/article_search_button';
    const ARTICLE_SEARCH_BLOCKTITLE            = 'articles/article_search_block/article_search_blocktitle';
    const ARTICLE_SEARCH_SHOW_LEFT             = 'articles/article_search_block/article_search_left';
    const ARTICLE_SEARCH_SHOW_RIGHT            = 'articles/article_search_block/article_search_right';
    const ARTICLE_PRODUCTPAGE_ENABLE           = 'articles/article_product_page/article_productpage_enable';
    const ARTICLE_PRODUCTPAGE_TITLE            = 'articles/article_product_page/article_product_pagetitle';
    const ARTICLE_PRODUCTPAGE_NUMBLOCK         = 'articles/article_product_page/article_product_numblock';
    const ARTICLE_CATEGORYBLOCK_ENABLE         = 'articles/article_category_block/article_categoryblock_enable';
    const ARTICLE_CATEGORYBLOCK_TITLE          = 'articles/article_category_block/article_category_blocktitle';
    const ARTICLE_CATEGORY_NUMBLOCK            = 'articles/article_category_block/article_category_numblock';
    const ARTICLE_CATEGORYBLOCK_LEFT           = 'articles/article_category_block/article_categoryblock_left';
    const ARTICLE_CATEGORYBLOCK_RIGHT          = 'articles/article_category_block/article_categoryblock_right';
    const ARTILCE_ARCHIVE_ENABLE               = 'articles/article_archive_block/article_archive_enable';
    const ARTICLE_ARCHIVE_BLOCKTITLE           = 'articles/article_archive_block/article_archive_blocktitle';
    const ARTICLE_ARCHIVES_NUMARCHIVES         = 'articles/article_archive_block/article_category_numarchives';
    const ARTICLE_ARCHIVE_LEFT                 = 'articles/article_archive_block/article_archive_left';
    const ARTICLE_ARCHIVE_RIGHT                = 'articles/article_archive_block/article_archive_right';

    public function __construct(
            \Magento\Framework\App\Helper\Context $context,         
            \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface
    ) 
    {
        $this->_timezoneInterface = $timezoneInterface;
        parent::__construct($context);
    }

    public function getTimeAccordingToTimeZone($dateTime)
    {
        
        $today = $this->_timezoneInterface->date()->format('m/d/y H:i:s');    
        $dateTimeAsTimeZone = $this->_timezoneInterface
                                        ->date(new \DateTime($dateTime))
                                        ->format('m/d/y H:i:s');
        return $dateTimeAsTimeZone;
    }

    public function isArticleModuleEnable()
    {
         $isEnabled = true;
         $enabled = $this->scopeConfig->getValue(self::ARTICLE_MODULE_ENABLED, ScopeInterface::SCOPE_STORE);
        if ($enabled == null || $enabled == '0') {
            $isEnabled = false;
        }
         return $isEnabled;
    }
     

    public function getSlidertype()
    {   
        return $this->scopeConfig->getValue(self::ARTICLE_MODULE_MODE, ScopeInterface::SCOPE_STORE);
    }
    public function isArticleVideGallEnable()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_POSTGALLERY_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    public function isArticleRssEnable()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_POST_RSSBLOCKLEFT, ScopeInterface::SCOPE_STORE);
    }
    
    public function isArticleHeaderLinkEnable()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_HEADER_LINK_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    public function isArticleBottomLinkEnable()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_BOTTOM_LINK_ENABLE, ScopeInterface::SCOPE_STORE);
    }

    public function getArticlePostPerpage()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_POST_PERPAGE, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleReadMore()
    {

        return $this->scopeConfig->getValue(self::ARTICLE_READ_MORE, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleShowDate()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_SHOW_DATE, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleCommentCounter()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_COMMENT_COUNTER, ScopeInterface::SCOPE_STORE);
    }
    public function getArticlePostEnable()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_POST_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticlePostTitle()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_POST_TITLE, ScopeInterface::SCOPE_STORE);
    }
    public function getArticlePostNumPost()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_POST_NUMPOSTS, ScopeInterface::SCOPE_STORE);
    }
    public function articleHeaderLinkTiltle()
    {
        if (self::isArticleHeaderLinkEnable()) {
            return $this->scopeConfig->getValue(self::ARTICLE_HEADER_LINK_TITLE, ScopeInterface::SCOPE_STORE);
        }
    }

    public function articleBottomLinkTitle()
    {
        if (self::isArticleBottomLinkEnable()) {
            return $this->scopeConfig->getValue(self::ARTICLE_BOTTOM_LINK_TITLE, ScopeInterface::SCOPE_STORE);
        }
    }
    
    public function getArticlePostViewAll()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_POST_VIEW_ALL, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleHomePostEnable()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_HOME_POST_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleHomePostTitle()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_HOMEPOST_TITLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleHomePostNumPost()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_HOMEPOST_NUMPOSTS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticlePostGalleryEnable()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_POSTGALLERY_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    public function getArticlePostGalleryTheme()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_POST_GALLERYTHEME, ScopeInterface::SCOPE_STORE);
    }
    public function getArticlePageTitleSeo()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_PAGE_TITLE_SEO, ScopeInterface::SCOPE_STORE);
    }
    public function getArticlePageMetakeywordSeo()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_PAGE_METAKEYWORD_SEO, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticlePageMetadescriptionSeo()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_PAGE_METADESCRIPTION_SEO, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSeoPrefix()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_URL_PREFIX_SEO, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleSeoSuffix()
    {
            return $this->scopeConfig->getValue(self::ARTICLE_URL_SUFFIX_SEO, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleCategorySeo()
    {
          return $this->scopeConfig->getValue(self::ARTICLE_CATEGORY_URL_SEO, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleFinalIdentifier()
    {
        if ($this->getArticleSeoPrefix()) {
            return $this->getArticleSeoPrefix().$this->getArticleSeoSuffix();
        } else {
            return 'article';
        }
    }
    
    public function getArticleFinalDetailIdentifier($detailId)
    {
        if ($this->getArticleSeoPrefix()) {
            //return $this->getArticleSeoPrefix().'/'.$detailId.$this->getArticleSeoSuffix();
            return $detailId.$this->getArticleSeoSuffix();
        } else {
            return 'article/'.$detailId.$this->getArticleSeoSuffix();
        }
    }

    public function getArticleFinalCategoryIdentifier($detailId)
    {
        if ($this->getArticleSeoPrefix()) {
            //return $this->getArticleSeoPrefix().'/cat/'.$detailId.$this->getArticleSeoSuffix();
            return 'cat/'.$detailId.$this->getArticleSeoSuffix();
        } else {
            return 'article/'.'cat/'.$detailId.$this->getArticleSeoSuffix();
        }
    }
    
    
    public function getArticleLink()
    {
        $identifier = $this->getArticleSeoPrefix();
        $seo_suffix = $this->getArticleSeoSuffix();
        if (isset($identifier) && isset($seo_suffix)) {
            return $identifier.$seo_suffix;
        } else {
            return 'article';
        }
    }
    
    public function isArticleSliderEnable()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SLIDER_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSliderTheme()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SLIDER_THEME, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSliderEffects()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SLIDER_EFFECTS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSliderPreviousNext()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SLIDER_PRENEXT, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSliderPagination()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SLIDER_PAGINATION, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticelAttachedProducts()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_ATTACHED_PRODUCTS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSharingOption()
    {
        return $this->scopeConfig->getvalue(self::ARTICLE_SHARING_OPTIONS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleRelatedPosts()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_RELATED_POSTS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleAllowComments()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_ALLOW_COMMENTS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSearchEnable()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SEARCH_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSerachButton()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SEARCH_BUTTON, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleSearchBlockTitle()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_SEARCH_BLOCKTITLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleProductPageEnable()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_PRODUCTPAGE_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleProductPageTitle()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_PRODUCTPAGE_TITLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleProductPageNumBlock()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_PRODUCTPAGE_NUMBLOCK, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleCategoryBlockEnable()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_CATEGORYBLOCK_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleCategoryBlockTitle()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_CATEGORYBLOCK_TITLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleCategoryNumBlock()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_CATEGORY_NUMBLOCK, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleArchiveEnable()
    {
        return $this->scopeConfig->getValue(self::ARTILCE_ARCHIVE_ENABLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleArchiveBlockTitle()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_ARCHIVE_BLOCKTITLE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleArchiveNumArchives()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_ARCHIVES_NUMARCHIVES, ScopeInterface::SCOPE_STORE);
    }

    public function getStorename()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/name',
            ScopeInterface::SCOPE_STORE
        );
    }

    public function getStoreEmail()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_sales/email',
            ScopeInterface::SCOPE_STORE
        );
    }
}
