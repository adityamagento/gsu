<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class ArticleSocial extends AbstractHelper
{

    const ARTICLE_SOCIAL_FACEBOOK_URL               = 'articles/article_social_facebook/article_social_fbookpageurl';
    const ARTICLE_SOCIAL_FACEBOOK_APPID               = 'articles/article_social_facebook/article_facebook_appid';
    const ARTICLE_SOCIAL_FACEBOOK_NUMPOSTS               = 'articles/article_social_facebook/article_facebook_numposts';
    
    const ARTICLE_FACEBOOK_WIDH                     = 'articles/article_social_facebook/article_social_fbpagewidth';
    const ARTICLE_FACEBOOK_HEIGHT                   = 'articles/article_social_facebook/article_social_fbpageheight';
    const ARTICLE_FACEBOOK_COVER_PHOTO              = 'articles/article_social_facebook/article_social_fbcoverphoto';
    const ARTICLE_FACEBOOK_SMALLHEADER              = 'articles/article_social_facebook/article_social_fbsmallheader';
    const ARTICLE_FACEBOOK_SHOWFRIENDFACE           = 'articles/article_social_facebook/article_social_fbshowfriendface';
    const ARTICLE_FACEBOOK_SHOWONLEFT               = 'articles/article_social_facebook/article_social_fbleft';
    
    const ARTICLE_TWITTER_USERNAME                  = 'articles/article_social_twitter/article_social_twitterusername';
    const ARTICLE_TWITTER_TWWIDGETID                = 'articles/article_social_twitter/article_social_twitterwidgetid';
    const ARTICLE_TWITTER_HEIGHT                    = 'articles/article_social_twitter/article_social_twitterheight';
    const ARTICLE_TWITTER_THEME                     = 'articles/article_social_twitter/article_social_twittertheme';
    const ARTICLE_TWITTER_SHOWONLEFT                = 'articles/article_social_twitter/article_social_twitterleft';
    
    const ARTICLE_RSS_URL                           = 'articles/article_rss_external/article_rss_url';
    const ARTICLE_RSS_NUMBEROFFEEDS                 = 'articles/article_rss_external/article_rss_numfeeds';
    const ARTICLE_RSS_THEME                         = 'articles/article_rss_external/article_rss_theme';
    const ARTICLE_RSS_SHOWONLEFT                    = 'articles/article_rss_external/article_rss_rssleft';
    
    const ARTICLE_LAYOUT_NUMBEROFCOLUMNS            = 'articles/article_rss_external/article_layout_numcolumns';
    
    public function getArticleSocialFacebookEnable()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_SHOWONLEFT, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleSocialFacebookUrl()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_SOCIAL_FACEBOOK_URL, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleSocialFacebookAppId()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_SOCIAL_FACEBOOK_APPID, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleSocialFacebookNumPosts()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_SOCIAL_FACEBOOK_NUMPOSTS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleFacebookWidth()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_WIDH, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleFbHeight()
    {

        return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_HEIGHT, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleFbCoverPhoto()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_COVER_PHOTO, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleFbSmallHeader()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_SMALLHEADER, ScopeInterface::SCOPE_STORE);
    }
    public function getArticleFbFriendFace()
    {
        
        return $this->scopeConfig->getValue(self::ARTICLE_FACEBOOK_SHOWFRIENDFACE, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleTwitterUserName()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_TWITTER_USERNAME, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleTwitterEnable()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_TWITTER_SHOWONLEFT, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleTwitterTwId()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_TWITTER_TWWIDGETID, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleTwitterHeight()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_TWITTER_HEIGHT, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleTwitterTheme()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_TWITTER_THEME, ScopeInterface::SCOPE_STORE);
    }

    public function getArticleRssEnable()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_RSS_SHOWONLEFT, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleRssUrl()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_RSS_URL, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleRssNumberFields()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_RSS_NUMBEROFFEEDS, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleRssTheme()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_RSS_THEME, ScopeInterface::SCOPE_STORE);
    }
    
    public function getArticleLayoutNumColumns()
    {
        return $this->scopeConfig->getValue(self::ARTICLE_LAYOUT_NUMBEROFCOLUMNS, ScopeInterface::SCOPE_STORE);
    }
}
