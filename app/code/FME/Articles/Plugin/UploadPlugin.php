<?php
 
namespace FME\Articles\Plugin;
 
class UploadPlugin
{ 

   public function aroundCheckMimeType($subject, \Closure $proceed, $validTypes = [])
    {
        $allowedMimeTypesFme = [        
        'video/x-flv',
        'video/mp4',
        'video/mov',
        'image/jpg',
        'image/jpeg',
        'image/gif',
        'image/png',
        'video/x-ms-wmv',
        'video/avi',
        'video/x-sgi-movie',
        'application/x-shockwave-flash'

    ];
    
    return $proceed($allowedMimeTypesFme);
        
    }

}