<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    
    public $categorySetupFactory;

    
    public function __construct(
        \FME\Articles\Setup\CategorySetupFactory $categorySetupFactory
    ) {
    
        $this->categorySetupFactory = $categorySetupFactory;
    }
    
    public function install(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
    
        $contextInstall = $context;
        $contextInstall->getVersion();
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
       
        $category = $categorySetup->createCategory();
        $category
            ->setParentId('0')
            ->setStoreId(1)
            ->setCategoryName('Default Category')
            ->setCategoryUrlKey('def-cat')
            ->setMetaKeywords('def-cat')
            ->setMetaDescription('def-cat')
            ->setInitialSetupFlag(true)
            ->save();
    }
}
