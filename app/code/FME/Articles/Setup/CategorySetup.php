<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Setup;

class CategorySetup
{
    
    public $setup;

    
    public $categoryFactory;

    
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \FME\Articles\Model\CategoryFactory $categoryFactory
    ) {
    
        $this->setup           = $setup;
        $this->categoryFactory = $categoryFactory;
    }

    
    public function createCategory($data = [])
    {
        return $this->categoryFactory->create($data);
    }
}
