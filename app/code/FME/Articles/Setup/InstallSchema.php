<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        /**
         * Create table 'fme_Category'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_categories')
        )->addColumn(
            'category_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Category ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'Parent ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Store ID'
        )->addColumn(
            'category_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Category Name'
        )->addColumn(
            'category_url_key',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Category Url Key'
        )->addColumn(
            'category_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Category Order'
        )->addColumn(
            'meta_keywords',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Meta Keywords'
        )->addColumn(
            'meta_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Description'
        )->addColumn(
            'left_node',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Left Node'
        )->addColumn(
            'right_node',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Right Node'
        )->addColumn(
            'creation_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Category Creation Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Category Modification Time'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Category Active'
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('fme_categories'),
                ['category_name', 'category_url_key'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['category_name', 'category_url_key'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'FME Category Table'
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_articles'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_articles')
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Articles ID'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Articles Title'
        )->addColumn(
            'article_publish_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Article Publish Date'
        )->addColumn(
            'image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Image'
        )->addColumn(
            'identifier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Identifier'
        )->addColumn(
            'articlesdetail',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Articles Detail'
        )->addColumn(
            'author',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Author'
        )->addColumn(
            'rating',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'Ratings'
        )->addColumn(
            'rating_voices',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => true],
            'Ratings Voices'
        )->addColumn(
            'video_url',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Video Url'
        )->addColumn(
            'article_meta_title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Meta Title'
        )->addColumn(
            'article_meta_keywords',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Meta Keywords'
        )->addColumn(
            'article_meta_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Description'
        )->addColumn(
            'artilce_short_summary',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Short Summary'
        )->addColumn(
            'creation_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Articles Creation Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Articles Modification Time'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Article Active'
        )->addColumn(
            'is_comments',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Comments Enable'
        )->addColumn(
            'featured',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '0'],
            'Is Article Featured'
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('fme_articles'),
                ['title', 'identifier'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['title', 'identifier'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'FME Articles Table'
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_comments'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_comments')
        )->addColumn(
            'comments_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Comments ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Articles ID'
        )->addColumn(
            'articles_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Articles Name'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Name'
        )->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Email'
        )->addColumn(
            'commentsdetail',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => true],
            'Comments Detail'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Store ID'
        )->addColumn(
            'creation_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Comment Creation Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Comment Modification Time'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Article Active'
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('fme_comments'),
                ['articles_name', 'name'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['articles_name', 'name'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'FME Comments Table'
        )->addForeignKey(
            $installer->getFkName('fme_comments', 'articles_id', 'fme_articles', 'articles_id'),
            'articles_id',
            $installer->getTable('fme_articles'),
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_articles_media'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_articles_media')
        )->addColumn(
            'imedia_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Media ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Article ID'
        )->addColumn(
            'file',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Media Path'
        )->addColumn(
            'label',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Media Label'
        )->addColumn(
            'position',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => true, 'primary' => false],
            'Media Position'
        )->addForeignKey(
            $installer->getFkName('fme_articles_media', 'articles_id', 'fme_articles', 'articles_id'),
            'articles_id',
            $installer->getTable('fme_articles'),
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addIndex(
            $installer->getIdxName('fme_articles', ['articles_id']),
            ['articles_id']
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_media'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_media')
        )->addColumn(
            'imedia_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Media ID'
        )->addColumn(
            'file',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Media Path'
        )->addColumn(
            'label',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Media Label'
        )->addColumn(
            'position',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => true, 'primary' => false],
            'Media Position'
        );
        $installer->getConnection()->createTable($table);
/*
    Multisotre Table ..fme_category_store
*/
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_category_store')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'category_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Category ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            255,
            ['nullable' => false, 'primary' => true],
            'Store ID'
        )->addForeignKey(
            $installer->getFkName('fme_category_store', 'category_id', 'fme_categories', 'category_id'),
            'category_id',
            $installer->getTable('fme_categories'),
            'category_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'fme_authors'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_authors')
        )->addColumn(
            'author_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Author ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Author Name'
        )->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Author Email'
        )->addColumn(
            'image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Author Image'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            'null',
            [],
            'Author Status'
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_author_articles'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_author_articles')
        )->addColumn(
            'author_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => false, 'nullable' => false, 'primary' => true],
            'Author ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Article ID'
        )->addForeignKey(
            $installer->getFkName('fme_author_articles', 'author_id', 'fme_authors', 'author_id'),
            'author_id',
            $installer->getTable('fme_authors'),
            'author_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('fme_author_articles', 'articles_id', 'fme_articles', 'articles_id'),
            'articles_id',
            $installer->getTable('fme_articles'),
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addIndex(
            $installer->getIdxName('fme_articles', ['articles_id']),
            ['articles_id']
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_articles_category_link'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_articles_category')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Aticles Cat ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => false, 'nullable' => false, 'primary' => true],
            'Aticles ID'
        )->addColumn(
            'category_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Categorry ID'
        )
        ->addIndex(
            $installer->getIdxName('fme_articles', ['articles_id']),
            ['articles_id']
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_tags'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_tags')
        )->addColumn(
            'tag_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Aticles Cat ID'
        )->addColumn(
            'tag_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Tag Name'
        )->addColumn(
            'tag_image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Tag Image'
        )->addIndex(
            $installer->getIdxName('fme_tags', ['tag_id']),
            ['tag_id']
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_articles_tags'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_articles_tags')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Aticles Tag ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false,'primary' => true],
            'Aticles ID'
        )->addColumn(
            'tag_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false,'primary' => true],
            'Tag ID'
        )->addForeignKey(
            $installer->getFkName('fme_articles_tags', 'articles_id', 'fme_articles', 'articles_id'),
            'articles_id',
            $installer->getTable('fme_articles'),
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('
                ', 'tag_id', 'fme_tags', 'tag_id'),
            'tag_id',
            $installer->getTable('fme_tags'),
            'tag_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addIndex(
            $installer->getIdxName('fme_articles', ['articles_id']),
            ['articles_id']
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_block_articles'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_block_articles')
        )->addColumn(
            'articles_block_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Block ID'
        )->addColumn(
            'block_title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Blck Title'
        )->addColumn(
            'block_identifier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Block Identifier'
        )->addColumn(
            'block_status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['identity' => false, 'nullable' => false],
            'Block Status'
        )->addColumn(
            'block_area',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Block Area'
        )->addColumn(
            'block_content',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Block Content'
        )->addColumn(
            'related_articles',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Related Articles'
        )->addIndex(
            $installer->getIdxName('fme_block_articles', ['articles_block_id']),
            ['articles_block_id']
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_articles_product's
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_article_products')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Article ID'
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'primary' => true],
            'Product ID'
        )->addForeignKey(
            $installer->getFkName('fme_article_products', 'articles_id', 'fme_articles', 'articles_id'),
            'articles_id',
            $installer->getTable('fme_articles'),
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
                
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_articles_product's
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_article_articles')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Article ID'
        )->addColumn(
            'related_articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'primary' => true],
            'Product ID'
        )->addForeignKey(
            $installer->getFkName('fme_article_articles', 'articles_id', 'fme_articles', 'articles_id'),
            'articles_id',
            $installer->getTable('fme_articles'),
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
                
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'fme_articles_blog_articles'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('fme_articles_blog_articles')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Aticles Tag ID'
        )->addColumn(
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => false, 'nullable' => false, 'primary' => true],
            'Aticles ID'
        )->addColumn(
            'articles_block_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Articles Blog ID'
        )->addForeignKey(
            $installer->getFkName('fme_articles_blog_articles', 'articles_id', 'fme_articles', 'articles_id'),
            'articles_id',
            $installer->getTable('fme_articles'),
            'articles_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('fme_articles_blog_articles
                ', 'articles_block_id', 'fme_block_articles', 'articles_block_id'),
            'articles_block_id',
            $installer->getTable('fme_block_articles'),
            'articles_block_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addIndex(
            $installer->getIdxName('fme_articles', ['articles_id']),
            ['articles_id']
        );
        $installer->getConnection()->createTable($table);
        /**
         * Create table 'Product Videos Table'
         */
        $table = $installer->getConnection()
          ->newTable($installer->getTable('fme_articles_productvideos'))
          ->addColumn(
              'video_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
              'Id'
          )
          ->addColumn(
              'title',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['nullable' => false],
              'Title'
          )
          ->addColumn(
              'content',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['nullable' => true,'default' => ''],
              'Content'
          )
          ->addColumn(
              'video_type',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['nullable' => false],
              'Video Type'
          )
          ->addColumn(
              'video_url',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['nullable' => true,'default' => ''],
              'Video Url'
          )
          ->addColumn(
              'video_file',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['nullable' => true,'default' => ''],
              'Video File'
          )
          ->addColumn(
              'video_thumb',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              null,
              ['nullable' => true, 'default' => null],
              'Thumbnail'
          )
          ->addColumn(
              'status',
              \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
              null,
              ['nullable' => false, 'default' =>1],
              'Status'
          )
          ->addColumn(
              'created_time',
              \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
              null,
              ['nullable' => false, 'default' => 0],
              'Created Time'
          )
          ->addColumn(
              'update_time',
              \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
              null,
              ['nullable' => false, 'default' => 0],
              'Update Time'
          )
          ->addIndex(
              $installer->getIdxName('video_id', ['video_id']),
              ['video_id']
          )
          ->setComment('Productvideos Table');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'Productvideos Products Table'
         */
        $table = $installer->getConnection()
          ->newTable($installer->getTable('fme_articles_related_videos'))
          ->addColumn(
              'id',
              \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
              null,
              ['identity' => true, 'nullable' => false, 'primary' => true],
              'ID'
          )->addColumn(
              'articles_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
              null,
              ['nullable' => false, 'primary' => true],
              'Article ID'
          )->addColumn(
              'video_id',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
              255,
              ['nullable' => false, 'primary' => true],
              'Video ID'
          )->addForeignKey(
              $installer->getFkName('fme_articles_related_videos', 'articles_id', 'fme_articles', 'articles_id'),
              'articles_id',
              $installer->getTable('fme_articles'),
              'articles_id',
              \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
          );
        $installer->getConnection()->createTable($table);
    }
}
