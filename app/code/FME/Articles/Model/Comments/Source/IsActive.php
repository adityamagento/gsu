<?php
namespace FME\Articles\Model\Comments\Source;

use Magento\Framework\Data\OptionSourceInterface;

class IsActive implements OptionSourceInterface
{
    protected $googleStore;
  
    public function __construct(\FME\Articles\Model\Comments $googleStore)
    {
        $this->googleStore = $googleStore;
    }
    
    public function toOptionArray()
    {
        $availableOptions = $this->googleStore->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
