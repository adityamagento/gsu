<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Model;

class Article extends \Magento\Framework\Model\AbstractModel
{
        const STATUS_ENABLED = 1;
        const STATUS_DISABLED = 0;

    protected function _construct()
    {
        $this->_init('FME\Articles\Model\ResourceModel\Article');
    }

    public function getAvailableStatuses()
    {
        $availableOptions = ['0' => 'Disable',
                           '1' => 'Enable'];
        return $availableOptions;
    }

    public function getAvailableThemes()
    {
        $availableThemes = ['0' => 'Default',
                            '1' => 'Dark',
                            '2' => 'Light'];
        return $availableThemes;
    }

    public function getAvailableThemesEffects()
    {
        $availableThemes = ['0' => 'Random',
                            '1' => 'Fold',
                            '2' => 'Fade',
                            '3' => 'Slice Down'];
        return $availableThemes;
    }

    public function getImagePosition()
    {
        if (!$this->getId()) {
            return [];
        }
        $array = $this->getData('image_position');

        if ($array === null) {
            $temp = $this->getData('articles_id');

            $tagsname = $this->getRelatedProducts($temp);
         if($tagsname> -1){
             for ($i = 0; $i < sizeof($tagsname); $i++) {
             $array[$tagsname[$i]] = 0;
                 }
               }
            $this->setData('image_position', $array);
        }
        return $array;
    }

    public function getVideoPosition()
    {
        if (!$this->getId()) {
            return [];
        }
        $array = $this->getData('video_position');

        if ($array === null) {
            $temp = $this->getData('articles_id');
            $tagsname = $this->getRelatedVideos($temp);
        if($tagsname> -1){
            for ($i = 0; $i < sizeof($tagsname); $i++) {
                $array[$tagsname[$i]] = 0;
            }
           }
            $this->setData('video_position', $array);
        }
        return $array;
       }

    public function getArticlesPosition()
    {
        if (!$this->getId()) {
            return [];
        }
        $array = $this->getData('articles_position');

        if ($array === null) {
            $temp = $this->getData('articles_id');
            $tagsname = $this->getRelatedArticles($temp);
if($tagsname> -1){
            for ($i = 0; $i < sizeof($tagsname); $i++) {
                $array[$tagsname[$i]] = 0;
            }
}
            $this->setData('articles_position', $array);
        }
        return $array;
    }

    public function getProductsPosition()
    {
        if (!$this->getId()) {
            return [];
        }
        $array = $this->getData('products_position');

        if ($array === null) {
            $temp = $this->getData('articles_id');
            $tagsname = $this->getRelatedPro($temp);
if($tagsname> -1){
            for ($i = 0; $i < sizeof($tagsname); $i++) {
                $array[$tagsname[$i]] = 0;
            }
}
            $this->setData('products_position', $array);
        }
        return $array;
    }

    public function getRelatedProducts($id)
    {

        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_articles_media'))->where('articles_id = ?', $id);
        $data = $this->_getResource()->getConnection()->fetchAll($select);

        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['imedia_id'];
            }

            return $productsArr;
        }
    }

    public function getRelatedArticles($id)
    {

        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_article_articles'))->where('articles_id = ?', $id);
        $data = $this->_getResource()->getConnection()->fetchAll($select);

        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['related_articles_id'];
            }

            return $productsArr;
        }
    }

    public function getArticlesForProductPage($pid)
    {
        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_article_products'))->where('entity_id = ?', $pid);
        $data = $this->_getResource()->getConnection()->fetchAll($select);
        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['articles_id'];
            }
            return $productsArr;
        }
    }

    public function getRelatedPro($id)
    {

        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_article_products'))->where('articles_id = ?', $id);
        $data = $this->_getResource()->getConnection()->fetchAll($select);

        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['entity_id'];
            }

            return $productsArr;
        }
    }

    public function getRelatedCategories($id)
    {

        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_articles_category'))->where('articles_id = ?', $id);
        $data = $this->_getResource()->getConnection()->fetchAll($select);

        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['category_id'];
            }

            return $productsArr;
        }
    }

    public function getRelatedCommentsFront($id)
    {

        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_comments'))->where('articles_id = ?', $id);
        $data = $this->_getResource()->getConnection()->fetchAll($select);
        return $data;

        // if ($data) {
        //     $productsArr = [];
        //     foreach ($data as $_i) {
        //         $productsArr[] = $_i['category_id'];
        //     }

        //     return $productsArr;
        // }
    }
    public function getRelatedVideos($id)
    {

        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_articles_related_videos'))->where('articles_id = ?', $id);
        $data = $this->_getResource()->getConnection()->fetchAll($select);

        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['video_id'];
            }

            return $productsArr;
        }
    }

    public function getRelatedVideosAndBanners($id)
    {
        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_articles_media'))->where('articles_id = ?', $id);
        $data = $this->_getResource()->getConnection()->fetchAll($select);

        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['imedia_id'];
            }

            return $productsArr;
        }
    }



    public function getTagsDistinctCollection()
    {
        $select = $this->_getResource()
                       ->getConnection()
                       ->select()
                       ->from($this->_getResource()->getTable('fme_articles_tags'))
                       ->group('tag_names');
        $data = $this->_getResource()->getConnection()->fetchAll($select);
        return $data;
    }

    public function getArticlesByTags($tagi)
    {
        $select = $this->_getResource()
                        ->getConnection()
                        ->select()
                        ->from($this->_getResource()->getTable('fme_articles_tags'), 'articles_id')
                        ->where('tag_names = ?', $tagi);
        $data = $this->_getResource()->getConnection()->fetchAll($select);
        return $data;
    }
}
