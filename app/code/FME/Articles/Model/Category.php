<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */

namespace FME\Articles\Model;

class Category extends \Magento\Framework\Model\AbstractModel
{
        const STATUS_ENABLED = 1;
        const STATUS_DISABLED = 0;
    protected $_logger;
    protected function _construct()
    {
        $this->_init('FME\Articles\Model\ResourceModel\Category');
    }
    public function getAvailableStatuses()
    {
        $availableOptions = ['0' => 'Disable',
                           '1' => 'Enable'];
        return $availableOptions;
    }
    public function getParentCat()
    {
        $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_categories'), ['id'=>'category_id','category_name'])
        ->where('is_active = ?', '1');
        $data = $this->_getResource()->getConnection()
          ->fetchAll($select);
        return $data;
    }
    public function getCategoryWiseArticlesId($cid)
    {

        $select = $this->_getResource()->getConnection()->select()
                   ->from($this->_getResource()->getTable('fme_articles_category'), 'articles_id')
                   ->where('category_id = ?', $cid);
        $data = $this->_getResource()->getConnection()
                ->fetchAll($select);
                
                
                return $data;
    }
    public function getCategoryUrlWiseArticles($cid)
    {

        $select = $this->_getResource()->getConnection()->select()
                   ->from($this->_getResource()->getTable('fme_articles_category'), 'articles_id')
                   ->where('category_url_key = ?', $cid);
        $data = $this->_getResource()->getConnection()
                ->fetchAll($select);
                
                
                return $data;
    }
}
