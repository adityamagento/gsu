<?php

namespace FME\Articles\Model\Media;

interface ConfigInterface
{

    public function getBaseMediaUrl();
    public function getBaseMediaPath();
    public function getMediaUrl($file);
    public function getMediaPath($file);
}
