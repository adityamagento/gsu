<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Model\ResourceModel\Article;

use Magento\Cms\Api\Data\PageInterface;
use \FME\Articles\Model\ResourceModel\AbstractCollection;
use Magento\Framework\Model\AbstractModel;

/**
 * CMS page collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'articles_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('FME\Articles\Model\Article', 'FME\Articles\Model\ResourceModel\Article');
        $this->_map['fields']['articles_id'] = 'main_table.articles_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }

    /**
     * Add filter by store
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
    protected function _afterLoad()
    {
        $this->mapAssociatedEntities('fme_articles_category', 'category_id');
       // $articleId = $object->getId();
        // print_r('afterload running');exit;
        // $connection = $this->getConnection();
        //      $select = $connection->select()->from(['fme_category_store' => 'fme_articles_category'])
        //          ->where('fme_category_store.' . 'articles_id' . ' IN (?)', $articleId);
        //      $result = $connection->fetchAll($select);
        return parent::_afterLoad();
    }
    protected function mapAssociatedEntities($entityType, $objectField)
    {
        if (!$this->_items) {
            return;
        }
        
        $columnKey = '1';
        $columnValue = 'xxxx';
        $columnValue2 = 'yyyyyyyy';
        $itemData = ['id' => 1, $columnKey => $columnValue];
        $itemData2 = ['id' => 2, $columnKey => $columnValue2];
        $item = $this->getItemByColumnValue($entityType, $objectField);
// $testItem = new \Magento\Framework\DataObject($itemData);
//     $testItem2 = new \Magento\Framework\DataObject($itemData2);
// $testItem->setData($columnKey , $columnValue);
    }

    /**
     * Perform operations after collection load
     *
     * @return $this
     */
    // protected function _afterLoad()
    // {
        
    //     $this->performAfterLoad('fme_articles_tags','' );
    //     $this->_previewFlag = false;

    //     return parent::_afterLoad();
    // }

    /**
     * Perform operations before rendering filters
     *
     * @return void
     */
    // protected function _renderFiltersBefore()
    // {
    //     $this->joinStoreRelationTable('fme_article_store', 'articles_id');
    // }
}
