<?php

namespace FME\Articles\Model\ResourceModel\Category;

use Magento\Cms\Api\Data\PageInterface;
use \FME\Articles\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'category_id';
    protected $_previewFlag;

    protected function _construct()
    {
        $this->_init('FME\Articles\Model\Category', 'FME\Articles\Model\ResourceModel\Category');
        $this->_map['fields']['category_id'] = 'main_table.category_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }

    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }

    protected function _afterLoad()
    {
        $this->performAfterLoad('fme_category_store', 'category_id');
        $this->_previewFlag = false;
        
        return parent::_afterLoad();
    }

    protected function _renderFiltersBefore()
    {
        $this->joinStoreRelationTable('fme_category_store', 'category_id');
    }
}
