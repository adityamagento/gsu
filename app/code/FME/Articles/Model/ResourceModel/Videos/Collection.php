<?php
namespace FME\Articles\Model\ResourceModel\Videos;

use Magento\Cms\Api\Data\PageInterface;
use \FME\Articles\Model\ResourceModel\AbstractCollection;
use Magento\Framework\Model\AbstractModel;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'video_id';
    protected $_previewFlag;

    protected function _construct()
    {
        $this->_init('FME\Articles\Model\Videos\Productvideos', 'FME\Articles\Model\ResourceModel\Videos');
        $this->_map['fields']['video_id'] = 'main_table.video_id';
        // $this->_map['fields']['store'] = 'store_table.store_id';
    }
        
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }

    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
