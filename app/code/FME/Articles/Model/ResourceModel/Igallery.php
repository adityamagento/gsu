<?php

namespace FME\Articles\Model\ResourceModel;

use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\EntityManager\EntityManager;
use FME\Events\Api\Data\PageInterface;

class Igallery extends AbstractDb
{

    protected $_store = null;
    protected $_tagCollectionFactory;
    protected $_tagGmapTable;
    protected $_storeManager;
    protected $entityManager;
    protected $metadataPool;
    
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_storeManager = $storeManager;
        $this->entityManager = $entityManager;
        $this->metadataPool = $metadataPool;
    }
    
    protected function _construct()
    {
        $this->_init('fme_media', 'imedia_id');
    }

    // protected function _afterSave(AbstractModel $product)
    // {
    //     $this->_saveEventMedia($product);
    //     return parent::_afterSave($product);
    // }
    protected function _saveEventMedia(\Magento\Framework\Model\AbstractModel $object)
    {
      
        $mediaIds = $object->getProduct();
        $mediaIds = $mediaIds['gallery']['images'];
        
        if (isset($mediaIds)) {
            // $condition = $this->getConnection()->quoteInto('event_id = ?', $object->getId());
            // $this->getConnection()->delete($this->getTable('fme_events_media'), $condition);
            foreach ($mediaIds as $media) {
                $gMediaArray = [];
                //$gMediaArray['imedia_id']= $object->getId();
                $gMediaArray['label'] = $media['label'];
                $gMediaArray['file'] = rtrim($media['file'], ".tmp");
                $gMediaArray['position'] = $media['position'];
                if ($media['removed'] != '1') {
                    $this->getConnection()
                    ->insert($this->getTable('fme_media'), $gMediaArray);
                }
            }
        }
    }

    public function setStore($store)
    {
        $this->_store = $store;
        return $this;
    }

    public function getStore()
    {
        return $this->_storeManager->getStore($this->_store);
    }
}
