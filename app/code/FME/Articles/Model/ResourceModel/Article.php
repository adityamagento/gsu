<?php

namespace FME\Articles\Model\ResourceModel;

use Magento\Framework\DB\Select;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\EntityManager\EntityManager;

class Article extends AbstractDb
{
    
    protected $_store = null;
    protected $_storeManager;
    protected $entityManager;
    protected $metadataPool;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        EntityManager $entityManager,
        MetadataPool $metadataPool,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_storeManager = $storeManager;
        $this->entityManager = $entityManager;
        $this->metadataPool = $metadataPool;
    }

    protected function _construct()
    {
        $this->_init('fme_articles', 'articles_id');
    }

    protected function _afterSave(AbstractModel $product)
    {
        $this->_saveArticleProducts($product);
        $this->_saveArticleMedia($product);
        //$this->_saveArticleTags($product);
        $this->_saveArticleArticles($product);
        $this->_saveArticleCategories($product);
        $this->_saveArticleVideos($product);
        return parent::_afterSave($product);
    }
    
    

    
    protected function _saveArticleMedia(\Magento\Framework\Model\AbstractModel $object)
    {
            $relatedPids = $object->getData('imedia_id');

           
        if (isset($relatedPids)) {
            $condition = $this->getConnection()->quoteInto(
                'articles_id = ?',
                $object->getId()
            );
            $this->getConnection()->delete(

                $this->getTable('fme_articles_media'),
                $condition
            );

            foreach ($relatedPids as $rPids) {
                $gProdArray = [];

                $gProdArray['articles_id']  = $object->getId();
               // print_r($rPids);exit;
                if ($rPids== 0) {
                    continue;
                }
                $gProdArray['imedia_id'] = $rPids;
                $this->getConnection()->insert($this->getTable('fme_articles_media'), $gProdArray);
            }
        }
    }
    // save articles associated with articles
    protected function _saveArticleArticles(\Magento\Framework\Model\AbstractModel $object)
    {


        $relatedPids = $object->getData('related_articles_id');
        // print_r($object->getId());exit;
        if (isset($relatedPids)) {
            $condition = $this->getConnection()->quoteInto(
                'articles_id = ?',
                $object->getId()
            );
            $this->getConnection()->delete(

                $this->getTable('fme_article_articles'),
                $condition
            );

            foreach ($relatedPids as $rPids) {
                $gProdArray = [];
                $gProdArray['articles_id']  = $object->getId();
               // print_r($rPids);exit;
                if ($rPids== 0) {
                    continue;
                }
                $gProdArray['related_articles_id'] = $rPids;
                $this->getConnection()->insert($this->getTable('fme_article_articles'), $gProdArray);
            }
        }
    }
    //save products associated with articles
    protected function _saveArticleProducts(\Magento\Framework\Model\AbstractModel $object)
    {
            $relatedPids = $object->getData('entity_id');
           

           
        if (isset($relatedPids)) {
            $condition = $this->getConnection()->quoteInto(
                'articles_id = ?',
                $object->getId()
            );
            $this->getConnection()->delete(

                $this->getTable('fme_article_products'),
                $condition
            );

            foreach ($relatedPids as $rPids) {
                $gProdArray = [];

                $gProdArray['articles_id']  = $object->getId();
               // print_r($rPids);exit;
                if ($rPids== 0) {
                    continue;
                }

                $gProdArray['entity_id'] = $rPids;
                $this->getConnection()->insert($this->getTable('fme_article_products'), $gProdArray);
            }
        }
    }
    //save products associated with categories
    protected function _saveArticleCategories(\Magento\Framework\Model\AbstractModel $object)
    {
            $relatedPids = $object->getData('category_id');

        if (isset($relatedPids)) {
            $condition = $this->getConnection()->quoteInto(
                'articles_id = ?',
                $object->getId()
            );
            $this->getConnection()->delete(

                $this->getTable('fme_articles_category'),
                $condition
            );

            foreach ($relatedPids as $rPids) {
                $gProdArray = [];

                $gProdArray['articles_id']  = $object->getId();
                
                if ($rPids== 0) {
                    continue;
                }

                $gProdArray['category_id'] = $rPids;
                $this->getConnection()->insert($this->getTable('fme_articles_category'), $gProdArray);
            }
        }
    }


    //save tags associated with articles
    protected function _saveArticleTags(\Magento\Framework\Model\AbstractModel $object)
    {
            $tagsValues = $object->getData('tag_names');
            $articlesIds = $object->getData('articles_id');
        if (isset($tagsValues)) {
            $condition = $this->getConnection()->quoteInto(
                'articles_id = ?',
                $object->getId()
            );

            $this->getConnection()->delete(
                $this->getTable('fme_articles_tags'),
                $condition
            );

            foreach ($tagsValues as $rPids) {
                $gProdArray = [];

                $gProdArray['articles_id']  = $object->getId();
                //print_r($rPids);exit;
                 
                $gProdArray['tag_names'] = $rPids;
                
                $this->getConnection()->insert($this->getTable('fme_articles_tags'), $gProdArray);
            }
        }
    }

    //save tags associated with articles
    protected function _saveArticleVideos(\Magento\Framework\Model\AbstractModel $object)
    {
            $relatedPids = $object->getData('video_id');

           
        if (isset($relatedPids)) {
            $condition = $this->getConnection()->quoteInto(
                'articles_id = ?',
                $object->getId()
            );
            $this->getConnection()->delete(

                $this->getTable('fme_articles_related_videos'),
                $condition
            );

            foreach ($relatedPids as $rPids) {
                $gProdArray = [];

                $gProdArray['articles_id']  = $object->getId();
               // print_r($rPids);exit;
                if ($rPids== 0) {
                    continue;
                }
                $gProdArray['video_id'] = $rPids;
                $this->getConnection()->insert($this->getTable('fme_articles_related_videos'), $gProdArray);
            }
        }
    }
}
