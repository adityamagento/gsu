<?php
namespace FME\Articles\Model\ResourceModel\Comments;

use Magento\Cms\Api\Data\PageInterface;
use \FME\Articles\Model\ResourceModel\AbstractCollection;
use Magento\Framework\Model\AbstractModel;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'comments_id';
    protected $_previewFlag;

    protected function _construct()
    {
        $this->_init('FME\Articles\Model\Comments', 'FME\Articles\Model\ResourceModel\Comments');
        $this->_map['fields']['comments_id'] = 'main_table.comments_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }
        
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }

    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
