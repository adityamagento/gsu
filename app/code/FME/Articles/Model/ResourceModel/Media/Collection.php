<?php

namespace FME\Articles\Model\ResourceModel\Media;

use \FME\Articles\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'imedia_id';
    protected $_previewFlag;
    
    protected function _construct()
    {
        $this->_init(
            
            'FME\Articles\Model\Media',
            'FME\Articles\Model\ResourceModel\Media'
        );
        $this->_map['fields']['imedia_id'] = 'main_table.imedia_id';
    }
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
