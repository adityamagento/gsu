<?php

namespace FME\Articles\Model;

class Media extends \Magento\Framework\Model\AbstractModel
{

        
    protected function _construct()
    {
        $this->_init('FME\Articles\Model\ResourceModel\Media');
    }
}
