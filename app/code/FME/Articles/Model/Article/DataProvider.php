<?php

namespace FME\Articles\Model\Article;

use FME\Articles\Model\ResourceModel\Article\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $collection;
    protected $_selectCat;
    protected $dataPersistor;
    public $_storeManager;
    protected $loadedData;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \FME\Articles\Model\Article $selectCat,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->_storeManager=$storeManager;
        $this->_selectCat = $selectCat;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        $baseurl =  $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if (isset($this->loadedData)) {

            return $this->loadedData;
        }

        $items = $this->collection->getItems();
 
        if ($items) {
            foreach ($items as $page) {
                $temp = $page->getData();
                $img = [];
                $img[0]['name'] = $temp['image'];
                $img[0]['url'] = $baseurl.$temp['image'];
                $temp['image'] = $img;
                $temp['related_categories'] = $this->_selectCat->getRelatedCategories($page->getId());
                $this->loadedData[$page->getId()] = $page->getData();
            }

            $data = $this->dataPersistor->get('fme_articles');
            if (!empty($data)) {
                $page = $this->collection->getNewEmptyItem();
                $page->setData($data);
                $this->loadedData[$page->getId()] = $page->getData();
            
                $this->dataPersistor->clear('fme_articles');
            } else {
                if ($page->getData('image') != null || $temp['related_categories'] != null) {
                    $t2[$page->getId()] = $temp;
                 
                    return $t2;
                } else {
                    // echo '<pre>';
        
                    return $this->loadedData;
                }
            }
        }
    }
}
