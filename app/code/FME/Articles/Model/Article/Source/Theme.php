<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Model\Article\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */

class Theme implements OptionSourceInterface
{
    protected $_theme;
  
    public function __construct(\FME\Articles\Model\Article $theme)
    {
        $this->_theme = $theme;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->_theme->getAvailableThemes();
        
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
