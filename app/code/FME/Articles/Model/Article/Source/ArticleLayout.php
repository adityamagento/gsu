<?php

namespace FME\Articles\Model\Article\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\View\Model\PageLayout\Config\BuilderInterface;

class ArticleLayout implements OptionSourceInterface
{
    
    protected $pageLayoutBuilder;
    protected $options;

    public function __construct(BuilderInterface $pageLayoutBuilder)
    {
        $this->pageLayoutBuilder = $pageLayoutBuilder;
    }
        
    public function toOptionArray()
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $configOptions = $this->pageLayoutBuilder->getPageLayoutsConfig()->getOptions();
        $options = [];
        foreach ($configOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        $this->options = $options;

        return $this->options;
    }
}
