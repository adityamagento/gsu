<?php

namespace FME\Articles\Model\Article\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\View\Design\Theme\Label\ListInterface;

class ThemeArticles implements OptionSourceInterface
{
    
    protected $themeList;

    public function __construct(ListInterface $themeList)
    {
        $this->themeList = $themeList;
    }
        
    public function toOptionArray()
    {
        $options[] = ['label' => 'Default', 'value' => ''];
        return array_merge($options, $this->themeList->getLabels());
    }
}
