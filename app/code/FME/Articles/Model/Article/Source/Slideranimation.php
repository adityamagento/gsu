<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\  FME Categorybanner Module  \\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   FME                            ///////
 \\\\\\\                      * @package    FME_Categorybanner              \\\\\\\
 ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
 /////////////////////////////////////////////////////////////////////////////////
 */

namespace FME\Articles\Model\Article\Source;

class Slideranimation extends \Magento\Framework\ObjectManager\ObjectManager
{
    /**
     * __construct
     * @param \Magento\Framework\ObjectManagerInterface         $objectManager
     * @param \Magento\Framework\ObjectManager\FactoryInterface $factory
     * @param \Magento\Framework\ObjectManager\ConfigInterface  $config
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\ObjectManager\FactoryInterface $factory,
        \Magento\Framework\ObjectManager\ConfigInterface $config
    ) {
        
            parent::__construct($factory, $config);
            $this->_objectManager = $objectManager;
    }
    /**
     * toOptionArray
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Random'),
                'value' => 'random'
            ],
            [
                'label' => __('SimpleFade'),
                'value' => 'simpleFade'
            ],
            [
                'label' => __('SimpleFade'),
                'value' => 'simpleFade'
            ],
            [
                'label' => __('curtainTopLeft'),
                'value' => 'curtainTopLeft'
            ],
            [
                'label' => __('curtainTopRight'),
                'value' => 'curtainTopRight'
            ],
            [
                'label' => __('curtainBottomLeft'),
                'value' => 'curtainBottomLeft'
            ],
            [
                'label' => __('curtainBottomRight'),
                'value' => 'curtainBottomRight'
            ],
            [
                'label' => __('curtainSliceLeft'),
                'value' => 'curtainSliceLeft'
            ],
            [
                'label' => __('curtainSliceRight'),
                'value' => 'curtainSliceRight'
            ],
            [
                'label' => __('blindCurtainTopLeft'),
                'value' => 'blindCurtainTopLeft'
            ],
            [
                'label' => __('blindCurtainTopRight'),
                'value' => 'blindCurtainTopRight'
            ],
            [
                'label' => __('blindCurtainBottomLeft'),
                'value' => 'blindCurtainBottomLeft'
            ],
            [
                'label' => __('blindCurtainBottomRight'),
                'value' => 'blindCurtainBottomRight'
            ],
            [
                'label' => __('blindCurtainSliceBottom'),
                'value' => 'blindCurtainSliceBottom'
            ],
            [
                'label' => __('blindCurtainSliceTop'),
                'value' => 'blindCurtainSliceTop'
            ],
            [
                'label' => __('stampede'),
                'value' => 'stampede'
            ],
            [
                'label' => __('mosaic'),
                'value' => 'mosaic'
            ],
            [
                'label' => __('scrollLeft'),
                'value' => 'scrollLeft'
            ],
            [
                'label' => __('scrollRight'),
                'value' => 'scrollRight'
            ],
            [
                'label' => __('scrollHorz'),
                'value' => 'scrollHorz'
            ],
            [
                'label' => __('scrollBottom'),
                'value' => 'scrollBottom'
            ],
            [
                'label' => __('scrollTop'),
                'value' => 'scrollTop'
            ]
        ];
    }
}
