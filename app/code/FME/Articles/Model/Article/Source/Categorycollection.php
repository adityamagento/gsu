<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Model\Article\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */

class Categorycollection implements OptionSourceInterface
{
    protected $googleStore;
    protected $categoryFactory;
  
    public function __construct(
        \FME\Articles\Model\Article $googleStore,
        \FME\Articles\Model\CategoryFactory $categoryFactory
    ) {
    
        $this->googleStore = $googleStore;
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        
        $collection = $this->categoryFactory->create()->getCollection();
       // print_r($collection->getData());exit;
        $options = [];
        foreach ($collection as $key) {
            $options[] = [
                'label' => $key['category_name'],
                'value' => $key['category_id'],
            ];
        }
        return $options;
    }
}
