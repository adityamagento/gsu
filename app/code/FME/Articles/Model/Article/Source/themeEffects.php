<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Model\Article\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */

class themeEffects implements OptionSourceInterface
{
    protected $_themeEffects;
  
    public function __construct(\FME\Articles\Model\Article $themeEffects)
    {
        $this->_themeEffects = $themeEffects;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->_themeEffects->getAvailableThemesEffects();
        
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
