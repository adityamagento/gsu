<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Articles\Model\Category\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 */

class IsParentCat implements OptionSourceInterface
{
    protected $parentCat;
  
    public function __construct(\FME\Articles\Model\Category $parentCat)
    {
        $this->parentCat = $parentCat;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->parentCat->getParentCat();
	$options=array();
        foreach ($availableOptions as $value) {
            $options[] = [
                'label' => $value['category_name'],
                'value' => $value['id'],
            ];
        }
        return $options;
    }
}
