<?php
/**
* FME Extensions
*
* NOTICE OF LICENSE
*
* This source file is subject to the fmeextensions.com license that is
* available through the world-wide-web at this URL:
* https://www.fmeextensions.com/LICENSE.txt
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this extension to newer
* version in the future.
*
* @category FME
* @package FME_Articles
* @copyright Copyright (c) 2019 FME (http://fmeextensions.com/)
* @license https://fmeextensions.com/LICENSE.txt
*/
namespace FME\Articles\Model\Videos;

class Productvideos extends \Magento\Framework\Model\AbstractModel
{
        
    protected $_objectManager;

    protected $_coreResource;

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * @param \Magento\Framework\Model\Context                               $context            [description]
     * @param \Magento\Framework\Registry                                    $registry           [description]
     * @param \Magento\Framework\ObjectManagerInterface                      $objectManager      [description]
     * @param \Magento\Framework\App\Resource                                $coreResource       [description]
     * @param \FME\Productvideos\Model\Resource\Productvideos            $resource           [description]
     * @param \FME\Productvideos\Model\Resource\Productvideos\Collection $resourceCollection [description]
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\ResourceConnection $coreResource,
        \FME\Articles\Model\ResourceModel\Videos $resource,
        \FME\Articles\Model\ResourceModel\Videos\Collection $resourceCollection
    ) {
        $this->_objectManager = $objectManager;
        $this->_coreResource = $coreResource;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
    }

    /**
     * _construct
     *
     */
    public function _construct()
    {
        $this->_init('FME\Articles\Model\ResourceModel\Videos');
    }


    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * getRelatedProducts
     * @param  $productvideosId
     * @return array
     */
    public function getRelatedProducts($productvideosId)
    {
                    
        $productvideosTable = $this->_coreResource
                                    ->getTableName('productvideos_products');
            
        $collection = $this->_objectManager->create('FME\Articles\Model\Videos\Productvideos')
                        ->getCollection()
                        ->addFieldToFilter('main_table.video_id', $productvideosId);
                      
                      
        $collection->getSelect()
            ->joinLeft(
                ['related' => $productvideosTable],
                'main_table.video_id = related.productvideos_id'
            )
            ->order('main_table.video_id');
                    return $collection->getData();
    }


    public function getProductRelatedVideos($productId)
    {
        
        $productvideosTable = $this->_coreResource
                                    ->getTableName('fme_articles_productvideos_products');
            
        $collection = $this->_objectManager->create('FME\Articles\Model\Videos\Productvideos')
                        ->getCollection();
                              
        $collection->getSelect()
            ->joinLeft(
                ['related' => $productvideosTable],
                'main_table.video_id = related.productvideos_id'
            )
            ->where('related.product_id = '.$productId .' and main_table.status = 1')
            ->order('main_table.video_id');
        
           
        return $collection;
    }

    public function getProducts(\FME\Productvideos\Model\Productvideos $object)
    {
              $select = $this->_getResource()->getConnection()->select()->from($this->_getResource()->getTable('fme_articles_productvideos_products'))->where('productvideos_id = ?', $object->getId());
        $data         = $this->_getResource()->getConnection()->fetchAll($select);
        if ($data) {
            $productsArr = [];
            foreach ($data as $_i) {
                $productsArr[] = $_i['product_id'];
            }

            
            return $productsArr;
        }
    }

    public function getProductsPosition()
    {
        if (!$this->getId()) {
            return [];
        }

        $array = $this->getData('products_position');
        if ($array === null) {
            $temp = $this->getData('product_id');

            for ($i = 0; $i < sizeof($this->getData('product_id')); $i++) {
                $array[$temp[$i]] = 0;
            }

            $this->setData('products_position', $array);
        }

        return $array;
    }//end getProductsPosition()
}
