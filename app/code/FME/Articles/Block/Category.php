<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

class Category extends Template
{
    public $articlesHelper;
    public $sliderHelper;
    public $socialHelper;
    protected $_requests;
    protected $_categoryWiseArticles;
    protected $scopeConfig;
    protected $collectionFactory;
    protected $mediaFactory;
    protected $objectManager;
    protected $request;
    protected $categoryCollection;
    protected $tagsCollection;
    protected $date;
    protected $timezone;
    protected $_defaultToolbarBlock = 'FME\Articles\Block\EventToolbar';
        
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FME\Articles\Model\ResourceModel\Article\
        CollectionFactory $collectionFactory,
        \FME\Articles\Model\ResourceModel\Media\
        CollectionFactory $mediaFactory,
        \FME\Articles\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \FME\Articles\Model\Article $tagsCollection,
        \FME\Articles\Helper\Article $helper,
        \FME\Articles\Helper\ArticleSocial $socialHelper,
        \FME\Articles\Model\Category $categoryWiseArticles,
        \FME\Articles\Helper\Slider $helperSlider,
        ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\App\RequestInterface $requests,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->mediaFactory = $mediaFactory;
        $this->objectManager = $objectManager;
        $this->articlesHelper = $helper;
        $this->sliderHelper = $helperSlider;
        $this->socialHelper = $socialHelper;
        $this->categoryCollection = $categoryCollection;
        $this->_categoryWiseArticles = $categoryWiseArticles;
        $this->request = $request;
        $this->_requests = $requests;
        $this->date = $date;
        $this->timezone = $timezone;
        $this->tagsCollection = $tagsCollection;
        parent::__construct($context);
    }
        
    public function _prepareLayout()
    {
        $identifier = $this->_requests->getPathInfo();
        $parts = explode('/', $identifier);
        
        //$parts = explode('.', $parts[3]);
        $parts = explode('.', $parts[2]);
        $parts = $parts[0];
        $cpid = $this->getCategoriesPageId($parts);
        $cpid = $cpid->getData();
       
        if ($this->articlesHelper->isArticleModuleEnable()) {
            $this->pageConfig->setKeywords($cpid[0]['meta_keywords']);
            $this->pageConfig->setDescription($cpid[0]['meta_description']);
            $this->pageConfig->getTitle()->set($cpid[0]['category_name']);
            if ($this->getAritlcesCollection()) {
                $pager = $this->getLayout()->createBlock(
                    'Magento\Theme\Block\Html\Pager',
                    'fme.articles.pager'
                )->setAvailableLimit([5=>5,10=>10,15=>15])->setShowPerPage(true)->setCollection(
                    $this->getAritlcesCollection()
                );
                $this->setChild('pager', $pager);
                $this->getAritlcesCollection()->load();
            }
            return parent::_prepareLayout();
        }
    }

    public function getPagerHtml()
    {
	     $html = $this->getChildHtml('pager');
	     $html = str_replace('/gemstonearticles/cat/','/cat/',$html);
         return $html;
    }

    public function getAritlcesCollection()
    {
        
        $param = $this->getRequest()->getParam('id');
        $identifier = $this->_requests->getPathInfo();
        $parts = explode('/', $identifier);
        
        //$parts = explode('.', $parts[3]);
        $parts = explode('.', $parts[2]);
        $parts = $parts[0];

        $tagParam = $this->getRequest()->getParam('tag');
        $postSearchValues = (array)$this->request->getPost();

        if (($parts !== null)) {
            $cpid = $this->getCategoriesPageId($parts);
            $cpid = $cpid->getData();
            $cpid = $cpid[0]['category_id'];
        
            $articleIds=$this->_categoryWiseArticles->getCategoryWiseArticlesId($cpid);
            foreach ($articleIds as $key => $values) {
                $valueIds[] = $values['articles_id'];
            }


             $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1)->
           
            addFieldToFilter('articles_id', $valueIds);
            $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest(
                
        )->getParam('limit') : 5;
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
            $collection->setOrder('articles_id','DESC')->load();
            return $collection;
        } elseif ($tagParam !== null) {
            $articleTagIds = $this->tagsCollection->getArticlesByTags($tagParam);
          
            $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('articles_id', $articleTagIds);
            return $collection;
        } elseif (!empty($postSearchValues['article_search'])) {
            $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1)
            ->addFieldToFilter(
                'title',
                [
                ['like' => '% '.$postSearchValues['article_search'].' %'],
                ['like' => '% '.$postSearchValues['article_search']],
                ['like' => $postSearchValues['article_search'].' %']
                ]
            );
            return $collection;
        } else {
            $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1);
            return $collection;
        }
    }

    public function getFrontArticles()
    {
        
          
        $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1);
        
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest(
                
        )->getParam('limit') : 5;
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        return $collection;
    }

    protected function _tohtml()
    {

        $this->blockidentifier = $this->getBlockId();
       
        if ($this->articlesHelper->getSlidertype() == '1') { 
            $this->setTemplate("FME_Articles::article-list.phtml");
        } elseif ($this->articlesHelper->getSlidertype() == '2') { 
            $this->setTemplate("FME_Articles::article-grid.phtml");
        }

        return parent::_toHtml();
    }

    public function getCategoriesCollection()
    {
        $collection = $this->categoryCollection->create()->addFieldToFilter('is_active', 1)->setPageSize(300);
        return $collection;
    }

    public function getCategoriesPageId($urlkey)
    {
        $collection = $this->categoryCollection->create()
        ->addFieldToFilter('is_active', 1)
        ->addFieldToFilter('category_url_key', $urlkey);
        return $collection;
    }

    public function getFeaturedArticles()
    {
        $collection = $this->collectionFactory->create()
        ->addFieldToFilter('is_active', 1)
        ->addFieldToFilter('featured', 1)
        ->setPageSize(25);
        return $collection;
    }

    public function getRecentPosts()
    {
        $collection = $this->collectionFactory->create()
        ->addFieldToFilter('is_active', 1)
        ->addFieldToFilter('featured', 0)
        ->setOrder('article_publish_date', 'ASC')
        ->setPageSize(25);
        return $collection;
    }

    public function getTagsCollections()
    {
                
        return $this->tagsCollection->getTagsDistinctCollection();
    }
    
    public function getCurrDateTime()
    {
      $datewithoffset = $this->timezone->date();
      $datewithoutoffset = $this->date->gmtDate();     
     return $datewithoffset;
     
    }
} 
