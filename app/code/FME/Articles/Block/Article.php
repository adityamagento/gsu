<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;
  
class Article extends Template
{
    public $articlesHelper;
    public $sliderHelper;
    public $socialHelper;
    protected $_categoryWiseArticles;
    protected $scopeConfig;
    protected $collectionFactory;
    protected $mediaFactory;
    protected $objectManager;
    protected $request;
    protected $categoryCollection;
    protected $tagsCollection;
    protected $date;
    protected $timezone;
    
        
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FME\Articles\Model\ResourceModel\Article\
        CollectionFactory $collectionFactory,
        \FME\Articles\Model\ResourceModel\Media\
        CollectionFactory $mediaFactory,
        \FME\Articles\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \FME\Articles\Model\Article $tagsCollection,
        \FME\Articles\Helper\Article $helper,
        \FME\Articles\Helper\ArticleSocial $socialHelper,
        \FME\Articles\Model\Category $categoryWiseArticles,
        \FME\Articles\Helper\Slider $helperSlider,
        ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->mediaFactory = $mediaFactory;
        $this->objectManager = $objectManager;
        $this->articlesHelper = $helper;
        $this->sliderHelper = $helperSlider;
        $this->socialHelper = $socialHelper;
        $this->categoryCollection = $categoryCollection;
        $this->_categoryWiseArticles = $categoryWiseArticles;
        $this->request = $request;
        $this->tagsCollection = $tagsCollection;
        $this->date = $date;
        $this->timezone = $timezone;
                
        parent::__construct($context);
    }
        
    public function _prepareLayout()
    {
        if ($this->articlesHelper->isArticleModuleEnable()) {
            $this->pageConfig->setKeywords($this->articlesHelper->getArticlePageMetakeywordSeo());
            $this->pageConfig->setDescription($this->articlesHelper->getArticlePageMetadescriptionSeo());
            $this->pageConfig->getTitle()->set($this->articlesHelper->getArticlePageTitleSeo());
            if ($this->getAritlcesCollection()) {
                $pager = $this->getLayout()->createBlock(
                    'Magento\Theme\Block\Html\Pager',
                    'fme.articles.pager'
                )->setAvailableLimit([5=>5,10=>10,15=>15])->setShowPerPage(true)->setCollection(
                    $this->getAritlcesCollection()
                );
                $this->setChild('pager', $pager);
                $this->getAritlcesCollection()->load();
            }
            return parent::_prepareLayout();
        }
    }

    
    public function getPagerHtml()
    {
         return $this->getChildHtml('pager');
    }

    public function getAritlcesCollection()
    {
       
        $param = $this->getRequest()->getParam('id');
        $tagParam = $this->getRequest()->getParam('tag');
        $postSearchValues = (array)$this->request->getPost();

        if (($param !== null)) {

            $articleIds=$this->_categoryWiseArticles->getCategoryWiseArticlesId($this->getRequest()->getParam('id'));
            foreach ($articleIds as $key => $values) {
                $valueIds[] = $values['articles_id'];
            }
             $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1)->
            addFieldToFilter('articles_id', $valueIds)
            ->setOrder('articles_id','DESC')->load();

            
            return $collection;
        } elseif ($tagParam !== null) {

            $articleTagIds = $this->tagsCollection->getArticlesByTags($tagParam);
          
            $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1)
            ->setOrder('articles_id','DESC')->load()
            ->addFieldToFilter('articles_id', $articleTagIds);
            return $collection;
        } elseif (!empty($postSearchValues['article_search'])) {
            $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1)
            ->addFieldToFilter(
                'title',
                [
                ['like' => '% '.$postSearchValues['article_search'].' %'],
                ['like' => '% '.$postSearchValues['article_search']],
                ['like' => $postSearchValues['article_search'].' %'],
                ['like' => $postSearchValues['article_search']]
                ]
            );
            return $collection;
        } else {
            
            $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1);
            $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
            $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest(
                
            )->getParam('limit') : 5;
            $collection->setPageSize($pageSize);
            $collection->setCurPage($page);
            $collection->setOrder('articles_id','DESC')->load();
           /* $collection->getSelect()->assemble();
$collection->getSelect()->__toString();
echo $collection->getSelect(); die();*/
            return $collection;
        }
    }

    protected function _tohtml()
    {

        $this->blockidentifier = $this->getBlockId();
        
        if ($this->articlesHelper->getSlidertype() == '1') {
            $this->setTemplate("FME_Articles::article-list.phtml");
        } elseif ($this->articlesHelper->getSlidertype() == '2') {
            $this->setTemplate("FME_Articles::article-grid.phtml");
        }

        return parent::_toHtml();
    }

    public function getCategoriesCollection()
    {
        $collection = $this->categoryCollection->create()->addFieldToFilter('is_active', 1)->setPageSize($this->articlesHelper->getArticleCategoryNumBlock());
        return $collection;
    }

    public function getFeaturedArticles()
    {
        $collection = $this->collectionFactory->create()
        ->addFieldToFilter('is_active', 1)
        ->addFieldToFilter('featured', 1)
        ->setPageSize($this->articlesHelper->getArticlePostNumPost() ? $this->articlesHelper->getArticlePostNumPost() : 5);
        return $collection;
    }

    public function getRecentPosts()
    {
        $collection = $this->collectionFactory->create()
        ->addFieldToFilter('is_active', 1)
        ->addFieldToFilter('featured', 0)
        ->setOrder('article_publish_date', 'ASC')
        ->setPageSize(3);
        return $collection;
    }

    public function getTagsCollections()
    {
                
        return $this->tagsCollection->getTagsDistinctCollection();
    }
    
    public function getCurrDateTime()
    {
      $datewithoffset = $this->timezone->date();
      $datewithoutoffset = $this->date->gmtDate();     
     return $datewithoffset;
     
    }
}
