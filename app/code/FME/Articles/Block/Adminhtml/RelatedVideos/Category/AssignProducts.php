<?php

namespace FME\Articles\Block\Adminhtml\RelatedVideos\Category;

class AssignProducts extends \Magento\Backend\Block\Template
{

    protected $_template = 'event/products/edit/assign_relatedvideos.phtml';
    protected $blockGrid;
    protected $registry;
    protected $jsonEncoder;
    

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->jsonEncoder = $jsonEncoder;
        
        parent::__construct($context, $data);
    }

    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                'FME\Articles\Block\Adminhtml\RelatedVideos\Category\Tab\Product',
                'related.videos.grid'
            );
        }
        return $this->blockGrid;
    }

    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }
    
    public function getProductsJson()
    {
        $products = $this->getCategory()->getVideoPosition();
        
        if (!empty($products)) {
            return $this->jsonEncoder->encode($products);
        }
        return '{}';
    }

    public function getCategory()
    {
        return $this->registry->registry('articles_article');
    }
}
