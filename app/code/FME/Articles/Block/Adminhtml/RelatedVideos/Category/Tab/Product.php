<?php

namespace FME\Articles\Block\Adminhtml\RelatedVideos\Category\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;

class Product extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    protected $_coreRegistry = null;
    protected $_mediaFactory;
    protected $_articleFactory;
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \FME\Articles\Model\Videos\ProductvideosFactory $mediaFactory,
        \FME\Articles\Model\Article $articleFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_mediaFactory = $mediaFactory;
        $this->_articleFactory = $articleFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('in_category_videos');
        $this->setDefaultSort('video_id');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('articles_id')) {
            $this->setDefaultFilter(['in_articles_vid'=>1]);
        }
    }

    public function getCategory()
    {
        return $this->_coreRegistry->registry('articles_article');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_articles_vid') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('video_id', ['in' => $productIds]);
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('video_id', ['nin' => $productIds]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
               

               $collection = $this->_mediaFactory->create()->getCollection()
               // ->addAttributeToSelect(
               //     'file'
               // )->addAttributeToSelect(
               //     'label'
               // )->addAttributeToSelect(
               //     'imedia_id'
               // )
        ->addOrder('video_id', 'asc');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
                
            $this->addColumn(
                'in_articles_vid',
                [
                    'header_css_class'  => 'a-center',
                    'type' => 'checkbox',
                    'name' => 'in_articles_vid',
                    'values' => $this->_getSelectedProducts(),
                    'index' => 'video_id',
                    'header_css_class' => 'col-select col-massaction',
                     'column_css_class' => 'col-select col-massaction',
                     //'use_index' => true
                ]
            );
        $this->addColumn(
            'video_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'video_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                
            ]
        );
        $this->addColumn(
            'video_thumb',
            [
               'header' => __('Image'),
               'index'  => 'video_thumb',
               'renderer'  => '\FME\Articles\Block\Adminhtml\Article\Grid\Renderer\Imagevideo',
               // 'component'  => 'Magento_Ui/js/grid/columns/thumbnail'
            ]
        );
        // $this->addColumn('title', ['header' => __('Label'), 'index' => 'label']);
        
        $this->addColumn(
            'title',
            [
                'header' => __('Title'),
                'sortable' => true,
                'index' => 'title',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                
            ]
        );
        /*$this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'editable' => 'false'
            ]
        );*/
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('articles/*/gridvideos', ['_current' => true]);
    }

    protected function _getSelectedProducts()
    {
        $id = $this->getRequest()->getParam('articles_id');
        $relatedProducts   = $this->_articleFactory->getRelatedVideos($id);
        //print_r($relatedProducts);exit;

        if ($relatedProducts) {
            $relatedProducts = array_values($relatedProducts);
            return $relatedProducts;
        } else {
            return [];
        }
    }
}
