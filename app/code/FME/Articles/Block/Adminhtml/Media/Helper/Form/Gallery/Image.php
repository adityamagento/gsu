<?php

namespace FME\Articles\Block\Adminhtml\Media\Helper\Form\Gallery;

use Magento\Backend\Block\Media\Uploader;
use Magento\Framework\View\Element\AbstractBlock;

class Image extends \Magento\Backend\Block\Widget
{

    protected $_template = 'catalog/product/helper/gallery.phtml';
    protected $_mediaConfig;
    protected $_jsonEncoder;
    public $objectMgr;
    private $imageUploadConfigDataProvider;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \FME\Articles\Model\Media\Config $mediaConfig,
        \Magento\Framework\Registry $coreRegister,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {

        $this->_jsonEncoder = $jsonEncoder;
        $this->_mediaConfig = $mediaConfig;
        $this->_coreRegister = $coreRegister;
        $this->objectMgr = $objectManager;
        parent::__construct($context, $data);
    }
    
    protected function _prepareLayout()
    {
        
        $productMetadata = $this->objectMgr->create('\Magento\Framework\App\ProductMetadata');
        $version = $productMetadata->getVersion();
        
        if (version_compare($version, '2.3.1', '>=')){
            
            $this->imageUploadConfigDataProvider = $this->objectMgr::getInstance()->get(\Magento\Backend\Block\DataProviders\ImageUploadConfig::class);

            $this->addChild(
                'uploader',
                \Magento\Backend\Block\Media\Uploader::class,
                ['image_upload_config_data' => $this->imageUploadConfigDataProvider]
            );
        
        }elseif (version_compare($version, '2.2.8', '>=') && version_compare($version, '2.3.0', '<')){
            
            $this->imageUploadConfigDataProvider = $this->objectMgr::getInstance()->get(\Magento\Backend\Block\DataProviders\UploadConfig::class);

            $this->addChild(
                'uploader',
                \Magento\Backend\Block\Media\Uploader::class,
                ['image_upload_config_data' => $this->imageUploadConfigDataProvider]
            );
        
        }else{
            //this is for 2.3.0 and 2.2.7 or less
            $this->addChild('uploader', 'Magento\Backend\Block\Media\Uploader');        
        
        }
        
        
        $this->getUploader()
                ->getConfig()
                ->setUrl(
                    $this->_urlBuilder->addSessionParam()
                    ->getUrl('articles/media/upload')
                )->setFileField('image')
                ->setFilters([
                    'images' => [
                        'label' => __('Images (.gif, .jpg, .png)'),
                        'files' => ['*.gif', '*.jpg', '*.jpeg', '*.png'],
                    ],
                ]);
        
        $this->_eventManager->dispatch('photogallery_prepare_layout', ['block' => $this]);

        return parent::_prepareLayout();
    }

    public function images()
    {
        $images = $this->_coreRegister->registry('photogallery_img');
        $img_data = $images->getData();
        return $img_data;
    }
    
    public function getUploader()
    {
        return $this->getChildBlock('uploader');
    }
    
    public function getUploaderHtml()
    {
        return $this->getChildHtml('uploader');
    }
    
    public function getJsObjectName()
    {
        return $this->getHtmlId() . 'JsObject';
    }
    
    public function getAddImagesButton()
    {
        return $this->getButtonHtml(
            __('Add New Images'),
            $this->getJsObjectName() . '.showUploader()',
            'add',
            $this->getHtmlId() . '_add_images_button'
        );
    }
    
    public function getMediaAttributes()
    {
        return $this->getElement()->getDataObject()->getMediaAttributes();
    }
    
    public function getImagesJson()
    {
        $value['images'] = $this->images();
        if (is_array($value['images']) && count($value['images']) > 0) {
            foreach ($value['images'] as &$image) {
                $image['url'] = $this->_mediaConfig->getMediaUrl($image['file']);
                $image['file'] = $image['file'];
                $image['label'] = $image['label'];
                $image['imedia_id'] = $image['imedia_id'];
            }
            return $this->_jsonEncoder->encode($value['images']);
        }

        return '[]';
    }
    
    public function getImagesValuesJson()
    {
        $values = [];
        return $this->_jsonEncoder->encode($values);
    }
    
    public function getImageTypes()
    {
        $imageTypes = [];
        foreach ($this->images() as $attribute) {
            $imageTypes['image'] = [
                'code' => 'image',
                'value' => $attribute['file'],
                'label' => $attribute['label'],
                'scope' => 'Store View',
                'name' => 'gallery[image]',
            ];
        }
        return $imageTypes;
    }
    
    public function getImageTypesJson()
    {
        return $this->_jsonEncoder->encode($this->getImageTypes());
    }
}
