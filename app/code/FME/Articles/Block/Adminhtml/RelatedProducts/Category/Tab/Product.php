<?php

namespace FME\Articles\Block\Adminhtml\RelatedProducts\Category\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;

class Product extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    protected $_coreRegistry = null;
    protected $_mediaFactory;
    protected $_articleFactory;
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \FME\Articles\Model\IgalleryFactory $mediaFactory,
        \FME\Articles\Model\Article $articleFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_mediaFactory = $mediaFactory;
        $this->_articleFactory = $articleFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('in_category_images');
        $this->setDefaultSort('imedia_id');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('articles_id')) {
            $this->setDefaultFilter(['in_articles'=>1]);
        }
    }

    public function getCategory()
    {
        return $this->_coreRegistry->registry('articles_article');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_articles') {
            $productIds = $this->_getSelectedProducts();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('imedia_id', ['in' => $productIds]);
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('imedia_id', ['nin' => $productIds]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
               

               $collection = $this->_mediaFactory->create()->getCollection()
               // ->addAttributeToSelect(
               //     'file'
               // )->addAttributeToSelect(
               //     'label'
               // )->addAttributeToSelect(
               //     'imedia_id'
               // )
        ->addOrder('imedia_id', 'asc');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
                
            $this->addColumn(
                'in_articles',
                [
                    'header_css_class'  => 'a-center',
                    'type' => 'checkbox',
                    'name' => 'in_articles',
                    'values' => $this->_getSelectedProducts(),
                    'index' => 'imedia_id',
                    'header_css_class' => 'col-select col-massaction',
                     'column_css_class' => 'col-select col-massaction'
                ]
            );
        $this->addColumn(
            'file',
            [
               'header' => __('Image'),
               'index'  => 'file',
               'renderer'  => '\FME\Articles\Block\Adminhtml\Article\Grid\Renderer\Image',
               // 'component'  => 'Magento_Ui/js/grid/columns/thumbnail'
            ]
        );
        $this->addColumn('label', ['header' => __('Label'), 'index' => 'label']);
        $this->addColumn(
            'imedia_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'imedia_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                
            ]
        );
        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'editable' => 'false'
            ]
        );
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('articles/*/grid', ['_current' => true]);
    }

    protected function _getSelectedProducts()
    {
        $id = $this->getRequest()->getParam('articles_id');
        $relatedProducts   = $this->_articleFactory->getRelatedProducts($id);
        //print_r($relatedProducts);exit;

        if ($relatedProducts) {
            $relatedProducts = array_values($relatedProducts);
            return $relatedProducts;
        } else {
            return [];
        }
    }
}
