<?php
namespace FME\Articles\Block\Adminhtml\RelatedArticles\Category\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;

class Product extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    protected $_coreRegistry = null;
    protected $_productFactory;
    protected $_eventFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \FME\Articles\Model\ArticleFactory $productFactory,
        \FME\Articles\Model\Article $eventFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_eventFactory = $eventFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('catalog_category_articles');
        $this->setDefaultSort('related_articles_id');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('articles_id')) {
            $this->setDefaultFilter(['in_events'=>1]);
        }
    }

    public function getCategory()
    {
        return $this->_coreRegistry->registry('articles_article');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_events') {
            $productIds = $this->_getSelectedProducts();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('articles_id', ['in' => $productIds]);
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('articles_id', ['nin' => $productIds]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
        $collection = $this->_productFactory->create()->getCollection()
               // ->addAttributeToSelect(
               //     'title'
               // )->addAttributeToSelect(
               //     'author'
               // )
        ->addOrder('articles_id', 'asc');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
                
        $this->addColumn(
            'in_events',
            [
                'header_css_class'  => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_events',
                'values' => $this->_getSelectedProducts(),
                'index' => 'articles_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction',
                   // 'use_index' => true
            ]
        );
       
        $this->addColumn(
            'articles_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'articles_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('title', ['header' => __('Name'), 'index' => 'title']);
        $this->addColumn('author', ['header' => __('Author'), 'index' => 'author'
            ]);
        /*$this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'editable' => 'false'
            ]
        );*/

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('articles/*/gridarticles', ['_current' => true]);
    }

    protected function _getSelectedProducts()
    {
        $id = $this->getRequest()->getParam('articles_id');
        $relatedProducts   = $this->_eventFactory->getRelatedArticles($id);
         
        if ($relatedProducts) {
            $relatedProducts = array_values($relatedProducts);
            return $relatedProducts;
        } else {
            return [];
        }
    }
}
