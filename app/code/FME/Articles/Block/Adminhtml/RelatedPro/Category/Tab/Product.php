<?php

namespace FME\Articles\Block\Adminhtml\RelatedPro\Category\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;

class Product extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    protected $_coreRegistry = null;
    protected $_productFactory;
    protected $_eventFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \FME\Articles\Model\Article $eventFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_eventFactory = $eventFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('catalog_category_products');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('articles_id')) {
            $this->setDefaultFilter(['in_events'=>1]);
        }
    }

    public function getCategory()
    {
        return $this->_coreRegistry->registry('articles_article');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_events') {
            $productIds = $this->_getSelectedProducts();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } elseif (!empty($productIds)) {
                $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
               $collection = $this->_productFactory->create()->getCollection()
               ->addAttributeToSelect(
                   'name'
               )->addAttributeToSelect(
                   'sku'
               )->addAttributeToSelect(
                   'price'
               )
        ->addOrder('entity_id', 'asc');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
                
            $this->addColumn(
                'in_events',
                [
                    'header_css_class'  => 'a-center',
                    'type' => 'checkbox',
                    'name' => 'in_events',
                    'values' => $this->_getSelectedProducts(),
                    'index' => 'entity_id',
                    'header_css_class' => 'col-select col-massaction',
                   'column_css_class' => 'col-select col-massaction',
                   // 'use_index' => true
                ]
            );
       
        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name']);
        $this->addColumn('sku', ['header' => __('SKU'), 'index' => 'sku'
            ]);
        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'type' => 'currency',
                'currency_code' => (string)$this->_scopeConfig->getValue(
                    \Magento\Directory\Model\Currency::XML_PATH_CURRENCY_BASE,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                'index' => 'price'
            ]
        );
        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'editable' => 'false'
            ]
        );

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('articles/*/gridpro', ['_current' => true]);
    }

    protected function _getSelectedProducts()
    {
        $id = $this->getRequest()->getParam('articles_id');
        $relatedProducts   = $this->_eventFactory->getRelatedPro($id);

        if ($relatedProducts) {
            $relatedProducts = array_values($relatedProducts);
            return $relatedProducts;
        } else {
            return [];
        }
    }
}
