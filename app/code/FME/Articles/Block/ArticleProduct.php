<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Block\Product\Options;
use Magento\Catalog\Block\Product\AbstractProduct;
use FME\Articles\Block\Detail;

class ArticleProduct extends Template
{

    protected $collectionFactory;
    protected $mediaFactory;
    protected $videoFactory;
    protected $objectManager;
    protected $articlefunctions;
    protected $productFactory;
    public $articlesHelper;
    public $videoHelper;
    public $sliderHelper;
    public $socialHelper;
    protected $productTypeConfig;
    //protected $_registry = null;
    protected $productRepository;
    protected $_productCollectionFactory;
    protected $listProductBlock;
    protected $viewProductBlock;
    protected $optionsProductBlock;
    protected $absHelper;
    protected $_cartHelper;
    protected $_selecto;

    protected $_registry;

    public function __construct(
        ProductFactory $productFactory,
        \FME\Articles\Model\ResourceModel\Article\CollectionFactory $collectionFactory,
        \FME\Articles\Model\Article $mediaFactory,
        \FME\Articles\Model\ResourceModel\Videos\CollectionFactory $videoFactory,
        \FME\Articles\Helper\Article $helper,
        \FME\Articles\Helper\Data $videoHelper,
        \FME\Articles\Helper\Slider $helperSlider,
        \FME\Articles\Helper\ArticleSocial $socialHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        ProductRepositoryInterface $productRepository,
        ObjectManagerInterface $objectManager,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ListProduct $listProductBlock,
        \Magento\Catalog\Block\Product\View $viewProductBlock,
        \Magento\Catalog\Block\Product\View\Options $optionsProductBlock,
        \Magento\Catalog\Block\Product\AbstractProduct $absHelper,
        \Magento\Checkout\Helper\Cart $cartHelper,
        Article $articlefunctions,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->productFactory = $productFactory;
        $this->collectionFactory = $collectionFactory;
        $this->mediaFactory = $mediaFactory;
        $this->videoFactory = $videoFactory;
        $this->objectManager = $objectManager;
        $this->articlesHelper = $helper;
        $this->videoHelper   = $videoHelper;
        $this->sliderHelper = $helperSlider;
        $this->socialHelper = $socialHelper;
        $this->productTypeConfig = $productTypeConfig;
        
        $this->productRepository = $productRepository;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->listProductBlock = $listProductBlock;
        $this->viewProductBlock = $viewProductBlock;
        $this->optionsProductBlock = $optionsProductBlock;
        $this->absHelper = $absHelper;
        $this->articlefunctions = $articlefunctions;
        $this->_cartHelper = $cartHelper;
        $this->_registry = $registry;



         parent::__construct($context, $data);
         $this->setTabTitle();
    }

    public function setTabTitle()
    {
        
        
            $title = $this->articlesHelper->getArticleProductPageTitle();
       

        $this->setTitle(__($title));
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }
    
    public function configVal($p)
    {
        $configValue = $this->getProduct($p)->getPreconfiguredValues()
            ->getData();
    }

    public function getArticleDetail()
    {
        $params = $this->getRequest()->getParams();
        $prefix = $params['id'];
        $collection = $this->collectionFactory->create()->addFieldToFilter('identifier', $prefix);
        return $collection;
    }

    public function getArticleDetailTitle($eventId, $earg)
    {

        $collection = $this->collectionFactory->create()->addFieldToFilter('identifier', $eventId);
        $collection = $collection->getData();
        $collection = $collection[0][$earg];
            return $collection;
    }

    public function getCurrentImage($eid)
    {
        $image = $this->mediaFactory->create()->addFieldToFilter('event_id', $eid)
            ->setPageSize(1);
        $image = $image->getData();
        if ($image) {
            $image = $image['0']['file'];
        }

        return $image;
    }

    public function getEventGalleries($eid)
    {
        $image = $this->mediaFactory->create()->         addFieldToFilter('event_id', $eid);
        return $image;
    }

    public function getMediaUrl()
    {

        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            return $media_dir.'tmp/skin01.zip';
    }
    
    public function getCarousUrl()
    {

        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            return $media_dir;
    }

    public function getJsUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
    }
    

    public function getArticleVideos($id)
    {

        $videoIds = $this->mediaFactory->getRelatedVideos($id);
        return $videos   = $this->videoFactory->create()->         addFieldToFilter('video_id', ['in' => $videoIds]);
    }

    public function getRelatedPostsFront($id)
    {
        $articleIds = $this->mediaFactory->getArticlesForProductPage($id);
        return $articles   = $this->collectionFactory->create()->
                       addFieldToFilter('articles_id', ['in'=> $articleIds]);
    }

    public function getRelatedCommentsFront($id)
    {
        return $articleIds = $this->mediaFactory->getRelatedCommentsFront($id);
    }

    public function getRelatedProducts($id)
    {
        $productIds = $this->mediaFactory->getRelatedPro($id);
        $products = $this->_productCollectionFactory->create()
        ->addFieldToFilter('entity_id', ['in'=> $productIds])
        ->addAttributeToSelect('*')->load();

        return $products;
    }

    public function loadMyProduct($sku)
    {
         return $this->productRepository->get($sku);
    }

    public function getCategoriesCollection()
    {
        return $this->articlefunctions->getCategoriesCollection();
    }
    public function getFeaturedArticles()
    {
        return $this->articlefunctions->getFeaturedArticles();
    }

    public function getRecentPosts()
    {
        return $this->articlefunctions->getRecentPosts();
    }

    public function getTagsCollections()
    {
        return $this->articlefunctions->getTagsCollections();
    }
}
