<?php
/**
 * FME Extensions
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the fmeextensions.com license that is
 * available through the world-wide-web at this URL:
 * https://www.fmeextensions.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    FME
 * @package     FME_Articles
 * @author      Dara Baig  (support@fmeextensions.com)
 * @copyright   Copyright (c) 2017 FME (http://fmeextensions.com/)
 * @license     https://fmeextensions.com/LICENSE.txt
 */
namespace FME\Articles\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;
  
class HomeArticle extends Template
{
    public $articlesHelper;
    public $sliderHelper;
    public $socialHelper;
    protected $_categoryWiseArticles;
    protected $scopeConfig;
    protected $collectionFactory;
    protected $mediaFactory;
    protected $objectManager;
    protected $request;
    protected $categoryCollection;
    protected $tagsCollection;
    protected $date;
    protected $timezone;
    
        
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FME\Articles\Model\ResourceModel\Article\
        CollectionFactory $collectionFactory,
        \FME\Articles\Model\ResourceModel\Media\
        CollectionFactory $mediaFactory,
        \FME\Articles\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \FME\Articles\Model\Article $tagsCollection,
        \FME\Articles\Helper\Article $helper,
        \FME\Articles\Helper\ArticleSocial $socialHelper,
        \FME\Articles\Model\Category $categoryWiseArticles,
        \FME\Articles\Helper\Slider $helperSlider,
        ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->mediaFactory = $mediaFactory;
        $this->objectManager = $objectManager;
        $this->articlesHelper = $helper;
        $this->sliderHelper = $helperSlider;
        $this->socialHelper = $socialHelper;
        $this->categoryCollection = $categoryCollection;
        $this->_categoryWiseArticles = $categoryWiseArticles;
        $this->request = $request;
        $this->tagsCollection = $tagsCollection;
        $this->date = $date;
        $this->timezone = $timezone;
                
        parent::__construct($context);
    }
    

     public function getAritlcesCollection()
    {
            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('fme_articles_category'); //gives table name with prefix
 
            //Select Data from table
            $sql = "SELECT `articles_id` FROM `fme_articles_category` WHERE `category_id`=26;";
            $result = $connection->fetchAll($sql);



             foreach ($result as $value) {
                  $id []= $value['articles_id'];  
                /*echo "\n";
                  // $id2 = $value[1];*/
             } 

               
        
           /*$id = "SELECT * FROM `fme_articles_category` WHERE `category_id`=26";*/

            $collection = $this->collectionFactory->create()->addFieldToFilter('is_active', 1) 
                                                            ->addFieldToFilter('articles_id',[$id]);


            
            return $collection;
        }
    

    

}