<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\  FME Photogallery Module  \\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   FME                            ///////
 \\\\\\\                      * @package    FME_Photogallery              \\\\\\\
 ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
namespace FME\Photogallery\Controller\Adminhtml ;

abstract class Photogallery extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */

   
    protected $resultPageFactory;
    protected $resultLayoutFactory;
    /**
     * @param \Magento\Backend\App\Action\Context                   $context
     * @param \Magento\Catalog\Controller\Adminhtml\Product\Builder $productBuilder
     * @param \Magento\Framework\View\Result\PageFactory            $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
    }
       


    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        
        return $resultPage;
    }
    

    protected function _initProductPhotogallery()
    {
        
        
        $photogallery = $this->_objectManager->create('FME\Photogallery\Model\Photogallery');
        $photogalleryId  = (int) $this->getRequest()->getParam('id');
       
        if ($photogalleryId) {
            $photogallery->load($photogalleryId);
        }
        $this->_objectManager->get('Magento\Framework\Registry')
        ->register('current_photogallery_products', $photogallery);
        
        return $photogallery;
    }
    public function splitImageValue($imageValue, $attr = "name")
    {
        $imArray=explode("/", $imageValue);

        $name=$imArray[count($imArray)-1];
        $path=implode("/", array_diff($imArray, [$name]));
        if ($attr=="path") {
            return $path;
        } else { 
            return $name;
        }
    }

    protected function _isAllowed()
    {
        return true;
    }
}
