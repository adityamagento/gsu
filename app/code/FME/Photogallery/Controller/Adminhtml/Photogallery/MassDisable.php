<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Photogallery\Controller\Adminhtml\Photogallery;

use FME\Photogallery\Controller\Adminhtml\AbstractMassStatus;

/**
 * Class MassDelete
 */
class MassDisable extends AbstractMassStatus
{
    /**
     * Field id
     */
    const ID_FIELD = 'photogallery_id';

    /**
     * ResourceModel collection
     *
     * @var string
     */
    protected $collection = 'FME\Photogallery\Model\ResourceModel\Photogallery\Collection';

    /**
     * Page model
     *
     * @var string
     */
    protected $model = 'FME\Photogallery\Model\Photogallery';

    /**
     * Item status
     *
     * @var bool
     */
    protected $status = 2;
}
