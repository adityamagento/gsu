<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Photogallery\Controller\Adminhtml\Photogallery;

use FME\Photogallery\Controller\Adminhtml\AbstractMassDelete;

/**
 * Class MassDelete
 */
class MassDelete extends AbstractMassDelete
{
    /**
     * Field id
     */
    const ID_FIELD = 'photogallery_id';

    /**
     * ResourceModel collection
     *
     * @var string
     */
    protected $collection = 'FME\Photogallery\Model\ResourceModel\Photogallery\Collection';

    /**
     * Page model
     *
     * @var string
     */
    protected $model = 'FME\Photogallery\Model\Photogallery';
}
