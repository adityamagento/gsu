<?php

/* ////////////////////////////////////////////////////////////////////////////////
  \\\\\\\\\\\\\\\\\\\\\\\\\  FME Photogallery Module  \\\\\\\\\\\\\\\\\\\\\\\\\
  /////////////////////////////////////////////////////////////////////////////////
  \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  ///////                                                                   ///////
  \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
  ///////   that is bundled with this package in the file LICENSE.txt.      ///////
  \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
  ///////          http://opensource.org/licenses/osl-3.0.php               ///////
  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  ///////                      * @category   FME                            ///////
  \\\\\\\                      * @package    FME_Photogallery              \\\\\\\
  ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
  \\\\\\\                                                                   \\\\\\\
  /////////////////////////////////////////////////////////////////////////////////
  \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
  /////////////////////////////////////////////////////////////////////////////////
 */

namespace FME\Photogallery\Controller\Adminhtml\Photogallery;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use FME\Prodfaqs\Model\Faqs;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\RequestInterface;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     *
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $dataPersistor;
    protected $scopeConfig;

    protected $_escaper;
    protected $inlineTranslation;
    protected $_dateFactory;
    /**
     * @param \Magento\Backend\App\Action\Context               $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $coreresource,
        Context $context,
        DataPersistorInterface $dataPersistor,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateFactory
    ) {

        $this->dataPersistor = $dataPersistor;
         $this->scopeConfig = $scopeConfig;
         $this->_escaper = $escaper;
        $this->_dateFactory = $dateFactory;
         $this->inlineTranslation = $inlineTranslation;
        parent::__construct($context);
        $this->_coreresource = $coreresource;
    }

    public function execute()
    {
        //print_r($data = $this->getRequest()->getPostValue());
       // exit;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

       //Load product by product id
        $helper = $objectManager->create('FME\Photogallery\Helper\Data');


        //$width = $helper->getThumbWidth();
        //$height = $helper->getThumbHeight();

        if ($data = $this->getRequest()->getPostValue()) {
            /* Gallery Images Array */
            if (empty($data['photogallery_categories']))
           {
              $data['category_ids']=null;
            }
            $gallery = isset($data['gallery']) ? $data['gallery'] : [];
            /* Images Array */
            $_photos_info = isset($gallery['images']) ? $gallery['images'] : [];
            // echo "<pre>";
            // print_r($data);
            // exit();
            //Upload File
            $id = $this->getRequest()->getParam('photogallery_id');
            if (empty($data['photogallery_id'])) {
                $data['photogallery_id'] = null;
            }
            $model = $this->_objectManager->create('FME\Photogallery\Model\Photogallery')->load($id);



            if ($id) {
                $model->load($id);
            }

            if (!empty($data["photogallery_categories"])) {
                $arr = $data["photogallery_categories"];
                $str = implode(",", $arr);
                //  $catIds = explode(",", $categoryidsString);
               // $result = array_unique($catIds);
               // $comma_separated = implode(",", $result);



                $data["category_ids"] = $str;
                //print_r($data["category_ids"]);
            }

//echo $data['photogallery_categories'];
//exit;
            /*if (isset($data["photogallery_categories"])) {
                $categoryidsString = trim($data["photogallery_categories"], ",");
                $catIds = explode(",", $categoryidsString);
                $result = array_unique($catIds);
                $comma_separated = implode(",", $result);



                $data["category_ids"] = $comma_separated;
            }*/


            if (isset($data["category_products"])) {
                $cat_array = json_decode($data['category_products'], true);



                $pro_array = array_values($cat_array);
                $c=0;
                foreach ($cat_array as $key => $value) {
                    $pro_array[$c] = $key;
                    $c++;
                }

                unset($data['category_products']);
                $data['product_id'] = $pro_array;
            }



            $model->setData($data);

            if ($id) {
                $model->setId($id);
            }

            $this->inlineTranslation->suspend();
            try {
                if ($model->getCreatedTime() == null || $model->getUpdateTime() == null) {
                    $model->setCreatedTime(date('y-m-d h:i:s'))
                        ->setUpdateTime(date('y-m-d h:i:s'));
                } else {
                    $model->setUpdateTime(date('y-m-d h:i:s'));
                }

                $model->save();
                /* Attaching Uploaded Images With Gallery */


                $_conn_read = $this->_coreresource->getConnection('core_read');
                $_conn = $this->_coreresource->getConnection('core_write');
                $photogallery_images_table = $this->_coreresource->getTableName('photogallery_images');
                if (!empty($_photos_info)) {
                    foreach ($_photos_info as $_photo_info) {
                        //Do update if we have gallery id (menaing photo is already saved)
                        if ($_photo_info['photogallery_id'] != null) {
                            $data = [
                                "img_name" => str_replace(".tmp", "", $_photo_info['file']),
                                "img_label" => $_photo_info['label'],
                                "tags" => $_photo_info['tags'],
                                //"width" => $_photo_info['width'],
                                //"height" => $_photo_info['height'],
                                "img_description" => $_photo_info['video_description'],
                                "photogallery_id" => $_photo_info['photogallery_id'],
                                "img_order" => $_photo_info['position'],
                                "disabled" => $_photo_info['disabled'],
                            ];

                            $where = ["img_id = " . (int) $_photo_info['value_id']];
                            $_conn->update($photogallery_images_table, $data, $where);

                            if (isset($_photo_info['removed']) and $_photo_info['removed'] == 1) {
                                $_conn->delete(
                                    $photogallery_images_table,
                                    'img_id = ' . (int) $_photo_info['value_id']
                                );
                            }
                        } else {
                            $_lookup = $_conn_read->fetchAll(
                                "SELECT * FROM " . $photogallery_images_table . " WHERE img_name = ?",
                                $_photo_info['file']
                            );
                            //echo "here2"; exit;
                            if (empty($_lookup)) {
                                $_conn->insert(
                                    $photogallery_images_table,
                                    [
                                    'img_name' => str_replace(".tmp", "", $_photo_info['file']),
                                    'img_label' => $_photo_info['label'],
                                    'tags' => $_photo_info['tags'],
                                    //'width' => $width,
                                    //'height' =>$height,
                                    'img_description' => $_photo_info['video_description'],
                                    'photogallery_id' => $model->getId(),
                                    'img_order' => $_photo_info['position'],
                                    "disabled" => $_photo_info['disabled'],
                                    ]
                                );
                            }
                        }
                    }
                }


                /* End Images Section */
                $this->messageManager->addSuccess(__('Photogallery was successfully saved'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                $this->dataPersistor->clear('photogallery');

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $model->getId()]);
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
                return;
            }
        }
        $this->messageManager->addError(__('Unable to find Photogallery to save'));
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('FME_Photogallery::manage_items');
    }
}
