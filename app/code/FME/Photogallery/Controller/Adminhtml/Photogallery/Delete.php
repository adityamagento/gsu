<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\  FME Photogallery Module  \\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   FME                            ///////
 \\\\\\\                      * @package    FME_Photogallery              \\\\\\\
 ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
namespace FME\Photogallery\Controller\Adminhtml\Photogallery;

use Magento\Framework\App\Filesystem\DirectoryList;

class Delete extends \FME\Photogallery\Controller\Adminhtml\Photogallery
{
   
    public function execute()
    {
        $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
            ->getDirectoryRead(DirectoryList::MEDIA);
        $config = $this->_objectManager->get('FME\Photogallery\Model\Media\ConfigPhotogallery');
        $mediaRootDir = $mediaDirectory->getAbsolutePath($config->getPhotogalleryBaseTmpMediaPath());
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = $this->_objectManager->create('FME\Photogallery\Model\Photogallery');
                /*Delete Images*/
                $object = $this->_objectManager->create('FME\Photogallery\Model\ImgFactory');
                $collection = $object->create()->getCollection()
                ->addFieldToFilter('photogallery_id', $this->getRequest()->getParam('id'));

                foreach ($collection as $col) {
                    $file_name = $col->getImgName();
                    $imgPath=  $this->splitImageValue($file_name, "path");
                    $imgName=  $this->splitImageValue($file_name, "name");
                    $file_path = $mediaRootDir . $file_name;
                    $thumb_path = $mediaRootDir .$imgPath. DIRECTORY_SEPARATOR.'thumb'.DIRECTORY_SEPARATOR.$imgName;
                    if ($file_path) {
                        unlink($file_path);
                        unlink($thumb_path);
                    }
                }

                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                     
                $this->messageManager->addSuccess(__('Gallery was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        $this->_redirect('*/*/');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;//$this->_authorization->isAllowed('Magento_Cms::save');
    }
}
