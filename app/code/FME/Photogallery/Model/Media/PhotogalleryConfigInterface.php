<?php

/**
 * Media library image config interface
 */
namespace FME\Photogallery\Model\Media;

interface PhotogalleryConfigInterface
{
    public function getPhotogalleryBaseMediaUrl();

    public function getPhotogalleryBaseMediaPath();

    public function getPhotogalleryMediaUrl($file);

    public function getPhotogalleryMediaPath($file);
}
