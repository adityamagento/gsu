<?php

namespace FME\Photogallery\Model\Config\Socialmedia;

class Iconstyle implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'social-icons-none', 'label' => __('None')],
        ['value' => 'social-icons-circle', 'label' => __('Cirlce')] ,
        ['value' => 'social-icons-bar', 'label' => __('Bar')] ];
    }
}
