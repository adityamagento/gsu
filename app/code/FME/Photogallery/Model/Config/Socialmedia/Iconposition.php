<?php

namespace FME\Photogallery\Model\Config\Socialmedia;

class Iconposition implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'social-icons-right', 'label' => __('Right')],
        ['value' => 'social-icons-bottom', 'label' => __('Bottom')]];
    }
}
