<?php

namespace FME\Photogallery\Model\Config\Zoom;

class Zoomspeed implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'effect-speed-very-slow', 'label' => __('1s')],
        ['value' => 'effect-speed-slow', 'label' => __('0.5s')],
        ['value' => 'effect-speed-medium', 'label' => __('0.35s')],
        ['value' => 'effect-speed-fast', 'label' => __('0.2s')],
        ['value' => 'effect-speed-very-fast', 'label' => __('0.1s')] ];
    }
}
