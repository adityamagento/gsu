<?php

namespace FME\Photogallery\Model\Config\Zoom;

class Zoomeffect implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'effect-deep-zoom-in', 'label' => __('Deep zoom in')],
        ['value' => 'effect-zoom-in', 'label' => __('Zoom in')],
        ['value' => 'effect-zoom-out', 'label' => __('Zoom out')],
        ['value' => 'effect-deep-zoom-out', 'label' => __('Deep zoom out')] ];
    }
}
