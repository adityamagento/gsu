<?php
/**
 * Copyright © 2016 FME. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace FME\Photogallery\Model\Config\Source;

class Galpositions implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {
        $gals[0] = [
                           'value' => 1,
                           'label' => __('Photogallery Page only'),
                          ];
        $gals[1] = [
                           'value' => 2,
                           'label' => __('Product Page Only'),
                          ];
        $gals[2] = [
                           'value' => 3,
                           'label' => __('Both in Product and Photogallery pages'),
                          ];
        return $gals;
    }//end toOptionArray()
}//end class
