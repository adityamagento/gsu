<?php

namespace FME\Photogallery\Model\Config\Popup;

class Magnifer implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'popup', 'label' => __('Pop Up')],
        ['value' => 'lighbox', 'label' => __('Light Box')] ];
    }
}
