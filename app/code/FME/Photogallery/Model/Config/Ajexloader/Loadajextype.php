<?php

namespace FME\Photogallery\Model\Config\Ajexloader;

class Loadajextype implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'manual', 'label' => __('Manual')],
        ['value' => 'auto', 'label' => __('Auto')] ];
    }
}
