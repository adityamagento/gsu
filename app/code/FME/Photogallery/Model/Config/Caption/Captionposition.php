<?php

namespace FME\Photogallery\Model\Config\Caption;

class Captionposition implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'caption-top', 'label' => __('Top')],
        ['value' => 'caption-middle', 'label' => __('Middle')],
        ['value' => 'caption-bottom', 'label' => __('Bottom')] ];
    }
}
