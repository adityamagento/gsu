<?php

namespace FME\Photogallery\Model\Config\Caption;

class Captionanimation implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'caption-none', 'label' => __('None')],
        ['value' => 'caption-fixed', 'label' => __('Fixed, always visible')],
        ['value' => 'caption-fixed-bg', 'label' => __('Fixed with background')],
        ['value' => 'caption-fixed-then-hidden', 'label' => __('Fixed with background, hide on mouse hover')],
        ['value' => 'caption-fixed-bottom', 'label' => __('Fixed at bottom with gradient background')],
        ['value' => 'caption-slide-from-top', 'label' => __('Slide from top')],
        ['value' => 'caption-slide-from-bottom', 'label' => __('Slide from bottom')] ];
    }
}
