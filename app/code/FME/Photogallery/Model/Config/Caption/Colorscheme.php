<?php

namespace FME\Photogallery\Model\Config\Caption;

class Colorscheme implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'caption-color-light', 'label' => __('Dark text on white background')],
        ['value' => 'caption-color-dark', 'label' => __('Light text on dark background')] ];
    }
}
