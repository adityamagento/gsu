<?php

namespace FME\Photogallery\Model\Config\Caption;

class Captionalingment implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'caption-left', 'label' => __('Left')],
        ['value' => 'caption-center', 'label' => __('Center')],
        ['value' => 'caption-right', 'label' => __('Right')] ];
    }
}
