<?php

namespace FME\Photogallery\Model\Config\Layout;

class Thumbslayout implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => 'col', 'label' => __('Columns')],
        ['value' => 'final', 'label' => __('Final')] ];
    }
}
