<?php

namespace FME\Photogallery\Model\Config\Margin;

class Marginlist implements \Magento\Framework\Option\ArrayInterface
{
    
    public function toOptionArray()
    {
        return [['value' => '0', 'label' => __('0 px')],
        ['value' => '2', 'label' => __('2 px')],
        ['value' => '5', 'label' => __('5 px')],
        ['value' => '10', 'label' => __('10 px')],
        ['value' => '20', 'label' => __('20 px')],
        ['value' => '40', 'label' => __('40 px')] ];
    }
}
