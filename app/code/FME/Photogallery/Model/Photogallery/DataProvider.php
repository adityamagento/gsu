<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace FME\Photogallery\Model\Photogallery;

use FME\Photogallery\Model\ResourceModel\Photogallery\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\Collection
     */
    protected $collection;
    public $_storeManager;
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string                 $name
     * @param string                 $primaryFieldName
     * @param string                 $requestFieldName
     * @param CollectionFactory      $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array                  $meta
     * @param array                  $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->_storeManager=$storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        $baseurl =  $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /**
 * @var \Magento\Cms\Model\Block $block
*/
        foreach ($items as $block) {
            $data = $block->getData();

                $data['photogallery_categories'] = explode(',', $block->getData()['category_ids']);
            

            $this->loadedData[$block->getId()] = $data;
        }

    /*foreach ($items as $item) {
        $data = $item->getData();
        $data['photogallery_categories'] = explode(',', $data['category_ids']);
        $result['example_details'] = $data;
        $this->loadedData[$item->getId()] = $result;
    }*/

        $data = $this->dataPersistor->get('photogallery');
         //print_r($data->getData());
        // exit;
        if (!empty($data)) {
            $block = $this->collection->getNewEmptyItem();
            $block->setData($data);
            //print_r($block->getData());
            //exit;
            $this->loadedData[$block->getId()] = $block->getData();

            $this->dataPersistor->clear('photogallery');
        }



                return $this->loadedData;
    }
}
