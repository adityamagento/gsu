<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\  FME Mediaappearance Module  \\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   FME                            ///////
 \\\\\\\                      * @package    FME_Mediaappearance              \\\\\\\
 ///////    * @author    FME Extensions <support@fmeextensions.com>   ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\* @copyright  Copyright 2015 © fmeextensions.com All right reserved\\\
 /////////////////////////////////////////////////////////////////////////////////
 */
namespace FME\Photogallery\Block;

use Magento\Store\Model\Store;

error_reporting(E_ALL);
ini_set('display_errors', 1);
class Owlcarousal extends \Magento\Framework\View\Element\Template
{

    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    public $_storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public $_scopeConfig;
    /**
     * @var \FME\Mediaappearance\Helper\Data
     */
    public $_helper;
    protected $blockid;
    protected $galleryidentifier;
    protected $gallerytype;
    protected $enablecaption;
    protected $captionposition;
    protected $captionanimation;
    protected $captionalignment;
    protected $captioncolor;
    protected $enableicons;
    protected $enablezoom;
    protected $zoomeffect;
    protected $zoomspeed;
    protected $enablesm;
    protected $smposition;
    protected $smstyle;
    protected $enableicon;
    protected $iconclass;

 

   
    

    
    /**
     *
     * @param \Magento\Backend\Block\Template\Context                           $context
     * @param \FME\Mediaappearance\Helper\Data                                  $helper
     * @param \FME\Mediaappearance\Model\Resource\Videoblocks\CollectionFactory $blockCollection
     * @param \FME\Mediaappearance\Model\Videoblocks                            $blockModel
     * @param array                                                             $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Registry $registry,
        \FME\Photogallery\Helper\Data $helper,
        \FME\Photogallery\Model\ImgFactory $photogalleryimgFactory,
        \FME\Photogallery\Model\Img $photogalleryimg,
        \FME\Photogallery\Model\PhotogalleryFactory $photogalleryphotogalleryFactory,
        \FME\Photogallery\Model\Photogallery $photogalleryphotogallery,
        \Magento\Framework\App\ResourceConnection $coreresource,
        \FME\Photogallery\Model\ResourceModel\Photogallery\CollectionFactory $blockCollection,
        // \FME\Photogallery\Model\Videoblocks $blockModel,
        array $data = []
    ) {
            $this->_blockCollection = $blockCollection;
          //  $this->_blockModel = $blockModel;
          $this->_photogalleryimgFactory = $photogalleryimgFactory;
          $this->_photogalleryimg = $photogalleryimg;
          $this->_photogalleryphotogalleryFactory = $photogalleryphotogalleryFactory;
          $this->_photogalleryphotogallery = $photogalleryphotogallery;
          $this->_urlInterface = $context->getUrlBuilder();
          $this->_objectManager = $objectManager;
          $this->_coreRegistry = $registry;
          $this->_storeManager = $context->getStoreManager();
          $this->pageConfig = $context->getPageConfig();
          $this->_helper = $helper;
          $this->_scopeConfig = $context->getScopeConfig();
          $this->_coreresource = $coreresource;
            //$this->_helper = $helper;
            parent::__construct($context, $data);
    }

   /**
    * _tohtml
    * @return html
    */
    protected function _tohtml()
    {
     
        $this->blockid = $this->getBlockId();
        $this->galleryidentifier = $this->getGalleryId();
        $this->gallerytype=$this->getGalleryType();
       // $this->galleryidentifier= $this->getEnableCaption();
        $this->enablecaption= $this->getEnableCaption();
        $this->captionposition= $this->getCaptionPosition();
        $this->captionanimation= $this->getCaptionAnimation();
        $this->captionalignment= $this->getCaptionAlignment();
        $this->captioncolor= $this->getCaptionColor();
        $this->enableicons= $this->getEnableIcon();
        $this->enablezoom= $this->getEnableZoom();
        $this->zoomeffect= $this->getZoomEffect();
        $this->zoomspeed= $this->getZoomSpeed();
        $this->enablesm= $this->getEnableSm();
        $this->smposition= $this->getSmiconsPosition();
        $this->smstyle= $this->getSmiconsStyle();

        $this->enableicon= $this->getEnableIcon();
        $this->iconclass= $this->getIconClass();
    
       // $this->setTemplate("FME_Photogallery::owlcaroisal.phtml");
        return parent::_toHtml();
    }


    public function isproductSet()
    {
        return $this->_coreRegistry->registry('product');
    }
    public function isCategorySet()
    {
        return $this->_coreRegistry->registry('current_category')->getId();
    }

    public function getGalleryForCatorPro()
    {
        $product = $this->_coreRegistry->registry('product');
        $storeId = $this->_storeManager->getStore()->getId();
        
        if ($this->_coreRegistry->registry('product')) {
            $productid = $this->_coreRegistry->registry('product')->getId();
            $galids = array();

            $galleryImages = array();

            $result = $this->getProductGalleries($productid);




            foreach ($result as $r) {
                $galids[] = $r['photogallery_id'];
            }


        //print_r($galleryIDs); exit;

            if (!empty($galids)) {
                $galleryImages = $this->getProductGimages($galids);
            }
            return  $galleryImages;
        } elseif ($this->_coreRegistry->registry('current_category')->getId()) {
            $cid = $this->_coreRegistry->registry('current_category')->getId();
            $collection = $this->_blockCollection->create()
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addFieldToFilter(
                'category_ids',
                [
                            ['finset'=> [$cid]]]
            )
            ->addFieldToFilter('main_table.status', 1);
            $galids = array();

            $galleryImages = array();

            $result =  $collection;
            



            foreach ($result as $r) {
                    $galids[] = $r['photogallery_id'];
            }

            
            //print_r($galleryIDs); exit;

            if (!empty($galids)) {
                $galleryImages = $this->getProductGGimages($galids);
            }


                return $galleryImages;
        }
    }


    public function getCategorygallery()
    {
        
        $storeId = $this->_storeManager->getStore()->getId();
        $cid = $this->_coreRegistry->registry('current_category')->getId();
        $collection = $this->_photogalleryphotogalleryFactory->create()
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addFieldToFilter(
                'category_ids',
                [
                            ['finset'=> [$cid]]]
            )
            ->addFieldToFilter('main_table.status', 1);
        return $collection;
    }
    /**
     * Retrieve Product Galleries Images
     *
     * @return array
     */
    
    public function getProductGGimages($photogalleryIds)
    {
        $photogalleryImages = $this->_blockCollection->create()->getPimages($photogalleryIds);
        return $photogalleryImages->getData();
    }







    public function getProduct()
    {
        $product = $this->_coreRegistry->registry('product');
        return $product;
    }


    public function getProductGalleries($productId)
    {
        
        $store = $this->_storeManager->getStore();
        $pgalleries = $this->_photogalleryphotogalleryFactory->create()
            ->getCollection()
            ->addStoreFilter($store)
            ->getPgalleries($productId);
                    //echo (string) $pgalleries->getSelect();exit;
        //echo '<pre>';print_r($pgalleries->getData());exit;
        return $pgalleries->getData();
    }

    /**
     * Retrieve Product Galleries Images
     *
     * @return array
     */
    public function getProductGimages($photogalleryIds)
    {
        $photogalleryImages = $this->_photogalleryphotogalleryFactory->create()->getCollection()->getPimages($photogalleryIds);
        return $photogalleryImages->getData();
    }
}
