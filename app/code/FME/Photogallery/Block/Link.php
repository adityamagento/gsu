<?php

namespace FME\Photogallery\Block;

class Link extends \Magento\Framework\View\Element\Html\Link
{

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \FME\Photogallery\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * @return Url string
     */
    public function getHref()
    {
        $url = $this->_helper->getPhotogalleryUrl();
        return $url;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Photogallery');
    }
}
