<?php

namespace FME\Photogallery\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            $this->addImageTagsColumn($setup);
        }
        
        $setup->endSetup();
    }
    
    private function addImageTagsColumn(SchemaSetupInterface $setup)
    {
        
        $connection = $setup->getConnection();

        
            $tableName = $setup->getTable('photogallery_images');
            $connection->addColumn(
                $tableName,
                'tags',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'default' => '',
                    'comment' => 'tags column'
                ]
            );
    }
}
