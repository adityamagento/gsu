<?php

namespace PayUIndia\Payu\Block\Adminhtml\Order\View\Tab;


class Verify extends \Magento\Backend\Block\Template implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'Order/View/Tab/verify.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,		
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context,$data);
    }

    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('current_order');
    }
	/**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
	public function getOrderId()
    {
        return $this->getOrder()->getEntityId();
    }
 
    /**
     * Retrieve order increment id
     *
     * @return string
     */
    public function getOrderIncrementId()
    {
        return $this->getOrder()->getIncrementId();
    }
	
	/**
	* Retrive merchant key
	* @return string
	*/
	public function getPaymentdetails()
	{
		$payment = $this->getOrder()->getPayment();
		$method = $payment->getMethodInstance();
		if ($method->getCode()!= "payu") {
            return false;
        }
        $txnId = $payment->getLastTransId() ? $payment->getLastTransId() : $this->getOrder()->getPayuTxnId();
		$res = $method->verifyPayment($this->getOrder(),$payment,$txnId);
		if(isset($res['error']))
			return $res['error'];
		else
		    return strtoupper($res['status']);
	}
	
    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Verify Payment');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Verify Payment');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        // For me, I wanted this tab to always show
        // You can play around with the ACL settings 
        // to selectively show later if you want
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        // For me, I wanted this tab to always show
        // You can play around with conditions to
        // show the tab later
        return false;
    }

    /**
     * Get Tab Class
     *
     * @return string
     */
    // public function getTabClass()
    // {
    //     // I wanted mine to load via AJAX when it's selected
    //     // That's what this does
    //     return 'ajax only';
    // }

    /**
     * Get Class
     *
     * @return string
     */
    // public function getClass()
    // {
    //     return $this->getTabClass();
    // }

    /**
     * Get Tab Url
     *
     * @return string
     */
    // public function getTabUrl()
    // {
    //     // customtab is a adminhtml router we're about to define
    //     // the full route can really be whatever you want
    //     return $this->getUrl('verifypaymenttab/*/verifypaymenttab', ['_current' => true]);
    // }
}